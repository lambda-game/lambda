# Contributing to lambda

First of all, thanks! Thanks for taking the time to make lambda better

## Clone & build

Follow the steps in the README to build lambda

## Make the changes

Make the changes to the source code, if you need help you can ask for help on the mailing list located
here: https://lists.sr.ht/~interfiber/Lambda

## Create a patchset

Once your changes are created you must create a patchset. You can create a patchset like below:

```bash
git format-patch -1 HEAD
```

## Submit the patch

To submit the patch you can send the patch to the mailing list, which you can find a link to above
