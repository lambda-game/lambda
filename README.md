# Lambda

Free & Open-Source Minecraft-like 2d tile game

![](./logo.png)

## Website

You can find prebuilt launcher binary's, and other info on [the website](https://lambda.interfiber.dev).

## What does the name lambda mean?

It's named after the [lambda character](https://en.wikipedia.org/wiki/Lambda). I don't really know why, it just sounded
cool to me, and i couldn't come up with another name.

## Building

Building lambda is an easy process

### Build Requirements

- Java 17+
- Gradle(use the wrapper, if in you don't want to install it)
- OpenAL
- OpenGL compatible graphics card
- Git

### Building

First clone the source as follows:

```bash
git clone https://git.sr.ht/~interfiber/lambda
```

Then change into the folder:

```bash
cd lambda
```

And finally build a release jar file:

```bash
./gradlew desktop:dist
# or use: gradlew desktop:dist for windows users
```

The result jar file can be found in: ```desktop/build/libs/desktop-[VERSION].jar```

## Questions, people might have

### pls use github!! :)

no.

### PORT THIS GAME TO RUST OR C++, IT WILL BE SOOOO MUCH FASTER!!!!!!!11!

I like rust, but it's not ready to develop games yet.
The performance also doesn't really matter that much, because this is a 2d game.

### Can you port this game to m1/m2 chips?

Since its written in java it should run, but the natives libraries used might not be compiled for m1 chips. If in does
not work use rosetta

### Where are the attributions for the project

You can find all credit in the ```CREDIT``` file.

### There's weird things happening in the result build for the main branch

The main branch might contain features that are being built, so they might not be completed yet.

### Why did you make this game free & open-source?

Because I felt like it

### How do I contribute?

Read the ```contributing.md``` file.
