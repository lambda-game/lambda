package com.interfiber.lambda.server.player;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.SerializableItem;
import com.interfiber.lambda.client.player.PlayerInventory;
import com.interfiber.lambda.client.world.WorldType;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.PlayerInventoryUpdatePacket;
import com.interfiber.lambda.server.networking.packets.clientbound.PlayerSwitchedWorldPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.SwitchedWorldPacket;
import com.interfiber.lambda.server.world.WorldLoader;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class OnlinePlayer {
    public String username;
    public String authUUID;
    public String playerUUID;
    public int x = 0;
    public int y = 0;
    public Channel serverConnection;
    public String facing = "down";
    public boolean isSwimming = false;
    public PlayerInventory inventory;
    public int selectedInventory = 0;
    public int selectedInventorySlot = 0;
    public WorldType currentWorld = WorldType.OVERWORLD;
    public int caveEnterX = -1;
    public int caveEnterY = -1;

    public ClientPlayer asClientPlayer() {
        ClientPlayer clientPlayer = new ClientPlayer();
        clientPlayer.username = this.username;
        clientPlayer.x = x;
        clientPlayer.y = y;
        clientPlayer.playerUUID = playerUUID;
        clientPlayer.currentWorld = currentWorld;
        return clientPlayer;
    }

    public void sendInventoryUpdate() {
        ParsedPacket payload = new ParsedPacket();

        ConcurrentHashMap<Integer, SerializableItem> items = new ConcurrentHashMap<>();

        for (Map.Entry<Integer, Item> set : inventory.getItems().entrySet()) {
            items.put(set.getKey(), set.getValue().getSerializableItem());
        }

        try {
            payload.content.put("inventoryContent", new ObjectMapper().writeValueAsString(items));
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to format packet content");
        }

        new PlayerInventoryUpdatePacket(payload).writeData(serverConnection);
    }

    public void switchWorld(WorldType targetWorld, boolean saveUnderworldExit, int underworldX, int underworldY) {
        MultiplayerGame.setWorldForPlayer(this, targetWorld);

        ParsedPacket packet = new ParsedPacket();

        try {
            if (targetWorld == WorldType.OVERWORLD) {
                packet.content.put("worldData", WorldLoader.getOverworldDataAsJson(true));
                packet.content.put("worldContainers", WorldLoader.getOverworldStorageContainersJson());
                packet.content.put("cropData", new ObjectMapper().writeValueAsString(CropManager.getCropData()));
                if (this.caveEnterY != -1 && this.caveEnterX != -1) {
                    packet.content.put("playerX", this.caveEnterX);
                    packet.content.put("playerY", this.caveEnterY);
                } else {
                    packet.content.put("playerX", (int) MultiplayerGame.overworldSpawn.x);
                    packet.content.put("playerY", (int) MultiplayerGame.overworldSpawn.y);
                }
            } else if (targetWorld == WorldType.UNDERWORLD) {

                if (saveUnderworldExit) {
                    MultiplayerGame.setCaveEntranceForPlayer(this, this.x, this.y);
                }

                packet.content.put("worldData", WorldLoader.getUnderworldDataAsJson(true));
                packet.content.put("worldContainers", WorldLoader.getUnderworldStorageContainersJson());
                packet.content.put("playerX", underworldX);
                packet.content.put("playerY", underworldY);
            }
            packet.content.put("worldType", targetWorld);
            packet.content.put("currentTime", MultiplayerGame.time);

            ParsedPacket changePacket = new ParsedPacket();
            changePacket.content.put("playerUUID", this.playerUUID);
            changePacket.content.put("worldType", targetWorld);

            MultiplayerGame.sendPacketToPlayers(new PlayerSwitchedWorldPacket(changePacket));
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Failed to format world switched packet");
            return;
        }

        new SwitchedWorldPacket(packet).writeData(this.serverConnection);
    }
}
