package com.interfiber.lambda.server.player;

import org.tinylog.Logger;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PlayerSaveDataManager {
    public static void savePlayerInfo(OnlinePlayer player) throws IOException {
        File saveDataFolder = getPlayerSaveFolder();

        PlayerDataSave playerDataSave = new PlayerDataSave();
        playerDataSave.playerItems = player.inventory.getStorableItems();
        playerDataSave.playerX = player.x;
        playerDataSave.playerY = player.y;
        playerDataSave.currentWorld = player.currentWorld;
        playerDataSave.caveEntranceX = player.caveEnterX;
        playerDataSave.caveEntranceY = player.caveEnterY;

        ObjectOutputStream outputStream = new ObjectOutputStream(Files.newOutputStream(Paths.get(String.valueOf(saveDataFolder), player.username + ".dat").toFile().toPath()));
        outputStream.writeObject(playerDataSave);
        outputStream.close();
    }

    public static PlayerDataSave getPlayerInfo(String username) {
        File saveDataFolder = getPlayerSaveFolder();

        Path path = Paths.get(String.valueOf(saveDataFolder), username + ".dat");
        try {
            if (path.toFile().exists()) {
                ObjectInputStream inputStream = new ObjectInputStream(Files.newInputStream(path));
                return (PlayerDataSave) inputStream.readObject();
            }
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Failed to load player save");
        }
        return null;
    }

    public static File getPlayerSaveFolder() {
        if (!new File("playerSaveData").exists()) {
            new File("playerSaveData").mkdirs();
        }

        return new File("playerSaveData");
    }
}
