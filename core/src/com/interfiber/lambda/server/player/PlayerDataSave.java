package com.interfiber.lambda.server.player;

import com.interfiber.lambda.client.items.SerializableItem;
import com.interfiber.lambda.client.world.WorldType;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerDataSave implements Serializable {
    public ConcurrentHashMap<Integer, SerializableItem> playerItems = new ConcurrentHashMap<>();
    public int playerX = 0;
    public int playerY = 0;
    public int caveEntranceX = 0;
    public int caveEntranceY = 0;
    public WorldType currentWorld = WorldType.OVERWORLD;
}
