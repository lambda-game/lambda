package com.interfiber.lambda.server.player;

import com.interfiber.lambda.client.world.WorldType;

public class ClientPlayer {
    public String username;
    public String playerUUID;
    public int x;
    public int y;
    public String facing = "down";
    public boolean isSwimming = false;
    public WorldType currentWorld = WorldType.OVERWORLD;
}
