package com.interfiber.lambda.server.auth;

import java.io.Serializable;

public class User implements Serializable {
    public String username;
    public String password;
    public String authToken;
    public String createDate;
}