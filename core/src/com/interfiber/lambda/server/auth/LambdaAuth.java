package com.interfiber.lambda.server.auth;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.server.MultiplayerGame;
import org.tinylog.Logger;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

public class LambdaAuth {
    public static User getUser(String token) throws IOException {

        RequestUserFromTokenInfo req = new RequestUserFromTokenInfo();
        req.token = token;
        String out = new ObjectMapper().writeValueAsString(req);

        URL url = new URL(MultiplayerGame.authServer + "/api/v1/auth/users/authenticateFromToken");
        URLConnection con = url.openConnection();
        HttpURLConnection http = (HttpURLConnection) con;
        http.setRequestMethod("POST"); // PUT is another valid option
        http.setDoOutput(true);

        http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
        http.connect();
        try (OutputStream os = http.getOutputStream()) {
            os.write(out.getBytes());
        }

        InputStream is = http.getInputStream();
        BufferedReader rd = new BufferedReader(new InputStreamReader(is));
        StringBuilder response = new StringBuilder(); // or StringBuffer if Java version 5+
        String line;
        while ((line = rd.readLine()) != null) {
            response.append(line);
            response.append('\r');
        }
        rd.close();

        UserInfoFromTokenRequestInfo userInfoFromToken = new ObjectMapper().readValue(response.toString(), UserInfoFromTokenRequestInfo.class);
        if (userInfoFromToken.user == null) {
            Logger.error("Invalid user");
            return null;
        } else {
            return userInfoFromToken.user;
        }
    }
}
