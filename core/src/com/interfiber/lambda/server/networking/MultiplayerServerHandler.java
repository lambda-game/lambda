package com.interfiber.lambda.server.networking;

import com.badlogic.gdx.graphics.Color;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.PlayerRemovedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import com.interfiber.lambda.server.player.PlayerSaveDataManager;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.tinylog.Logger;

public class MultiplayerServerHandler extends SimpleChannelInboundHandler<String> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) {
        PacketHandler.handlePacket(ctx.channel(), msg);
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) {
        for (OnlinePlayer player : MultiplayerGame.players) {
            if (player.serverConnection == ctx.channel()) {
                Logger.info(player.username + " left the game");
                MultiplayerGame.players.remove(player);

                ParsedPacket payload = new ParsedPacket();
                payload.content.put("playerUUID", player.playerUUID);

                MultiplayerGame.sendPacketToPlayers(new PlayerRemovedPacket(payload));

                MultiplayerGame.sendSystemMessage(player.username + " left the game", Color.YELLOW);

                // save player info
                try {
                    PlayerSaveDataManager.savePlayerInfo(player);
                } catch (Exception e) {
                    Logger.error(e);
                    Logger.error("Failed to save player info for:" + player.username);
                }
                break;
            }
        }
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        Logger.error(cause);
        Logger.error("Error during multiplayer server netty pipeline");
    }
}
