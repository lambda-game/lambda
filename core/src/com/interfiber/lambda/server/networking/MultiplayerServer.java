package com.interfiber.lambda.server.networking;

import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.config.LambdaServerConfigLoader;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.tinylog.Logger;

public class MultiplayerServer {
    public void startServer() throws InterruptedException {
        Logger.info("Starting server...");
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup) // Set boss & worker groups
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast(new DelimiterBasedFrameDecoder(Integer.MAX_VALUE, Delimiters.lineDelimiter()));
                            p.addLast(new StringDecoder());
                            p.addLast(new StringEncoder());
                            p.addLast(new MultiplayerServerHandler());
                        }
                    });
            // Start the server.
            if (LambdaServerConfigLoader.config.getProperty("port") != null){
                MultiplayerGame.port = Integer.parseInt(LambdaServerConfigLoader.config.getProperty("port"));
            }

            ChannelFuture f = b.bind("0.0.0.0", MultiplayerGame.port).sync();
            Logger.info("Started server on: 0.0.0.0:" + MultiplayerGame.port);

            // Wait until the server socket is closed.
            f.channel().closeFuture().sync();
        } finally {
            // Shut down all event loops to terminate all threads.
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }

    }
}
