package com.interfiber.lambda.server.networking;

import com.interfiber.lambda.server.networking.packets.Packet;

import java.util.HashMap;
import java.util.Map;

public class PacketManager {
    private static HashMap<Integer, Packet.PacketType> packetTypes = new HashMap<>();

    public static void init() {
        for (Packet.PacketType packetType : Packet.PacketType.values()) {
            packetTypes.put(packetType.getId(), packetType);
        }
    }

    public static Packet.PacketType lookupPacket(int packetId) {
        for (Map.Entry<Integer, Packet.PacketType> set : packetTypes.entrySet()) {
            if (set.getKey() == packetId) {
                return set.getValue();
            }
        }
        return null;
    }
}
