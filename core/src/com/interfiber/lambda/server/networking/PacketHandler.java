package com.interfiber.lambda.server.networking;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.*;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class PacketHandler {
    public static void handlePacket(Channel c, String msg) {
        // parse the json
        try {
            ParsedPacket parsedPacket = new ObjectMapper().readValue(msg, ParsedPacket.class);
            Packet.PacketType packetType = PacketManager.lookupPacket(parsedPacket.packetId);
            if (packetType == null) {
                Logger.error("Invalid packet");
                return;
            }

            if (packetType == Packet.PacketType.INVALID) {
                Logger.error("Invalid packet with ID: " + packetType.getId());
            } else if (packetType == Packet.PacketType.LOGIN) {
                new PlayerLoginPacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.PLAYER_POSITION_UPDATE) {
                new PlayerPositionUpdatePacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.PLACE_TILE) {
                new PlaceTilePacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.REMOVE_TILE) {
                new RemoveTilePacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.SWITCH_SELECTED_INVENTORY) {
                new SwitchSelectedInventoryPacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.SWITCH_SELECTED_INVENTORY_SLOT) {
                new SwitchSelectedInventorSlotPacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.CRAFT_RECIPE) {
                new CraftRecipePacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.SEND_CHAT_MESSAGE) {
                new SendChatMessagePacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.ADD_ITEM_TO_CHEST) {
                new AddItemToChestPacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.REMOVE_ITEM_FROM_CHEST) {
                new RemoveItemFromChestPacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.SWITCH_WORLD) {
                new SwitchWorldPacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.HOE_SOIL) {
                new HoeSoilPacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.WATER_SOIL) {
                new WaterSoilPacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.PLANT_CROP) {
                new PlantCropPacket(parsedPacket).handler(c);
            } else if (packetType == Packet.PacketType.HARVEST_CROP) {
                new HarvestCropPacket(parsedPacket).handler(c);
            } else {
                Logger.error("Unhandled packet!! ID of packet is: " + packetType.getId());
                Logger.debug("Packet content is: " + msg);
            }

        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to parse packet content!");
        }
    }
}
