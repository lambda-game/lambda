package com.interfiber.lambda.server.networking;

public enum PacketDirection {
    CLIENTBOUND,
    SERVERBOUND
}
