package com.interfiber.lambda.server.networking.packets.clientbound;

import com.badlogic.gdx.Gdx;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.farming.SerializableCrop;
import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.screens.GameOverworldScreen;
import com.interfiber.lambda.client.screens.GameUnderworldScreen;
import com.interfiber.lambda.client.world.*;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.ArrayList;

public class SwitchedWorldPacket extends Packet {

    public SwitchedWorldPacket(ParsedPacket packetData) {
        super(PacketType.SWITCHED_WORLD.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        Logger.info("Switching world");


        ObjectMapper mapper = new ObjectMapper();
        TypeFactory typeFactory = mapper.getTypeFactory();

        WorldType worldType = WorldType.valueOf(this.parsedPacket.content.get("worldType").toString());
        ArrayList<StorageContainerWorldTile> storageContainers;
        ArrayList<WorldTile> worldData;
        ArrayList<SerializableCrop> cropData = null;
        Time currentTime = Time.valueOf(this.parsedPacket.content.get("currentTime").toString());

        int x = Integer.parseInt(this.parsedPacket.content.get("playerX").toString());
        int y = Integer.parseInt(this.parsedPacket.content.get("playerY").toString());

        try {
            storageContainers = mapper.readValue(this.parsedPacket.content.get("worldContainers").toString(), typeFactory.constructCollectionType(ArrayList.class, StorageContainerWorldTile.class));
            worldData = mapper.readValue(this.parsedPacket.content.get("worldData").toString(), typeFactory.constructCollectionType(ArrayList.class, WorldTile.class));
            if (this.parsedPacket.content.containsKey("cropData")) {
                cropData = mapper.readValue(this.parsedPacket.content.get("cropData").toString(), typeFactory.constructCollectionType(ArrayList.class, SerializableCrop.class));
            }
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to parse switched world packet");
            return;
        }

        ArrayList<SerializableCrop> finalCropData = cropData;
        Gdx.app.postRunnable(() -> {
            if (Game.underworld == null) {
                Game.underworld = new Underworld();
            } else {
                Game.overworld = new Overworld();
            }

            if (finalCropData != null) {
                CropManager.loadCropData(finalCropData);
            }

            Game.currentWorld = worldType;
            Game.time = currentTime;
            Game.getCurrentWorld().loadWorldData(worldData);

            for (StorageContainerWorldTile tile : storageContainers) {
                Game.getCurrentWorld().getWorldData().add(tile);
            }

            if (Game.currentWorld == WorldType.UNDERWORLD) {
                Game.screenManager.switchScene(new GameUnderworldScreen(false));
            } else {
                Game.screenManager.switchScene(new GameOverworldScreen());
            }

            Player.x = x;
            Player.y = y;
        });
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
