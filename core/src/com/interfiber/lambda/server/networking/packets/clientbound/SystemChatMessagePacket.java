package com.interfiber.lambda.server.networking.packets.clientbound;

import com.badlogic.gdx.graphics.Color;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.multiplayer.ClientChatMessage;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class SystemChatMessagePacket extends Packet {
    public SystemChatMessagePacket(ParsedPacket packetData) {
        super(PacketType.SYSTEM_CHAT_MESSAGE.getId(), packetData);
        Logger.info("<System> " + packetData.content.get("messageContent").toString());
    }

    @Override
    public void handlePacket(Channel c) {
        Color color = Color.valueOf(this.parsedPacket.content.get("color").toString());
        String content = this.parsedPacket.content.get("messageContent").toString();

        ClientChatMessage message = new ClientChatMessage();
        message.isSystem = true;
        message.color = color;
        message.content = content;

        if (Game.chatHistory.size() + 1 > 6) {
            Game.chatHistory.remove(0);
        }
        Game.chatHistory.add(message);
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
