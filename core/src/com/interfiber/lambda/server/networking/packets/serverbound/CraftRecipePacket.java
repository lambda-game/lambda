package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.client.crafting.CraftingUtils;
import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeManager;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.SendClientToastPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CraftRecipePacket extends Packet {
    public CraftRecipePacket(ParsedPacket packetData) {
        super(PacketType.CRAFT_RECIPE.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        Logger.info("Crafting recipe");

        // check content
        if (this.parsedPacket.content.get("recipeName").toString() == null) {
            Logger.error("Invalid craft recipe packet, missing recipeName");
            return;
        }

        Recipe recipe = RecipeManager.getRecipe(this.parsedPacket.content.get("recipeName").toString());
        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);
        assert player != null;

        if (recipe == null) {
            Logger.error("Invalid recipe name!");
            return;
        }

        Logger.info("Recipe name is: " + recipe.getRecipeName());
        if (!CraftingUtils.canCraftItem(recipe, player.inventory)) {
            // send toast

            ParsedPacket packet = new ParsedPacket();
            packet.content.put("toastContent", "You cannot craft " + recipe.getRecipeName());

            new SendClientToastPacket(packet).writeData(player.serverConnection);

            return;
        }

        // craft
        ConcurrentHashMap<Integer, Item> playerItems = player.inventory.getItems();

        // remove items
        for (Map.Entry<Integer, Item> set : playerItems.entrySet()) {
            Item playerItem = set.getValue();
            Ingredient ingredient = new Ingredient();
            ingredient.itemType = playerItem.getType();
            ingredient.amount = playerItem.amount;
            int slot = 0;
            int removeAmount = 0;
            for (Ingredient ingredient1 : recipe.getItems()) {
                if (ingredient.amount >= ingredient1.amount && ingredient.itemType == ingredient1.itemType) {
                    slot = set.getKey();
                    removeAmount = ingredient1.amount;
                    break;
                }
            }
            Logger.info("Removing item at slot: " + slot);
            player.inventory.removeItem(slot, removeAmount);
        }

        Item outputItem = recipe.getOutputItem();
        player.inventory.pickupItem(outputItem);

        player.sendInventoryUpdate();
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
