package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;

public class SendClientToastPacket extends Packet {
    public SendClientToastPacket(ParsedPacket packetData) {
        super(PacketType.SEND_TOAST.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        Game.alertSystem.createToast(this.parsedPacket.content.get("toastContent").toString());
    }

    @Override
    public PacketDirection getDirection() {
        return PacketDirection.CLIENTBOUND;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
