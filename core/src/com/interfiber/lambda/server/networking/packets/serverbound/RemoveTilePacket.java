package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.client.tiles.Tile;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.TileRemovedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class RemoveTilePacket extends Packet {
    public RemoveTilePacket(ParsedPacket packetData) {
        super(PacketType.REMOVE_TILE.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {

        if (parsedPacket.content.get("xPosition") == null || parsedPacket.content.get("yPosition") == null) {
            Logger.error("Invalid remove tile packet");
            return;
        }

        int x = (int) parsedPacket.content.get("xPosition");
        int y = (int) parsedPacket.content.get("yPosition");
        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);
        assert player != null;


        Tile tile = TileLoader.getTile(MultiplayerGame.getWorldForPlayer(player).getUserTileAtPosition(x, y).type);
        boolean canDestroy = tile.onDestroy();

        if (!canDestroy) {
            Logger.error("Cannot destroy that tile!");
            return;
        }

        // give items
        tile.getLoot().giveToPlayer(player.inventory, player.selectedInventory);

        player.sendInventoryUpdate();

        MultiplayerGame.getWorldForPlayer(player).deleteUserPlacedTile(x, y);

        ParsedPacket payload = new ParsedPacket();

        payload.content.put("xPosition", x);
        payload.content.put("yPosition", y);

        // send packet to every body in current world


        for (int i = 0; i < MultiplayerGame.players.size(); i++) {
            OnlinePlayer target = MultiplayerGame.players.get(i);
            if (!Objects.equals(target.authUUID, this.parsedPacket.guard.authToken) && target.currentWorld == player.currentWorld) {
                new TileRemovedPacket(payload).writeData(target.serverConnection);
            }
        }
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
