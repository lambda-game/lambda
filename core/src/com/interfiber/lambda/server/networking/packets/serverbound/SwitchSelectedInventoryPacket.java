package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class SwitchSelectedInventoryPacket extends Packet {
    public SwitchSelectedInventoryPacket(ParsedPacket packetData) {
        super(PacketType.SWITCH_SELECTED_INVENTORY.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {

        int selectedInventory = 0;

        if (this.parsedPacket.content.get("selectedInventory").toString() == null) {
            Logger.error("Invalid packet, no selectedInventory found");
            return;
        }
        selectedInventory = Integer.parseInt(this.parsedPacket.content.get("selectedInventory").toString());

        for (OnlinePlayer player : MultiplayerGame.players) {
            if (Objects.equals(player.authUUID, this.parsedPacket.guard.authToken)) {
                Logger.info("Updating selected inventory to " + selectedInventory);
                player.selectedInventory = selectedInventory;
                break;
            }
        }
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
