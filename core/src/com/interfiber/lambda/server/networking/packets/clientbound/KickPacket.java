package com.interfiber.lambda.server.networking.packets.clientbound;

import com.badlogic.gdx.Gdx;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.screens.MainMenuScreen;
import com.interfiber.lambda.client.world.WorldType;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class KickPacket extends Packet {
    public KickPacket(ParsedPacket packetData) {
        super(PacketType.KICKED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        Logger.warn("Kicked from server");

        String kickMsg = this.parsedPacket.content.get("kickMessage").toString();

        Gdx.app.postRunnable(() -> {
            Game.screenManager.switchScene(new MainMenuScreen());
            Game.currentWorld = WorldType.UNKNOWN;

            Game.alertSystem.createToast("Kick from server:\n" + kickMsg);
            MultiplayerGameState.serverConnection.close();
        });
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
