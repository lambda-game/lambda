package com.interfiber.lambda.server.networking.packets.clientbound;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.interfiber.lambda.client.items.SerializableItem;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.concurrent.ConcurrentHashMap;

public class PlayerInventoryUpdatePacket extends Packet {
    public PlayerInventoryUpdatePacket(ParsedPacket packetData) {
        super(PacketType.INVENTORY_UPDATE.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        Logger.info("Updating inventory");

        String inventoryContent = this.parsedPacket.content.get("inventoryContent").toString();

        ObjectMapper mapper = new ObjectMapper();
        TypeFactory factory = mapper.getTypeFactory();

        try {
            ConcurrentHashMap<Integer, SerializableItem> content = mapper.readValue(inventoryContent, factory.constructMapType(ConcurrentHashMap.class, Integer.class, SerializableItem.class));
            Player.inventory.setInventoryContent(content);
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to parse packet");
            throw new IllegalStateException("Failed to parse packet");
        }
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
