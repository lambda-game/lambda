package com.interfiber.lambda.server.networking.packets;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public abstract class Packet {
    public int packetId;
    public ParsedPacket parsedPacket;

    public Packet(int id, ParsedPacket packetData) {
        packetId = id;
        parsedPacket = packetData;
    }

    public abstract void handlePacket(Channel c);

    public abstract PacketDirection getDirection();

    public abstract boolean requiresAuth();

    public void handler(Channel c) {
        if (this.requiresAuth()) {
            // check auth
            String auth = parsedPacket.guard.authToken;
            if (MultiplayerGame.getTokenOwner(auth) != null) {
                this.handlePacket(c);
            } else {
                Logger.error("Token " + auth + " is not a valid token!");
                // TODO kick player
            }
        } else {
            this.handlePacket(c);
        }
    }

    public void writeData(Channel con) {
        parsedPacket.packetId = packetId;
        try {
            con.writeAndFlush(new ObjectMapper().writeValueAsString(parsedPacket) + "\n");
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Failed to write packet content");
        }
    }

    public enum PacketType {
        INVALID(-1),
        LOGIN(0),
        PLAY(1),
        PLAYER_ADDED(2),
        PLAYER_POSITION_UPDATE(3),
        PLAYER_POSITION_UPDATED_BROADCAST(4),
        PLAYER_REMOVED(5),
        SEND_TOAST(6),
        PLACE_TILE(7),
        TILE_PLACED(8),
        REMOVE_TILE(9),
        TILE_REMOVED(10),
        KICKED(11),
        INVENTORY_UPDATE(12),
        SWITCH_SELECTED_INVENTORY(13),
        SWITCH_SELECTED_INVENTORY_SLOT(14),
        CRAFT_RECIPE(15),
        SEND_CHAT_MESSAGE(16),
        CHAT_MESSAGE(17),
        SYSTEM_CHAT_MESSAGE(18),
        UPDATED_WEATHER(19),
        ADD_ITEM_TO_CHEST(20),
        REMOVE_ITEM_FROM_CHEST(21),
        CHEST_UPDATED(22),
        SWITCH_WORLD(23),
        SWITCHED_WORLD(24),
        PLAYER_SWITCHED_WORLD(25),
        STORAGE_CONTAINER_PLACED(26),
        HOE_SOIL(27),
        SOIL_HOED(28),
        PLANT_CROP(29),
        CROP_PLANTED(30),
        CROP_GROWN(31),
        WATER_SOIL(32),
        SOIL_WATERED(33),
        HARVEST_CROP(34),
        CROP_HARVESTED(35),
        TIME_UPDATED(36);

        private final int id;

        PacketType(int i) {
            this.id = i;
        }

        public int getId() {
            return id;
        }
    }
}
