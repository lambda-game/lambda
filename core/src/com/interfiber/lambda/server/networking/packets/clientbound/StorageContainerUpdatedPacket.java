package com.interfiber.lambda.server.networking.packets.clientbound;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.items.SerializableItem;
import com.interfiber.lambda.client.world.StorageContainerWorldTile;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;

import java.util.concurrent.ConcurrentHashMap;

public class StorageContainerUpdatedPacket extends Packet {
    public StorageContainerUpdatedPacket(ParsedPacket packetData) {
        super(PacketType.CHEST_UPDATED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {

        int chestX = (int) this.parsedPacket.content.get("chestX");
        int chestY = (int) this.parsedPacket.content.get("chestY");
        String chestContentString = this.parsedPacket.content.get("chestContent").toString();

        ObjectMapper mapper = new ObjectMapper();
        TypeFactory typeFactory = mapper.getTypeFactory();
        ConcurrentHashMap<Integer, SerializableItem> chestContent;


        try {
            chestContent = mapper.readValue(chestContentString, typeFactory.constructMapType(ConcurrentHashMap.class, Integer.class, SerializableItem.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        StorageContainerWorldTile tile = (StorageContainerWorldTile) Game.getCurrentWorld().getUserTileAtPosition(chestX, chestY);
        Game.getCurrentWorld().getWorldData().remove(tile);
        tile.contents = chestContent;
        Game.getCurrentWorld().getWorldData().add(tile);
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
