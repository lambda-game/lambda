package com.interfiber.lambda.server.networking.packets.clientbound;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.lighting.LightSource;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class TilePlacedPacket extends Packet {
    public TilePlacedPacket(ParsedPacket packetData) {
        super(PacketType.TILE_PLACED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        WorldTile tile = null;
        try {
            tile = new ObjectMapper().readValue(this.parsedPacket.content.get("tile").toString(), WorldTile.class);
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to parse tile info");
            System.exit(-1);
        }

        Game.getCurrentWorld().getWorldData().add(tile);


        // check for light sources
        if (TileLoader.getTile(tile.type) instanceof LightSource) {
            LightSource source = (LightSource) TileLoader.getTile(tile.type);
            source.createLight(tile.x, tile.y);
        }

    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
