package com.interfiber.lambda.server.networking.packets.serverbound;

import com.badlogic.gdx.math.Vector2;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.StorageContainerWorldTile;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.StorageContainerPlacedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.TilePlacedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class PlaceTilePacket extends Packet {
    public PlaceTilePacket(ParsedPacket packetData) {
        super(PacketType.PLACE_TILE.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        // check
        if (this.parsedPacket.content.get("tile") == null) {
            Logger.info("Invalid place tile packet");
            return;
        }

        WorldTile tile;
        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);
        assert player != null;
        ParsedPacket payload = new ParsedPacket();

        try {
            tile = new ObjectMapper().readValue(this.parsedPacket.content.get("tile").toString(), WorldTile.class);

            if (Vector2.dst(player.x, player.y, tile.x, tile.y) > 241) {
                MultiplayerGame.kickPlayer(player, "Cannot reach");
                return;
            }

            // override values
            tile.collider = null;
            tile.userPlaced = true;

            payload.content.put("tile", new ObjectMapper().writeValueAsString(tile));
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to parse packet");
            return;
        }

        if (tile.type == TileType.CHEST) {
            // place storage container
            StorageContainerWorldTile storageContainerWorldTile = new StorageContainerWorldTile();
            storageContainerWorldTile.contents = new ConcurrentHashMap<>();
            storageContainerWorldTile.x = tile.x;
            storageContainerWorldTile.y = tile.y;
            storageContainerWorldTile.userPlaced = true;
            storageContainerWorldTile.collider = null;
            storageContainerWorldTile.type = tile.type;

            MultiplayerGame.getWorldForPlayer(player).getWorldData().add(storageContainerWorldTile);

            player.inventory.removeItem(player.selectedInventorySlot, 1);
            player.sendInventoryUpdate();

            ParsedPacket storageContainerPayload = new ParsedPacket();
            try {
                storageContainerPayload.content.put("storageContainer", new ObjectMapper().writeValueAsString(storageContainerWorldTile));
            } catch (JsonProcessingException e) {
                Logger.error(e);
                Logger.error("Failed to format storage container placed packet");
                return;
            }

            for (int i = 0; i < MultiplayerGame.players.size(); i++) {
                OnlinePlayer target = MultiplayerGame.players.get(i);
                if (!Objects.equals(target.authUUID, this.parsedPacket.guard.authToken) && target.currentWorld == player.currentWorld) {
                    new StorageContainerPlacedPacket(storageContainerPayload).writeData(target.serverConnection);
                }
            }

        } else {

            MultiplayerGame.getWorldForPlayer(player).placeTile(tile.x, tile.y, tile.type.toString().toLowerCase(), true);

            player.inventory.removeItem(player.selectedInventorySlot, 1);
            player.sendInventoryUpdate();

            // only send the packet to people who are in the current world as the player

            for (int i = 0; i < MultiplayerGame.players.size(); i++) {
                OnlinePlayer target = MultiplayerGame.players.get(i);
                if (!Objects.equals(target.authUUID, this.parsedPacket.guard.authToken) && target.currentWorld == player.currentWorld) {
                    new TilePlacedPacket(payload).writeData(target.serverConnection);
                }
            }
        }
    }

    @Override
    public PacketDirection getDirection() {
        return PacketDirection.SERVERBOUND;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
