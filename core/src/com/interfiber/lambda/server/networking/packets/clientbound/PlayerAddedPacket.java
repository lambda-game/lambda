package com.interfiber.lambda.server.networking.packets.clientbound;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.ClientPlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class PlayerAddedPacket extends Packet {
    public PlayerAddedPacket(ParsedPacket packetData) {
        super(PacketType.PLAYER_ADDED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        // add the player
        String playerJson = parsedPacket.content.get("player").toString();
        if (playerJson == null) {
            throw new IllegalStateException("Player info packet is empty");
        }
        ClientPlayer player;
        try {
            player = new ObjectMapper().readValue(playerJson, ClientPlayer.class);
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Failed to parse packet");
            throw new IllegalStateException("Could not parse packet");
        }

        if (!Objects.equals(player.playerUUID, MultiplayerGameState.player.playerUUID)) {
            MultiplayerGameState.players.add(player);
            Logger.info(player.username + " joined the game");
        }
    }

    @Override
    public PacketDirection getDirection() {
        return PacketDirection.CLIENTBOUND;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
