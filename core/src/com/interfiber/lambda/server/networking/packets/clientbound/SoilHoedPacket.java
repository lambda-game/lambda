package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.farming.HoeUtils;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;

public class SoilHoedPacket extends Packet {
    public SoilHoedPacket(ParsedPacket packetData) {
        super(PacketType.SOIL_HOED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        int x = (int) this.parsedPacket.content.get("tileX");
        int y = (int) this.parsedPacket.content.get("tileY");
        HoeUtils.hoeSoil(Game.getCurrentWorld().getTileAtPosition(x, y));
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
