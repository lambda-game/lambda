package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.farming.CropType;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.CropPlantedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class PlantCropPacket extends Packet {
    public PlantCropPacket(ParsedPacket packetData) {
        super(PacketType.PLANT_CROP.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {

        if (this.parsedPacket.content.get("cropX") == null || this.parsedPacket.content.get("cropY") == null || this.parsedPacket.content.get("cropType") == null) {
            Logger.error("Invalid plant crop packet, missing cropX, cropY, or cropType");
            return;
        }

        Logger.info("Planting crop");

        int cropX = Integer.parseInt(this.parsedPacket.content.get("cropX").toString());
        int cropY = Integer.parseInt(this.parsedPacket.content.get("cropY").toString());


        if (CropManager.getCropAtPosition(cropX, cropY) != null) {
            Logger.error("Crop already at position");
            return;
        }

        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);
        assert player != null; // pls go away warnings :)

        WorldTile tile = MultiplayerGame.getWorldForPlayer(player).getTileAtPosition(cropX, cropY);

        boolean autoWater = false;

        if (tile.type == TileType.WATERED_FARMLAND) {
            autoWater = true;
        } else if (tile.type != TileType.FARMLAND) {
            Logger.error("Invalid tile type");
            return;
        }


        CropType cropType = CropType.valueOf(this.parsedPacket.content.get("cropType").toString());

        CropManager.getCropData().add(CropManager.cropTypeToCrop(cropType).getSerializableCrop(cropX, cropY));

        if (autoWater) {
            CropManager.setCropAtPositionToBeWatered(cropX, cropY);
        }


        player.inventory.removeItem(player.selectedInventorySlot, 1);
        player.sendInventoryUpdate();


        ParsedPacket packet = new ParsedPacket();
        packet.content.put("cropType", cropType);
        packet.content.put("cropX", cropX);
        packet.content.put("cropY", cropY);


        for (int i = 0; i < MultiplayerGame.players.size(); i++) {
            OnlinePlayer target = MultiplayerGame.players.get(i);
            if (target.currentWorld == player.currentWorld) {
                new CropPlantedPacket(packet).writeData(target.serverConnection);
            }
        }

    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
