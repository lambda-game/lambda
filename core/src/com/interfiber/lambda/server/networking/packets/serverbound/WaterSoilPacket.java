package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.SoilWateredPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class WaterSoilPacket extends Packet {
    public WaterSoilPacket(ParsedPacket packetData) {
        super(PacketType.WATER_SOIL.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {

        if (this.parsedPacket.content.get("tileX") == null || this.parsedPacket.content.get("tileY") == null) {
            Logger.error("Invalid water soil packet, missing tileX or tileY");
            return;
        }

        int tileX = Integer.parseInt(this.parsedPacket.content.get("tileX").toString());
        int tileY = Integer.parseInt(this.parsedPacket.content.get("tileY").toString());

        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);
        WorldTile tile = MultiplayerGame.getWorldForPlayer(player).getTileAtPosition(tileX, tileY);

        if (tile == null) {
            Logger.error("Invalid position: " + tileX + ", " + tileY);
            return;
        }


        if (CropManager.getCropAtPosition(tileX, tileY) == null) {
            Logger.error("No crop at position");
            return;
        }

        if (tile.type != TileType.FARMLAND) {
            Logger.error("Tile not farmland, tile type is: " + tile.type);
            return;
        }

        MultiplayerGame.getWorldForPlayer(player).deleteTile(tile.x, tile.y);
        tile.type = TileType.WATERED_FARMLAND;
        MultiplayerGame.placeTile(tile, player);

        CropManager.setCropAtPositionToBeWatered(tile.x, tile.y);

        ParsedPacket packet = new ParsedPacket();
        packet.content.put("tileX", tile.x);
        packet.content.put("tileY", tile.y);

        for (int i = 0; i < MultiplayerGame.players.size(); i++) {
            OnlinePlayer target = MultiplayerGame.players.get(i);
            if (!Objects.equals(target.authUUID, this.parsedPacket.guard.authToken) && target.currentWorld == player.currentWorld) {
                new SoilWateredPacket(packet).writeData(target.serverConnection);
            }
        }

    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
