package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.farming.SerializableCrop;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class CropGrownPacket extends Packet {
    public CropGrownPacket(ParsedPacket packetData) {
        super(PacketType.CROP_GROWN.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        int cropX = Integer.parseInt(this.parsedPacket.content.get("cropX").toString());
        int cropY = Integer.parseInt(this.parsedPacket.content.get("cropY").toString());


        SerializableCrop sCrop = CropManager.getSerializableCropAtPosition(cropX, cropY);

        CropManager.getCropData().remove(sCrop);
        sCrop.hasGrown = true;
        CropManager.getCropData().add(sCrop);

        Logger.info("Crop grown!");
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
