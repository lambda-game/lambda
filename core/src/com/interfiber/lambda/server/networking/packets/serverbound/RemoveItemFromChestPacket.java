package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemUtils;
import com.interfiber.lambda.client.items.SerializableItem;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.StorageContainerWorldTile;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class RemoveItemFromChestPacket extends Packet {
    public RemoveItemFromChestPacket(ParsedPacket packetData) {
        super(PacketType.REMOVE_ITEM_FROM_CHEST.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {

        // check packet
        if (this.parsedPacket.content.get("chestX") == null || this.parsedPacket.content.get("chestY") == null || this.parsedPacket.content.get("index") == null) {
            Logger.error("Invalid remove item from chest packet, missing chestX, chestY, or index");
            return;
        }


        int chestX = Integer.parseInt(this.parsedPacket.content.get("chestX").toString());
        int chestY = Integer.parseInt(this.parsedPacket.content.get("chestY").toString());
        int index = Integer.parseInt(this.parsedPacket.content.get("index").toString());
        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);
        assert player != null;

        WorldTile tile = MultiplayerGame.getWorldForPlayer(player).getUserTileAtPosition(chestX, chestY);
        if (tile == null) {
            Logger.error("Invalid chest position: " + chestX + ", " + chestY + " got null when querying world");
            return;
        }

        if (tile.type != TileType.CHEST) {
            Logger.error("Cannot update tile with type: " + tile.type);
            return;
        }

        StorageContainerWorldTile storageContainerWorldTile = (StorageContainerWorldTile) tile;
        if (!storageContainerWorldTile.contents.containsKey(index)) {
            Logger.error("Invalid chest index");
            return;
        }

        SerializableItem item = storageContainerWorldTile.contents.get(index);
        storageContainerWorldTile.contents.remove(index);

        Item inventoryItem = ItemUtils.getItemFromId(item.type);
        if (inventoryItem == null) {
            Logger.error("Invalid item");
            return;
        }
        inventoryItem.amount = item.amount;

        player.inventory.pickupItem(inventoryItem);

        MultiplayerGame.getWorldForPlayer(player).deleteUserPlacedTile(chestX, chestY);
        MultiplayerGame.getWorldForPlayer(player).getWorldData().add(storageContainerWorldTile);
        MultiplayerGame.sendStorageContainerUpdated(chestX, chestY, storageContainerWorldTile.contents);
        player.sendInventoryUpdate();
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
