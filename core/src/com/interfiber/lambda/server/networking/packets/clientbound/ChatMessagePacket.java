package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.multiplayer.ClientChatMessage;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.ClientPlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class ChatMessagePacket extends Packet {
    public ChatMessagePacket(ParsedPacket packetData) {
        super(PacketType.CHAT_MESSAGE.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        String content = this.parsedPacket.content.get("messageContent").toString();
        boolean isPrivate = Boolean.parseBoolean(this.parsedPacket.content.get("isPrivate").toString());
        String fromPlayerUUID = this.parsedPacket.content.get("fromPlayer").toString();

        ClientPlayer fromPlayer = null;

        for (ClientPlayer player : MultiplayerGameState.players) {
            if (Objects.equals(player.playerUUID, fromPlayerUUID)) {
                fromPlayer = player;
                break;
            }
        }

        if (Objects.equals(fromPlayerUUID, MultiplayerGameState.player.playerUUID)) {
            fromPlayer = MultiplayerGameState.player.asClientPlayer();
        }

        if (fromPlayer == null) {
            Logger.error("Got message from unknown player");
            return;
        }

        Logger.info("<" + fromPlayer.username + "> " + content);
        ClientChatMessage message = new ClientChatMessage();
        message.content = content;
        message.isPrivate = isPrivate;
        message.fromPlayer = fromPlayer;

        if (Game.chatHistory.size() + 1 > 6) {
            Game.chatHistory.remove(0);
        }
        Game.chatHistory.add(message);
//        Game.alertSystem.createToast("<" + fromPlayer.username + "> " + content);

    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
