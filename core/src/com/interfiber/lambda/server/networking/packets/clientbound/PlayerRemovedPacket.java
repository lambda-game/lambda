package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.ClientPlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class PlayerRemovedPacket extends Packet {
    public PlayerRemovedPacket(ParsedPacket packetData) {
        super(PacketType.PLAYER_REMOVED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        String playerUUID = this.parsedPacket.content.get("playerUUID").toString();
        for (ClientPlayer player : MultiplayerGameState.players) {
            if (Objects.equals(player.playerUUID, playerUUID)) {
                MultiplayerGameState.players.remove(player);
                Logger.info(player.username + " left the game");
                break;
            }
        }
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
