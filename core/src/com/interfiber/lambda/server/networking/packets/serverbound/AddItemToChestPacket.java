package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.client.items.SerializableItem;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.StorageContainerWorldTile;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class AddItemToChestPacket extends Packet {
    public AddItemToChestPacket(ParsedPacket packetData) {
        super(PacketType.ADD_ITEM_TO_CHEST.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        // check packet
        if (this.parsedPacket.content.get("chestX").toString() == null || this.parsedPacket.content.get("chestY").toString() == null || this.parsedPacket.content.get("index").toString() == null) {
            Logger.error("Invalid add item to chest packet, missing chestX, chestY, or index");
            return;
        }

        int chestX = Integer.parseInt(this.parsedPacket.content.get("chestX").toString());
        int chestY = Integer.parseInt(this.parsedPacket.content.get("chestY").toString());
        int index = Integer.parseInt(this.parsedPacket.content.get("index").toString());
        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);
        assert player != null;

        SerializableItem item = player.inventory.getItems().get(index).getSerializableItem();

        if (!player.inventory.getItems().containsKey(index)) {
            Logger.error("Invalid index");
            return;
        }


        WorldTile tile = MultiplayerGame.getWorldForPlayer(player).getUserTileAtPosition(chestX, chestY);
        if (tile == null) {
            Logger.error("Invalid chest position: " + chestX + ", " + chestY + " got null when querying world");
            return;
        }

        if (tile.type != TileType.CHEST) {
            Logger.error("Cannot update tile with type: " + tile.type);
            return;
        }


        StorageContainerWorldTile storageContainerWorldTile = (StorageContainerWorldTile) tile;
        storageContainerWorldTile.contents.put(storageContainerWorldTile.contents.size() + 1, item);
        player.inventory.getItems().remove(index);

        MultiplayerGame.getWorldForPlayer(player).deleteUserPlacedTile(chestX, chestY);
        MultiplayerGame.getWorldForPlayer(player).getWorldData().add(storageContainerWorldTile);

        // create chest update packet
        MultiplayerGame.sendStorageContainerUpdated(chestX, chestY, storageContainerWorldTile.contents);
        player.sendInventoryUpdate();
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
