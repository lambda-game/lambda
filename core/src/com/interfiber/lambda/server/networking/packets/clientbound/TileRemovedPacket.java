package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.lighting.LightSource;
import com.interfiber.lambda.client.lighting.LightUtils;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;

public class TileRemovedPacket extends Packet {
    public TileRemovedPacket(ParsedPacket packetData) {
        super(PacketType.TILE_REMOVED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {

        int x = (int) parsedPacket.content.get("xPosition");
        int y = (int) parsedPacket.content.get("yPosition");

        WorldTile tile = Game.getCurrentWorld().getUserTileAtPosition(x, y);

        Game.getCurrentWorld().deleteUserPlacedTile(x, y);


        // check for light source
        if (TileLoader.getTile(tile.type) instanceof LightSource) {
            LightUtils.rebuildLighting();
        }
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
