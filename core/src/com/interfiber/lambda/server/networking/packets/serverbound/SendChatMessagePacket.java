package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.chat.ChatMessage;
import com.interfiber.lambda.server.commands.CommandManager;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.ChatMessagePacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class SendChatMessagePacket extends Packet {
    public SendChatMessagePacket(ParsedPacket packetData) {
        super(PacketType.SEND_CHAT_MESSAGE.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);
        assert player != null;

        // check packet

        if (this.parsedPacket.content.get("messageContent").toString() == null || this.parsedPacket.content.get("isCommand").toString() == null || this.parsedPacket.content.get("isPrivate").toString() == null || this.parsedPacket.content.get("toPlayer").toString() == null) {
            Logger.error("Invalid chat packet, missing messageContent, isCommand, isPrivate, or toPlayer");
            return;
        }

        String content = this.parsedPacket.content.get("messageContent").toString();
        boolean isCommand = Boolean.parseBoolean(this.parsedPacket.content.get("isCommand").toString());
        boolean isPrivate = Boolean.parseBoolean(this.parsedPacket.content.get("isPrivate").toString());

        if (isCommand) {
            CommandManager.executeCommand(content, player);
            return;
        }

        String toPlayer = this.parsedPacket.content.get("toPlayer").toString();

        ChatMessage message = new ChatMessage();
        message.content = content;
        message.isPrivate = isPrivate;
        message.toPlayerUUID = toPlayer;

        ParsedPacket packet = new ParsedPacket();
        packet.content.put("messageContent", content);
        packet.content.put("isPrivate", message.isPrivate);
        packet.content.put("fromPlayer", player.playerUUID);

        if (!message.isPrivate) {

            // only hold 25 chat messages at a time, to prevent memory from being leaked over time
            if (MultiplayerGame.chatHistory.size() + 1 > 25) {
                MultiplayerGame.chatHistory.remove(0);
            }

            MultiplayerGame.chatHistory.add(message);
            MultiplayerGame.sendPacketToPlayers(new ChatMessagePacket(packet));

            Logger.info("<" + player.username + "> " + content);
        } else {
            // get to player
            OnlinePlayer toOnlinePlayer = MultiplayerGame.getPlayerWithPlayerUUID(message.toPlayerUUID);
            if (toOnlinePlayer == null) {
                Logger.error("Invalid player!");
                return;
            }

            new ChatMessagePacket(packet).writeData(toOnlinePlayer.serverConnection);
        }
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
