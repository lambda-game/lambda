package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.weather.WeatherType;
import com.interfiber.lambda.client.weather.WeatherUtils;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class WeatherUpdatedPacket extends Packet {
    public WeatherUpdatedPacket(ParsedPacket packetData) {
        super(PacketType.UPDATED_WEATHER.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        Logger.info("Updating weather from server");
        WeatherType weatherType = WeatherType.valueOf(this.parsedPacket.content.get("weatherType").toString());
        Logger.info("Setting weather to: " + weatherType);

        WeatherUtils.setWeather(WeatherUtils.weatherTypeToWeather(weatherType));
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
