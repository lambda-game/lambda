package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.client.world.WorldType;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class SwitchWorldPacket extends Packet {
    public SwitchWorldPacket(ParsedPacket packetData) {
        super(PacketType.SWITCH_WORLD.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        if (this.parsedPacket.content.get("targetWorld") == null) {
            Logger.error("Invalid switch world packet, missing targetWorld");
            return;
        }

        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);
        WorldType targetWorld;
        try {
            targetWorld = WorldType.valueOf(this.parsedPacket.content.get("targetWorld").toString().toUpperCase());
        } catch (IllegalArgumentException e) {
            Logger.error(e);
            Logger.error("Invalid target world type");
            return;
        }
        assert player != null;


        // change world
        player.switchWorld(targetWorld, true, (int) MultiplayerGame.underworldSpawn.x, (int) MultiplayerGame.underworldSpawn.y);

        Logger.info(player.username + " has entered: " + targetWorld);
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
