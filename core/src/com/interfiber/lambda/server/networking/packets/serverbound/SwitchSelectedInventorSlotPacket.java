package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class SwitchSelectedInventorSlotPacket extends Packet {
    public SwitchSelectedInventorSlotPacket(ParsedPacket packetData) {
        super(PacketType.SWITCH_SELECTED_INVENTORY_SLOT.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {

        int selectedInventorySlot = 0;

        if (this.parsedPacket.content.get("selectedSlot").toString() == null) {
            Logger.error("Invalid packet, missing selectedSlot");
            return;
        }

        selectedInventorySlot = Integer.parseInt(this.parsedPacket.content.get("selectedSlot").toString());

        for (OnlinePlayer player : MultiplayerGame.players) {
            if (Objects.equals(player.authUUID, this.parsedPacket.guard.authToken)) {
                player.selectedInventorySlot = selectedInventorySlot;
            }
        }
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
