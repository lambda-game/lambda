package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.farming.CropType;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;

public class CropPlantedPacket extends Packet {
    public CropPlantedPacket(ParsedPacket packetData) {
        super(PacketType.CROP_PLANTED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        CropType cropType = CropType.valueOf(this.parsedPacket.content.get("cropType").toString());
        int cropX = Integer.parseInt(this.parsedPacket.content.get("cropX").toString());
        int cropY = Integer.parseInt(this.parsedPacket.content.get("cropY").toString());

        CropManager.getCropData().add(CropManager.cropTypeToCrop(cropType).getSerializableCrop(cropX, cropY));
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
