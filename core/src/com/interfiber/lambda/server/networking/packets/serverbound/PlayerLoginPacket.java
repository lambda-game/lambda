package com.interfiber.lambda.server.networking.packets.serverbound;

import com.badlogic.gdx.graphics.Color;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.player.PlayerInventory;
import com.interfiber.lambda.client.world.WorldType;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.auth.LambdaAuth;
import com.interfiber.lambda.server.auth.User;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.KickPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.PlayPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.PlayerAddedPacket;
import com.interfiber.lambda.server.player.ClientPlayer;
import com.interfiber.lambda.server.player.OnlinePlayer;
import com.interfiber.lambda.server.player.PlayerDataSave;
import com.interfiber.lambda.server.player.PlayerSaveDataManager;
import com.interfiber.lambda.server.world.WorldLoader;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class PlayerLoginPacket extends Packet {
    public PlayerLoginPacket(ParsedPacket packetData) {
        super(PacketType.LOGIN.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel channel) {

        // add new player
        Logger.info("Authenticating with auth server");
        if (this.parsedPacket.content.get("authToken").toString() == null) {
            Logger.error("Invalid login packet, no authToken found!");
            return;
        }

        String username;
        String authToken;
        try {
            if (MultiplayerGame.doAuth) {
                User user = LambdaAuth.getUser(this.parsedPacket.content.get("authToken").toString());
                if (user == null) {
                    Logger.error("Got invalid user response, kicking player");

                    ParsedPacket packet = new ParsedPacket();
                    packet.content.put("kickMessage", "Invalid authentication token");
                    new KickPacket(packet).writeData(channel);
                    return;
                } else {
                    username = user.username;
                    authToken = user.authToken;
                }
            } else {
                username = String.valueOf(new Random().nextInt());
                authToken = UUID.randomUUID().toString();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


        OnlinePlayer player = new OnlinePlayer();
        player.username = username;
        player.authUUID = authToken;
        player.playerUUID = UUID.randomUUID().toString();
        player.x = (int) MultiplayerGame.overworldSpawn.x;
        player.y = (int) MultiplayerGame.overworldSpawn.y;

        PlayerDataSave dataSave = PlayerSaveDataManager.getPlayerInfo(username);
        if (dataSave != null) {
            player.x = dataSave.playerX;
            player.y = dataSave.playerY;
            player.currentWorld = dataSave.currentWorld;
        }

        ParsedPacket response = new ParsedPacket();
        parsedPacket.initGuard();
        response.packetId = PacketType.PLAY.getId();
        try {
            response.content.put("player", new ObjectMapper().writeValueAsString(player));
            response.content.put("overworldData", WorldLoader.getOverworldDataAsJson(true));
            response.content.put("overworldContainers", WorldLoader.getOverworldStorageContainersJson());
            response.content.put("weatherType", MultiplayerGame.weatherType);
            response.content.put("cropData", new ObjectMapper().writeValueAsString(CropManager.getCropData()));
            response.content.put("currentTime", MultiplayerGame.time);

            List<ClientPlayer> clientPlayerList = new ArrayList<>();

            for (OnlinePlayer player1 : MultiplayerGame.players) {
                clientPlayerList.add(player1.asClientPlayer());
            }
            response.content.put("playerList", new ObjectMapper().writeValueAsString(clientPlayerList));

        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to format packet!");
            throw new IllegalStateException("Packet formatting failed");
        }
        player.serverConnection = channel;
        player.inventory = new PlayerInventory();

        if (dataSave != null) {
            player.inventory.loadItems(dataSave.playerItems);
            player.sendInventoryUpdate();
        }

        MultiplayerGame.addPlayer(player);

        if (dataSave != null) {
            MultiplayerGame.setCaveEntranceForPlayer(player, dataSave.caveEntranceX, dataSave.caveEntranceY);
        }
        new PlayPacket(response).writeData(channel);

        ParsedPacket announceResponse = new ParsedPacket();
        announceResponse.initGuard();

        ClientPlayer clientPlayer = player.asClientPlayer();

        try {
            announceResponse.content.put("player", new ObjectMapper().writeValueAsString(clientPlayer));
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Failed to format packet!");
            throw new IllegalStateException("Packet formatting failed");
        }

        // send packet to all players announcing join
        MultiplayerGame.sendPacketToPlayers(new PlayerAddedPacket(announceResponse));

        Logger.info(player.username + " has joined the game");
        MultiplayerGame.sendSystemMessage(player.username + " has joined the game", Color.YELLOW);

        // switch world
        if (player.currentWorld != WorldType.OVERWORLD) {
            player.switchWorld(player.currentWorld, false, player.x, player.y);
        }
    }

    @Override
    public PacketDirection getDirection() {
        return PacketDirection.SERVERBOUND;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
