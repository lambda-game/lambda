package com.interfiber.lambda.server.networking.packets;

import com.interfiber.lambda.server.networking.PacketGuard;

import java.util.HashMap;

public class ParsedPacket {
    public int packetId = -1;
    public PacketGuard guard = new PacketGuard();
    public HashMap<Object, Object> content = new HashMap<>();

    public void initGuard() {
        this.guard.sendTime = System.currentTimeMillis();
    }

    public void initGuard(String auth) {
        initGuard();
        this.guard.authToken = auth;
    }
}
