package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;

public class SoilWateredPacket extends Packet {
    public SoilWateredPacket(ParsedPacket packetData) {
        super(PacketType.SOIL_WATERED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        int tileX = Integer.parseInt(this.parsedPacket.content.get("tileX").toString());
        int tileY = Integer.parseInt(this.parsedPacket.content.get("tileY").toString());
        WorldTile tile = Game.getCurrentWorld().getTileAtPosition(tileX, tileY);

        Game.getCurrentWorld().getWorldData().remove(tile);
        tile.type = TileType.WATERED_FARMLAND;
        Game.getCurrentWorld().getWorldData().add(tile);
        CropManager.setCropAtPositionToBeWatered(tile.x, tile.y);
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
