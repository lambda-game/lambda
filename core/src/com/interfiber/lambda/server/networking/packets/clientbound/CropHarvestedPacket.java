package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.farming.SerializableCrop;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;

public class CropHarvestedPacket extends Packet {
    public CropHarvestedPacket(ParsedPacket packetData) {
        super(PacketType.CROP_HARVESTED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {

        SerializableCrop crop = CropManager.getSerializableCropAtPosition((int) this.parsedPacket.content.get("cropX"), (int) this.parsedPacket.content.get("cropY"));

        CropManager.getCropData().remove(crop);
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
