package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;

public class TimeUpdatedPacket extends Packet {
    public TimeUpdatedPacket(ParsedPacket packetData) {
        super(PacketType.TIME_UPDATED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        Game.time = Time.valueOf(this.parsedPacket.content.get("time").toString());
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
