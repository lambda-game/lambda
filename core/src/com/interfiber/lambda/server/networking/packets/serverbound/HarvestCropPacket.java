package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.farming.SerializableCrop;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.weather.WeatherType;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.CropHarvestedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class HarvestCropPacket extends Packet {
    public HarvestCropPacket(ParsedPacket packetData) {
        super(PacketType.HARVEST_CROP.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {

        if (this.parsedPacket.content.get("cropX") == null || this.parsedPacket.content.get("cropY") == null) {
            Logger.error("Invalid harvest crop packet, missing cropX, or cropY");
            return;
        }

        int cropX = Integer.parseInt(this.parsedPacket.content.get("cropX").toString());
        int cropY = Integer.parseInt(this.parsedPacket.content.get("cropY").toString());
        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);

        if (CropManager.getCropAtPosition(cropX, cropY) == null) {
            Logger.info("No crop at that position");
            return;
        }

        SerializableCrop sCrop = CropManager.getSerializableCropAtPosition(cropX, cropY);

        if (sCrop.hasGrown) {
            CropManager.removeCropAtPosition(sCrop.x, sCrop.y);

            assert player != null;

            player.inventory.pickupItem(CropManager.cropTypeToCrop(sCrop.cropType).getProduce());
            player.sendInventoryUpdate();

            WorldTile tile = MultiplayerGame.getWorldForPlayer(player).getTileAtPosition(cropX, cropY);

            if (MultiplayerGame.weatherType == null || MultiplayerGame.weatherType == WeatherType.CLEAR) {
                MultiplayerGame.getWorldForPlayer(player).deleteTile(tile.x, tile.y);
                tile.type = TileType.FARMLAND;
                MultiplayerGame.getWorldForPlayer(player).getWorldData().add(tile);
            }
        }

        ParsedPacket packet = new ParsedPacket();
        packet.content.put("cropX", cropX);
        packet.content.put("cropY", cropY);

        for (int i = 0; i < MultiplayerGame.players.size(); i++) {
            OnlinePlayer target = MultiplayerGame.players.get(i);
            if (!Objects.equals(target.authUUID, this.parsedPacket.guard.authToken) && target.currentWorld == player.currentWorld) {
                new CropHarvestedPacket(packet).writeData(target.serverConnection);
            }
        }
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
