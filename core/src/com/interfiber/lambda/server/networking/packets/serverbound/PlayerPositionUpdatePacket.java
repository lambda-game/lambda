package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.PlayerPositionUpdateBroadcastPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class PlayerPositionUpdatePacket extends Packet {
    public PlayerPositionUpdatePacket(ParsedPacket packetData) {
        super(PacketType.PLAYER_POSITION_UPDATE.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {


        String x = parsedPacket.content.get("xPosition").toString();
        String y = parsedPacket.content.get("yPosition").toString();
        String facing = parsedPacket.content.get("facing").toString();
        String swimming = parsedPacket.content.get("isSwimming").toString();

        if (x == null || y == null || swimming == null) {
            Logger.error("Failed to update position, x or y not provided");
            return;
        }

        for (OnlinePlayer player : MultiplayerGame.players) {
            if (Objects.equals(player.authUUID, parsedPacket.guard.authToken)) {
                player.x = Integer.parseInt(x);
                player.y = Integer.parseInt(y);
                player.facing = facing;
                player.isSwimming = Boolean.parseBoolean(swimming);

                // send position update packet to all players
                ParsedPacket responsePacket = new ParsedPacket();
                responsePacket.initGuard();

                responsePacket.packetId = PacketType.PLAYER_POSITION_UPDATED_BROADCAST.getId();

                responsePacket.content.put("playerUUID", player.playerUUID);
                responsePacket.content.put("xPosition", player.x);
                responsePacket.content.put("yPosition", player.y);
                responsePacket.content.put("facing", player.facing);
                responsePacket.content.put("isSwimming", player.isSwimming);

                MultiplayerGame.sendPacketToPlayers(new PlayerPositionUpdateBroadcastPacket(responsePacket));

                break;
            }
        }
    }

    @Override
    public PacketDirection getDirection() {
        return PacketDirection.SERVERBOUND;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
