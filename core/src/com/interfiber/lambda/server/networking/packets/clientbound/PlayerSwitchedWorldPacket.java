package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.world.WorldType;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.ClientPlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class PlayerSwitchedWorldPacket extends Packet {
    public PlayerSwitchedWorldPacket(ParsedPacket packetData) {
        super(PacketType.PLAYER_SWITCHED_WORLD.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        String playerUUID = this.parsedPacket.content.get("playerUUID").toString();
        if (Objects.equals(playerUUID, MultiplayerGameState.player.playerUUID)) {
            return;
        }
        WorldType newWorld = WorldType.valueOf(this.parsedPacket.content.get("worldType").toString().toUpperCase());

        for (ClientPlayer player : MultiplayerGameState.players) {
            if (Objects.equals(player.playerUUID, playerUUID)) {
                player.currentWorld = newWorld;

                Logger.info(player.username + " entered: " + newWorld);

                break;
            }
        }
    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
