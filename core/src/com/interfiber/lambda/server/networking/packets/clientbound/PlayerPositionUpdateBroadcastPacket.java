package com.interfiber.lambda.server.networking.packets.clientbound;

import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.ClientPlayer;
import io.netty.channel.Channel;

import java.util.Objects;

public class PlayerPositionUpdateBroadcastPacket extends Packet {

    public PlayerPositionUpdateBroadcastPacket(ParsedPacket packetData) {
        super(PacketType.PLAYER_POSITION_UPDATED_BROADCAST.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        // update position
        String playerUUID = parsedPacket.content.get("playerUUID").toString();
        if (MultiplayerGameState.player == null) {
            return;
        }
        if (Objects.equals(playerUUID, MultiplayerGameState.player.playerUUID)) {
            return;
        }
        int x = Integer.parseInt(parsedPacket.content.get("xPosition").toString());
        int y = Integer.parseInt(parsedPacket.content.get("yPosition").toString());
        String facing = parsedPacket.content.get("facing").toString();
        boolean swimming = (boolean) parsedPacket.content.get("isSwimming");

        for (ClientPlayer player : MultiplayerGameState.players) {
            if (Objects.equals(player.playerUUID, playerUUID)) {
                player.x = x;
                player.y = y;
                player.facing = facing;
                player.isSwimming = swimming;
                break;
            }
        }
    }

    @Override
    public PacketDirection getDirection() {
        return PacketDirection.CLIENTBOUND;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
