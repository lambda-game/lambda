package com.interfiber.lambda.server.networking.packets.serverbound;

import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.SoilHoedPacket;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.Objects;

public class HoeSoilPacket extends Packet {
    public HoeSoilPacket(ParsedPacket packetData) {
        super(PacketType.HOE_SOIL.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        // check packet

        if (this.parsedPacket.content.get("tileX") == null || this.parsedPacket.content.get("tileY") == null) {
            Logger.error("Invalid hoe soil packet, missing tileX, or tileY");
            return;
        }

        int tileX = Integer.parseInt(this.parsedPacket.content.get("tileX").toString());
        int tileY = Integer.parseInt(this.parsedPacket.content.get("tileY").toString());

        // check if soil is already hoed there
        OnlinePlayer player = MultiplayerGame.getTokenOwner(this.parsedPacket.guard.authToken);
        WorldTile worldTile = MultiplayerGame.getWorldForPlayer(player).getTileAtPosition(tileX, tileY);


        if (worldTile.type != TileType.GRASS) {
            Logger.info("worldTile.type != TileType.GRASS");
            return;
        }

        WorldTile farmTile = new WorldTile();
        farmTile.type = TileType.FARMLAND;
        farmTile.userPlaced = false;
        farmTile.x = worldTile.x;
        farmTile.y = worldTile.y;
        farmTile.collider = null;
        MultiplayerGame.getWorldForPlayer(player).deleteTile(worldTile.x, worldTile.y);
        MultiplayerGame.placeTile(farmTile, player);

        ParsedPacket packet = new ParsedPacket();
        packet.content.put("tileX", tileX);
        packet.content.put("tileY", tileY);

        for (int i = 0; i < MultiplayerGame.players.size(); i++) {
            OnlinePlayer target = MultiplayerGame.players.get(i);
            if (!Objects.equals(target.authUUID, this.parsedPacket.guard.authToken) && target.currentWorld == player.currentWorld) {
                new SoilHoedPacket(packet).writeData(target.serverConnection);
            }
        }

    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return true;
    }
}
