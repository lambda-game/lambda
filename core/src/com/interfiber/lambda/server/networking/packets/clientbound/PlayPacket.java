package com.interfiber.lambda.server.networking.packets.clientbound;

import com.badlogic.gdx.Gdx;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.farming.SerializableCrop;
import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.screens.GameOverworldScreen;
import com.interfiber.lambda.client.screens.SwitchingWorldScreen;
import com.interfiber.lambda.client.weather.WeatherType;
import com.interfiber.lambda.client.weather.WeatherUtils;
import com.interfiber.lambda.client.world.Overworld;
import com.interfiber.lambda.client.world.StorageContainerWorldTile;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.client.world.WorldType;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.player.ClientPlayer;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;
import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.List;

public class PlayPacket extends Packet {

    public PlayPacket(ParsedPacket packetData) {
        super(PacketType.PLAY.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {


        ObjectMapper mapper = new ObjectMapper();
        TypeFactory typeFactory = mapper.getTypeFactory();

        Logger.info("Loading players");
        Game.serverConnectStatus = "Loading players...";
        String playerInfo = this.parsedPacket.content.get("playerList").toString();

        try {
            List<ClientPlayer> players = mapper.readValue(playerInfo, typeFactory.constructCollectionType(List.class, ClientPlayer.class));
            MultiplayerGameState.players = (ArrayList<ClientPlayer>) players;
            Logger.info("Loaded players");
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed parse packet");
            System.exit(-1);
        }


        Logger.info("Setting user info");
        String userInfo = this.parsedPacket.content.get("player").toString();
        String worldDataJson = this.parsedPacket.content.get("overworldData").toString();

        if (userInfo == null) {
            throw new IllegalStateException("Expected player info json, got null");
        }

        OnlinePlayer player = null;

        try {
            player = new ObjectMapper().readValue(userInfo, OnlinePlayer.class);
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Failed to parse packet!");
            System.exit(-1);
        }
        MultiplayerGameState.player = player;

        Logger.info("Loaded player info");

        Game.serverConnectStatus = "Loading world...";
        List<WorldTile> worldData;
        List<StorageContainerWorldTile> overworldContainers;
        ArrayList<SerializableCrop> cropData;
        Time time = Time.valueOf(this.parsedPacket.content.get("currentTime").toString());

        try {
            worldData = mapper.readValue(worldDataJson, typeFactory.constructCollectionType(List.class, WorldTile.class));
            overworldContainers = mapper.readValue(this.parsedPacket.content.get("overworldContainers").toString(), typeFactory.constructCollectionType(List.class, StorageContainerWorldTile.class));
            cropData = mapper.readValue(this.parsedPacket.content.get("cropData").toString(), typeFactory.constructCollectionType(ArrayList.class, SerializableCrop.class));
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to parse world data");
            throw new IllegalStateException("World data could not be parsed!");
        }
        Game.overworld = new Overworld();
        Game.overworld.loadWorldData(worldData);

        CropManager.loadCropData(cropData);


        // load storage containers
        for (StorageContainerWorldTile tile : overworldContainers) {
            Game.overworld.getWorldData().add(tile);
        }


        OnlinePlayer finalPlayer = player;
        Gdx.app.postRunnable(() -> {
            Game.currentWorld = WorldType.OVERWORLD;
            Game.time = time;
            Game.screenManager.switchScene(new GameOverworldScreen());
            Player.x = MultiplayerGameState.player.x;
            Player.y = MultiplayerGameState.player.y;
            WeatherUtils.setWeather(WeatherUtils.weatherTypeToWeather(WeatherType.valueOf(this.parsedPacket.content.get("weatherType").toString()))); // )))(()()()9)9)()

            if (finalPlayer.currentWorld != WorldType.OVERWORLD) {
                Game.screenManager.switchScene(new SwitchingWorldScreen());
            }
        });
    }

    @Override
    public PacketDirection getDirection() {
        return PacketDirection.CLIENTBOUND;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
