package com.interfiber.lambda.server.networking.packets.clientbound;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.world.StorageContainerWorldTile;
import com.interfiber.lambda.server.networking.PacketDirection;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import io.netty.channel.Channel;
import org.tinylog.Logger;

public class StorageContainerPlacedPacket extends Packet {
    public StorageContainerPlacedPacket(ParsedPacket packetData) {
        super(PacketType.STORAGE_CONTAINER_PLACED.getId(), packetData);
    }

    @Override
    public void handlePacket(Channel c) {
        try {
            StorageContainerWorldTile storageContainerWorldTile = new ObjectMapper().readValue(this.parsedPacket.content.get("storageContainer").toString(), StorageContainerWorldTile.class);
            Game.getCurrentWorld().getWorldData().add(storageContainerWorldTile);
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to parse storage container placed packet");
        }

    }

    @Override
    public PacketDirection getDirection() {
        return null;
    }

    @Override
    public boolean requiresAuth() {
        return false;
    }
}
