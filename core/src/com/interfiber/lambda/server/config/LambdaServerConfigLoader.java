package com.interfiber.lambda.server.config;

import org.tinylog.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.stream.Collectors;

public class LambdaServerConfigLoader {

    private static final String configPath = "serverconfig.properties";
    public static Properties config = new Properties();

    public static void loadConfigFile(){

        try {
            createConfigFile();
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Failed to create config file");
            System.exit(-1);
        }

        try {
            config.load(Files.newInputStream(Paths.get(configPath)));
            Logger.info(config.getProperty("world-file"));
        } catch (Exception e){
            Logger.error(e);
            Logger.error("Failed to load server config");
            System.exit(-1);
        }
    }

    public static void createConfigFile() throws IOException {
        if (!new File(configPath).exists()){
            InputStream inputStream = LambdaServerConfigLoader.class.getClassLoader().getResourceAsStream("serverconfig.properties");
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line = reader.lines().collect(Collectors.joining(System.lineSeparator()));

            FileWriter fileWriter = new FileWriter(configPath);
            fileWriter.write(line);
            fileWriter.close();
        }
    }
}
