package com.interfiber.lambda.server.bootstrap;

import com.interfiber.lambda.client.bootstrap.RecipeBootstrap;
import com.interfiber.lambda.client.bootstrap.TileBootstrap;
import com.interfiber.lambda.client.ticks.TickableFarm;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.config.LambdaServerConfigLoader;
import com.interfiber.lambda.server.networking.MultiplayerServer;
import com.interfiber.lambda.server.networking.PacketManager;
import com.interfiber.lambda.server.world.ServerWeatherThread;
import com.interfiber.lambda.server.world.WorldLoader;
import org.tinylog.Logger;

import java.io.File;
import java.util.Arrays;

public class ServerBootstrap {
    public static void bootstrap() {

        PacketManager.init();

        Logger.info("Loading config");
        LambdaServerConfigLoader.loadConfigFile();

        if (!new File(LambdaServerConfigLoader.config.getProperty("world-file")).exists()){
            Logger.info("Generating world");
            WorldLoader.generateWorld(LambdaServerConfigLoader.config.getProperty("world-file"));
        }

        if (!Boolean.parseBoolean(LambdaServerConfigLoader.config.getProperty("do-auth"))){
            Logger.warn("!!!! DISABLING MULTIPLAYER AUTHENTICATION !!!!");
            MultiplayerGame.doAuth = false;
        }

        String opListString = LambdaServerConfigLoader.config.getProperty("ops");
        if (opListString != null) {
            MultiplayerGame.opList = opListString.split(",");
            Logger.info("Op list is: " + Arrays.toString(MultiplayerGame.opList));
        }

        // load world
        try {
            WorldLoader.loadWorld(LambdaServerConfigLoader.config.getProperty("world-file"));
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Failed to load world file!");
            System.exit(-1);
        }

        Logger.info("Loading tiles");
        TileBootstrap.bootstrap();

        Logger.info("Loading recipes");
        RecipeBootstrap.bootstrap();

        Logger.info("Loading weather");
        Thread weatherThread = new Thread(new ServerWeatherThread());
        weatherThread.setName("Server-Weather");
        weatherThread.start();

        Logger.info("Loading ticks");
        MultiplayerGame.tickManager.addTickSubscriber(new TickableFarm());

        Thread tickThread = new Thread(new TickRunnable());
        tickThread.setName("Server-Ticks");
        tickThread.start();


        Logger.info("Loading shutdown task");

        Thread shutdownThread = new Thread(new ShutdownRunnable());

        Runtime.getRuntime().addShutdownHook(shutdownThread);

        Logger.info("Loading commands");

        MultiplayerGame.server = new MultiplayerServer();

        try {
            MultiplayerGame.server.startServer();
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Error during server runtime");
        }
    }

    public static void main(String[] args) {
        bootstrap();
    }
}
