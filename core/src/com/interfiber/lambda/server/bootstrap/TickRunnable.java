package com.interfiber.lambda.server.bootstrap;

import com.badlogic.gdx.utils.TimeUtils;
import com.interfiber.lambda.server.MultiplayerGame;
import org.tinylog.Logger;

public class TickRunnable implements Runnable {
    private static long startTime = -1;

    @Override
    public void run() {
        while (true) {
            if (MultiplayerGame.shuttingDown) return;
            if (startTime == -1) {
                startTime = TimeUtils.millis();
            }
            try {
                if (TimeUtils.timeSinceMillis(startTime) > 1000) {
                    MultiplayerGame.tickManager.runTickEvents();
                    startTime = -1;
                }
            } catch (Exception e) {
                Logger.error(e);
                Logger.error("Error occurred while a game tick was in progress");
            }
        }
    }
}
