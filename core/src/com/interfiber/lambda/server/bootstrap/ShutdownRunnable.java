package com.interfiber.lambda.server.bootstrap;

import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.config.LambdaServerConfigLoader;
import com.interfiber.lambda.server.world.WorldLoader;
import org.tinylog.Logger;

import java.io.IOException;

public class ShutdownRunnable implements Runnable {
    @Override
    public void run() {
        MultiplayerGame.shuttingDown = true;
        Logger.info("Saving world...");
        try {
            WorldLoader.saveWorldFile(LambdaServerConfigLoader.config.getProperty("world-file"));
        } catch (IOException e) {
            Logger.error(e);
            Logger.error("Failed to save world to disk!");
        }
        Logger.info("Saved world");
    }
}
