package com.interfiber.lambda.server.chat;


import com.badlogic.gdx.graphics.Color;

public class ChatMessage {
    public String content = "";
    public boolean isPrivate = false;
    public boolean isSystem = false;
    public Color color = Color.WHITE;
    public String toPlayerUUID = "";
}
