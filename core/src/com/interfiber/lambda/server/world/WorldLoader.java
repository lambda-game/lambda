package com.interfiber.lambda.server.world;

import com.badlogic.gdx.math.Vector2;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.*;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.config.LambdaServerConfigLoader;
import org.tinylog.Logger;

import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class WorldLoader {

    public static void loadWorld(String path) throws IOException, ClassNotFoundException {
        Logger.info("Loading save file");
        ObjectInputStream in = new ObjectInputStream(
                Files.newInputStream(Paths.get(path)));
        GameSave save = (GameSave) in.readObject();
        MultiplayerGame.overworld = new Overworld();
        MultiplayerGame.overworld.loadWorldData(save.overworldData);

        MultiplayerGame.underworld = new Underworld();
        MultiplayerGame.underworld.loadWorldData(save.underworldData);

        MultiplayerGame.overworldSpawn.set(new Vector2(save.overworldX, save.overworldY));
        MultiplayerGame.underworldSpawn.set(new Vector2(save.underworldX, save.underworldY));

        CropManager.loadCropData(save.cropData);

        Logger.info("Loaded save file");
    }

    public static String getOverworldDataAsJson(boolean removeStorageContainers) {
        return getWorldDataAsJson(MultiplayerGame.overworld, removeStorageContainers);
    }

    public static String getOverworldStorageContainersJson() {
        return getWorldStorageContainersJson(MultiplayerGame.overworld);
    }

    public static String getUnderworldDataAsJson(boolean removeStorageContainers) {
        return getWorldDataAsJson(MultiplayerGame.underworld, removeStorageContainers);
    }

    public static String getUnderworldStorageContainersJson() {
        return getWorldStorageContainersJson(MultiplayerGame.underworld);
    }

    public static String getWorldDataAsJson(World world, boolean removeStorageContainers) {
        try {
            if (removeStorageContainers) {
                ArrayList<WorldTile> tiles = new ArrayList<>();
                for (WorldTile tile : world.getWorldData()) {
                    if (tile.type != TileType.CHEST) {
                        tiles.add(tile);
                    }
                }
                return new ObjectMapper().writeValueAsString(tiles);
            }
            return new ObjectMapper().writeValueAsString(world.getWorldData());
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to format world data json packet!");
            throw new IllegalStateException("Failed to format world data packet!");
        }
    }

    public static String getWorldStorageContainersJson(World world) {
        ArrayList<StorageContainerWorldTile> tiles = new ArrayList<>();
        for (WorldTile tile : world.getWorldData()) {
            if (tile.type == TileType.CHEST) {
                tiles.add((StorageContainerWorldTile) tile);
            }
        }

        try {
            return new ObjectMapper().writeValueAsString(tiles);
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to format storage container data packet");
            throw new IllegalStateException("Failed to format storage container data packet");
        }

    }

    public static void saveWorldFile(String destFile) throws IOException {
        File file = Paths.get(destFile).toFile();

        GameSave save = new GameSave();
        save.underworldData = MultiplayerGame.underworld.getWorldData();
        save.overworldData = MultiplayerGame.overworld.getWorldData();
        save.weather = MultiplayerGame.weatherType;
        save.overworldX = (int) MultiplayerGame.overworldSpawn.x;
        save.overworldY = (int) MultiplayerGame.overworldSpawn.y;

        save.underworldX = (int) MultiplayerGame.underworldSpawn.x;
        save.underworldY = (int) MultiplayerGame.underworldSpawn.y;

        save.cropData = CropManager.getCropData();

        ObjectOutputStream outputStream = new ObjectOutputStream(Files.newOutputStream(file.toPath()));
        outputStream.writeObject(save);
        outputStream.close();
    }

    public static void generateWorld(String file) {
        int seed = (int) System.currentTimeMillis();

        if (LambdaServerConfigLoader.config.getProperty("world-seed") != null){
            seed = Integer.parseInt(LambdaServerConfigLoader.config.getProperty("world-seed"));
        }
        Logger.info("Generating world");
        Logger.info("Generator seed: " + seed);

        GameSave saveFile = new GameSave();

        Overworld overworld = new Overworld();
        overworld.generate(seed);

        Underworld underworld = new Underworld();
        underworld.generate(seed);

        saveFile.underworldData = underworld.getWorldData();
        saveFile.overworldData = overworld.getWorldData();

        saveFile.time = Time.MORNING;

        saveFile.overworldX = (int) Game.overworldSpawnPoint.x;
        saveFile.overworldY = (int) Game.overworldSpawnPoint.y;

        saveFile.underworldX = (int) Game.underworldSpawnPoint.x;
        saveFile.underworldY = (int) Game.underworldSpawnPoint.y;

        Logger.info("Saving world file...");

        try {
            ObjectOutputStream out =
                    new ObjectOutputStream(Files.newOutputStream(Paths.get(file)));
            out.writeObject(saveFile);
            out.close();
        } catch (Exception e){
            Logger.error(e);
            Logger.error("Failed to save world");
            System.exit(-1);
        }
    }
}
