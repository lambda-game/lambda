package com.interfiber.lambda.server.world;

import com.badlogic.gdx.utils.TimeUtils;
import com.interfiber.lambda.client.weather.WeatherType;
import com.interfiber.lambda.server.MultiplayerGame;
import org.tinylog.Logger;

import java.util.Random;

public class ServerWeatherThread implements Runnable {

    private long startTime = 0;
    private Random random = new Random();

    @Override
    public void run() {
        if (startTime == 0) {
            startTime = TimeUtils.millis();
        }
        boolean updatedWeather = false;
        if (TimeUtils.timeSinceMillis(startTime) > 30000 && MultiplayerGame.weatherType == WeatherType.CLEAR) {
            startTime = TimeUtils.millis();
            Logger.info("Checking for weather updates");
            // thunderstorm?
            if (random.nextInt(40) == 20) {
                updatedWeather = true;
                // update weather on clients
                MultiplayerGame.weatherType = WeatherType.THUNDERSTORM;

                MultiplayerGame.sendWeatherUpdate();
            }
            Logger.info("Weather update status: " + updatedWeather);
        } else if (TimeUtils.timeSinceMillis(startTime) > 180000) {
            Logger.info("Clearing weather");
            // every 3 mins clear the weather
            startTime = TimeUtils.millis();
            MultiplayerGame.weatherType = WeatherType.CLEAR;
        }
    }
}
