package com.interfiber.lambda.server;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.client.items.SerializableItem;
import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.client.ticks.TickManager;
import com.interfiber.lambda.client.weather.WeatherType;
import com.interfiber.lambda.client.world.*;
import com.interfiber.lambda.server.chat.ChatMessage;
import com.interfiber.lambda.server.networking.MultiplayerServer;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.*;
import com.interfiber.lambda.server.player.OnlinePlayer;
import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class MultiplayerGame {
    public static MultiplayerServer server;
    public static int port = 7956;
    public static boolean doAuth = true;
    public static ArrayList<OnlinePlayer> players = new ArrayList<>();
    public static WorldType currentWorld = WorldType.OVERWORLD;
    public static Overworld overworld;
    public static Underworld underworld;
    public static Vector2 underworldSpawn = new Vector2();
    public static Vector2 overworldSpawn = new Vector2();
    public static ArrayList<ChatMessage> chatHistory = new ArrayList<>();
    public static WeatherType weatherType = WeatherType.CLEAR;
    public static String authServer = "https://lambda-auth-services.herokuapp.com"; // official auth server endpoint
    public static TickManager tickManager = new TickManager();
    public static boolean shuttingDown = false;
    public static Time time = Time.MORNING;
    public static String[] opList = new String[]{}; // array of player usernames who have OP

    public static void addPlayer(OnlinePlayer player) {
        players.add(player);
    }

    public static OnlinePlayer getPlayerWithPlayerUUID(String uuid) {
        for (int i = 0; players.size() > i; i++) {
            OnlinePlayer player = players.get(i);
            if (Objects.equals(player.playerUUID, uuid)) {
                return player;
            }
        }
        return null;
    }

    public static void sendPacketToPlayers(Packet packet) {
        for (OnlinePlayer player : players) {
            packet.writeData(player.serverConnection);
        }
    }

    public static void sendPacketToPlayers(Packet packet, OnlinePlayer exclude) {
        for (OnlinePlayer player : players) {
            if (!Objects.equals(player.playerUUID, exclude.playerUUID)) {
                packet.writeData(player.serverConnection);
            }
        }
    }

    public static OnlinePlayer getTokenOwner(String token) {
        for (int i = 0; players.size() > i; i++) {
            OnlinePlayer player = players.get(i);
            if (Objects.equals(player.authUUID, token)) {
                return player;
            }
        }
        return null;
    }


    public static World getWorldForPlayer(OnlinePlayer player) {
        if (player.currentWorld == WorldType.OVERWORLD) {
            return overworld;
        } else if (player.currentWorld == WorldType.UNDERWORLD) {
            return underworld;
        } else {
            Logger.error("Failed to get current world for player: " + player.username + "!");
            return null;
        }
    }

    public static void kickPlayer(OnlinePlayer player, String msg) {
        /*
          kick steps:

          1. send kick packet
          2. close player connection to server
          3. remove player from server player list
          4. send player removed packet to everybody
         */

        ParsedPacket kickPayload = new ParsedPacket();
        kickPayload.content.put("kickMessage", msg);
        new KickPacket(kickPayload).writeData(player.serverConnection);

        player.serverConnection.close();

        players.remove(player);

        ParsedPacket packet = new ParsedPacket();
        packet.content.put("playerUUID", player.playerUUID);

        sendPacketToPlayers(new PlayerRemovedPacket(packet));
        sendSystemMessage(player.username + " was kicked from the game", Color.RED);
    }

    public static void sendSystemMessage(String message, Color color) {
        ParsedPacket packet = new ParsedPacket();
        packet.content.put("messageContent", message);
        packet.content.put("color", color.toString());

        sendPacketToPlayers(new SystemChatMessagePacket(packet));
    }

    public static void sendSystemMessage(String message, Color color, OnlinePlayer target) {
        ParsedPacket packet = new ParsedPacket();
        packet.content.put("messageContent", message);
        packet.content.put("color", color.toString());


        new SystemChatMessagePacket(packet).writeData(target.serverConnection);
    }

    public static void sendWeatherUpdate() {
        ParsedPacket packet = new ParsedPacket();
        packet.content.put("weatherType", weatherType.toString());

        sendPacketToPlayers(new WeatherUpdatedPacket(packet));
    }

    public static OnlinePlayer getPlayerByUsername(String username) {
        for (int i = 0; players.size() > i; i++) {
            OnlinePlayer player = players.get(i);
            if (Objects.equals(player.username, username)) {
                return player;
            }
        }
        return null;
    }

    public static void sendStorageContainerUpdated(int chestX, int chestY, ConcurrentHashMap<Integer, SerializableItem> contents) {
        ParsedPacket packet = new ParsedPacket();
        packet.content.put("chestX", chestX);
        packet.content.put("chestY", chestY);
        try {
            packet.content.put("chestContent", new ObjectMapper().writeValueAsString(contents));
        } catch (JsonProcessingException e) {
            Logger.error(e);
            Logger.error("Failed to format packet contents");
            return;
        }

        sendPacketToPlayers(new StorageContainerUpdatedPacket(packet));
    }

    public static void setWorldForPlayer(OnlinePlayer player, WorldType targetWorld) {
        for (OnlinePlayer plyr : players) {
            if (plyr == player) {
                plyr.currentWorld = targetWorld;
                break;
            }
        }
    }

    public static void setCaveEntranceForPlayer(OnlinePlayer player, int x, int y) {
        for (OnlinePlayer plyr : players) {
            if (plyr == player) {
                plyr.caveEnterX = x;
                plyr.caveEnterY = y;
            }
        }
    }

    public static void placeTile(WorldTile tile, OnlinePlayer player) {
        if (player.currentWorld == WorldType.OVERWORLD) {
            overworld.getWorldData().add(tile);
        } else {
            underworld.getWorldData().add(tile);
        }
    }

    public static void sendTimeUpdate(){
        ParsedPacket timeUpdatePacket = new ParsedPacket();
        timeUpdatePacket.content.put("time", time);

        sendPacketToPlayers(new TimeUpdatedPacket(timeUpdatePacket));
    }
}