package com.interfiber.lambda.server.commands;

import com.interfiber.lambda.server.player.OnlinePlayer;
import org.tinylog.Logger;

import java.util.HashMap;

public abstract class Command {

    public HashMap<String, CommandArgument> arguments = new HashMap<>();

    public Command(String command) {
        String[] parts = command.replace("/" + getCommandName() + " ", "").split(" ");
        String[] argumentNames = getArgumentNames();

        if (parts.length != argumentNames.length) {
            Logger.error("Parser error: spliced command length does match amount of arguments");
            return;
        }

        for (int i = 0; i < parts.length; i++) {
            this.setArgumentValue(argumentNames[i], parts[i]);
        }
    }

    public abstract void execute(OnlinePlayer sender);

    public abstract String getCommandName();

    public abstract String[] getArgumentNames();

    public void setArgumentValue(String name, String value) {
        CommandArgument arg = new CommandArgument();
        arg.name = name;
        arg.value = value;

        this.arguments.put(name, arg);
    }
}
