package com.interfiber.lambda.server.commands.chatcommands;

import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.commands.Command;
import com.interfiber.lambda.server.player.OnlinePlayer;
import org.tinylog.Logger;

public class KickPlayerCommand extends Command {
    public KickPlayerCommand(String command) {
        super(command);
    }

    @Override
    public void execute(OnlinePlayer sender) {
        OnlinePlayer playerToKick = MultiplayerGame.getPlayerByUsername(this.arguments.get("playerName").value);
        if (playerToKick == null) {
            Logger.error("Invalid player to kick");
            return;
        }

        MultiplayerGame.kickPlayer(playerToKick, "Kicked by player");
    }

    @Override
    public String getCommandName() {
        return "kickplayer";
    }

    @Override
    public String[] getArgumentNames() {
        return new String[]{"playerName"};
    }
}
