package com.interfiber.lambda.server.commands.chatcommands;

import com.interfiber.lambda.client.items.ItemType;
import com.interfiber.lambda.client.items.ItemUtils;
import com.interfiber.lambda.server.commands.Command;
import com.interfiber.lambda.server.player.OnlinePlayer;

public class GiveItemCommand extends Command {
    public GiveItemCommand(String command) {
        super(command);
    }

    @Override
    public void execute(OnlinePlayer sender) {
        ItemType itemType = ItemType.valueOf(this.arguments.get("itemName").value.toUpperCase());

        sender.inventory.pickupItem(ItemUtils.getItemFromId(itemType));
        sender.sendInventoryUpdate();
    }

    @Override
    public String getCommandName() {
        return "giveitem";
    }

    @Override
    public String[] getArgumentNames() {
        return new String[]{"itemName"};
    }
}
