package com.interfiber.lambda.server.commands.chatcommands;

import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.commands.Command;
import com.interfiber.lambda.server.player.OnlinePlayer;

public class SetTimeCommand extends Command {
    public SetTimeCommand(String command) {
        super(command);
    }

    @Override
    public void execute(OnlinePlayer sender) {
        MultiplayerGame.time = Time.valueOf(this.arguments.get("time").value.toUpperCase());
        MultiplayerGame.sendTimeUpdate();
    }

    @Override
    public String getCommandName() {
        return "settime";
    }

    @Override
    public String[] getArgumentNames() {
        return new String[]{"time"};
    }
}
