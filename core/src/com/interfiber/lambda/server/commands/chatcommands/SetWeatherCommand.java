package com.interfiber.lambda.server.commands.chatcommands;

import com.interfiber.lambda.client.weather.WeatherType;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.commands.Command;
import com.interfiber.lambda.server.player.OnlinePlayer;
import org.tinylog.Logger;

public class SetWeatherCommand extends Command {
    public SetWeatherCommand(String command) {
        super(command);
    }

    @Override
    public void execute(OnlinePlayer sender) {
        Logger.info("Setting weather to: " + this.arguments.get("weather").value.toUpperCase());

        MultiplayerGame.weatherType = WeatherType.valueOf(this.arguments.get("weather").value.toUpperCase());
        MultiplayerGame.sendWeatherUpdate();
    }

    @Override
    public String getCommandName() {
        return "setweather";
    }

    @Override
    public String[] getArgumentNames() {
        return new String[]{"weather"};
    }
}
