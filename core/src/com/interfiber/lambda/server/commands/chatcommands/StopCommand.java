package com.interfiber.lambda.server.commands.chatcommands;

import com.badlogic.gdx.graphics.Color;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.commands.Command;
import com.interfiber.lambda.server.player.OnlinePlayer;
import org.tinylog.Logger;


public class StopCommand extends Command {
    public StopCommand(String command) {
        super(command);
    }

    @Override
    public void execute(OnlinePlayer sender) {
        Logger.info("Stopping server");
        MultiplayerGame.sendSystemMessage("Shutting down server", Color.YELLOW);
        System.exit(0);
    }

    @Override
    public String getCommandName() {
        return null;
    }

    @Override
    public String[] getArgumentNames() {
        return new String[0];
    }
}
