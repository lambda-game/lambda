package com.interfiber.lambda.server.commands;

import com.badlogic.gdx.graphics.Color;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.commands.chatcommands.*;
import com.interfiber.lambda.server.player.OnlinePlayer;
import org.tinylog.Logger;

import java.util.Objects;

public class CommandManager {

    public static void executeCommand(String cmd, OnlinePlayer sender) {

        // check if they are in the OP list
        boolean canRun = false;

        for (int i = 0; i < MultiplayerGame.opList.length; i++){
            String username = MultiplayerGame.opList[i];
            if (Objects.equals(username, sender.username)){
               canRun = true;
               break;
            }
        }

        if (!canRun){
            MultiplayerGame.sendSystemMessage("You dont have permission to do that!", Color.RED, sender);
            return;
        }

        String[] parts = cmd.split(" ");
        String command = parts[0].replace("/", "");

        Logger.info("Running command " + cmd);

        switch (command) {
            case "setweather":
                new SetWeatherCommand(cmd).execute(sender);
                break;
            case "kickplayer":
                new KickPlayerCommand(cmd).execute(sender);
                break;
            case "giveitem":
                new GiveItemCommand(cmd).execute(sender);
                break;
            case "settime":
                new SetTimeCommand(cmd).execute(sender);
                break;
            case "stop":
                new StopCommand(cmd).execute(sender);
                break;
            default:
                Logger.error("Invalid command: " + cmd);
                MultiplayerGame.sendSystemMessage("Invalid command: " + cmd, Color.RED, sender);
                break;
        }
    }
}
