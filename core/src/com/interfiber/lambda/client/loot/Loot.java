package com.interfiber.lambda.client.loot;

import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.player.PlayerInventory;

import java.util.List;

public abstract class Loot {
    public abstract List<Item> getResultItem();

    public void giveToPlayer(PlayerInventory inventory) {
        this.giveToPlayer(inventory, Player.currentInventory);
    }

    public void giveToPlayer(PlayerInventory inventory, int inventoryId) {
        for (Item item : getResultItem()) {
            inventory.pickupItem(item, inventoryId);
        }
    }
}
