package com.interfiber.lambda.client.loot.tiles;

import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.WoodItem;
import com.interfiber.lambda.client.loot.TileLoot;

import java.util.ArrayList;
import java.util.List;

public class TreeTileLoot extends TileLoot {
    @Override
    public List<Item> getResultItem() {
        List<Item> result = new ArrayList<>();
        WoodItem item = new WoodItem();
        item.amount = 3;
        result.add(item);

        return result;
    }

    @Override
    public int getPickaxeLevel() {
        return 0;
    }
}
