package com.interfiber.lambda.client.loot.tiles;

import com.interfiber.lambda.client.items.IronOreItem;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.loot.TileLoot;

import java.util.ArrayList;
import java.util.List;

public class IronOreTileLoot extends TileLoot {
    @Override
    public List<Item> getResultItem() {
        List<Item> items = new ArrayList<>();
        items.add(new IronOreItem());

        return items;
    }

    @Override
    public int getPickaxeLevel() {
        return 2;
    }
}
