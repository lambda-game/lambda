package com.interfiber.lambda.client.loot.tiles;

import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.RockItem;
import com.interfiber.lambda.client.loot.TileLoot;

import java.util.ArrayList;
import java.util.List;

public class StoneTileLoot extends TileLoot {
    @Override
    public List<Item> getResultItem() {
        List<Item> items = new ArrayList<>();
        items.add(new RockItem());
        return items;
    }

    @Override
    public int getPickaxeLevel() {
        return 1;
    }
}
