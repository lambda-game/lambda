package com.interfiber.lambda.client.loot.tiles;

import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.WheatSeedsItem;
import com.interfiber.lambda.client.loot.TileLoot;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class LeafyGrassTileLoot extends TileLoot {
    @Override
    public List<Item> getResultItem() {
        Random random = new Random();
        List<Item> items = new ArrayList<>();

        int max = 10;
        if (random.nextInt(max) == 3) {
            items.add(new WheatSeedsItem());
        }

        return items;
    }

    @Override
    public int getPickaxeLevel() {
        return 0;
    }
}
