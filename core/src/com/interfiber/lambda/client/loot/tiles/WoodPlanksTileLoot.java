package com.interfiber.lambda.client.loot.tiles;

import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.WoodItem;
import com.interfiber.lambda.client.loot.TileLoot;

import java.util.ArrayList;
import java.util.List;

public class WoodPlanksTileLoot extends TileLoot {
    @Override
    public List<Item> getResultItem() {
        List<Item> items = new ArrayList<>();
        items.add(new WoodItem());

        return items;
    }

    @Override
    public int getPickaxeLevel() {
        return 0;
    }
}
