package com.interfiber.lambda.client.loot;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.items.Pickaxe;
import com.interfiber.lambda.client.player.PlayerInventory;
import org.tinylog.Logger;

public abstract class TileLoot extends Loot {
    public abstract int getPickaxeLevel();

    public void giveToPlayer(PlayerInventory inventory) {

        if (Game.multiplayerEnabled) {
            return; // the server handles loot, not us
        }

        // check pickaxe level
        boolean hasPickaxe = Pickaxe.requirePickaxeLevel(this.getPickaxeLevel());

        if (hasPickaxe) {
            super.giveToPlayer(inventory);
        } else {
            Logger.error("Loot requires higher pickaxe level!");
            if (Game.alertSystem != null) {
                Game.alertSystem.createToast("Better pickaxe required to obtain items!");
            }
        }
    }
}
