package com.interfiber.lambda.client.weather;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Game;
import org.tinylog.Logger;

public class WeatherUtils {
    public static void renderWeather(SpriteBatch batch) {
        Game.weather.renderWeather(batch);
    }

    public static void disposeWeather() {
        Game.weather.dispose();
    }

    public static void setWeather(Weather weather) {
        Logger.info("Updating current weather");
        Game.weather = weather;
        if (Game.weather == null) {
            Logger.info("Set weather to null, no weather event is occuring now");
            return;
        } else {
            Game.weather.create();
            Logger.info("Updated current weather");
        }
    }

    public static Weather weatherTypeToWeather(WeatherType t) {
        switch (t) {
            case CLEAR:
                return null;
            case THUNDERSTORM:
                return new ThunderstormWeather();
            default:
                return null;
        }
    }
}
