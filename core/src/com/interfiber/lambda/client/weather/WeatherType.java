package com.interfiber.lambda.client.weather;

public enum WeatherType {
    CLEAR,
    THUNDERSTORM
}
