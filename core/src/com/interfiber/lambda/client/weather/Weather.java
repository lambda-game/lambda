package com.interfiber.lambda.client.weather;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Weather {
    public abstract void create();

    public abstract void renderWeather(SpriteBatch batch);

    public abstract void dispose();

    public abstract WeatherType getWeatherType();
}
