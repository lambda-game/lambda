package com.interfiber.lambda.client.weather;

import com.badlogic.gdx.utils.TimeUtils;
import com.interfiber.lambda.client.Game;
import org.tinylog.Logger;

import java.util.Random;

public class WeatherCycle {
    private static long startTime = TimeUtils.millis();
    private static Random random = new Random();

    public static void updateWeatherCycle() {
        if (Game.multiplayerEnabled) {
            return;
        }
        boolean updatedWeather = false;
        if (TimeUtils.timeSinceMillis(startTime) > 30000 && Game.weather == null) {
            startTime = TimeUtils.millis();
            Logger.info("Checking for weather updates");
            // thunderstorm?
            if (random.nextInt(40) == 20) {
                updatedWeather = true;
                WeatherUtils.setWeather(new ThunderstormWeather());
            }
            Logger.info("Weather update status: " + updatedWeather);
        } else if (TimeUtils.timeSinceMillis(startTime) > 180000) {
            Logger.info("Clearing weather");
            // every 3 mins clear the weather
            startTime = TimeUtils.millis();
            Game.weather.dispose();
            Game.weather = null;
        }
    }
}
