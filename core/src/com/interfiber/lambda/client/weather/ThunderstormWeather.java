package com.interfiber.lambda.client.weather;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.audio.AudioManager;
import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.client.world.WorldType;
import org.tinylog.Logger;

import java.util.Random;

public class ThunderstormWeather extends Weather {

    private Music music;
    private String[] thunderstormDirections = {"north", "south", "east", "west"};

    @Override
    public void create() {
        music = AudioManager.getMusic("thunderstorm_ambient");
        music.setLooping(true);
        music.play();

        Game.time = Time.NIGHT;
        String direction =
                thunderstormDirections[new Random().nextInt(thunderstormDirections.length)];
        Game.alertSystem.createToast("A thunderstorm approaches from the " + direction);

        // water all tiles
        for (WorldTile tile : Game.getCurrentWorld().getWorldData()) {
            if (tile.type == TileType.FARMLAND) {
                tile.type = TileType.WATERED_FARMLAND;
            }
        }
    }

    @Override
    public void renderWeather(SpriteBatch batch) {
        if (Game.currentWorld != WorldType.OVERWORLD || Game.time != Time.NIGHT) {
            Logger.info("Stopping weather, incorrect Game.currentWorld");
            // thunderstorms dont happen underground, duh
            WeatherUtils.setWeather(null);
            this.dispose();
            return;
        }
    }

    @Override
    public void dispose() {
        if (music != null) {
            music.dispose();
        }
    }

    @Override
    public WeatherType getWeatherType() {
        return WeatherType.THUNDERSTORM;
    }
}
