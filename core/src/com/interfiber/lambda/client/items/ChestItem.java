package com.interfiber.lambda.client.items;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;

public class ChestItem extends Item {

    @Override
    public Texture getTextureName() {
        return Assets.manager.get("chest_item.png");
    }

    @Override
    public String getItemDisplayName() {
        return "Chest";
    }

    @Override
    public ItemType getType() {
        return ItemType.CHEST;
    }

    @Override
    public void interact(int slot) {
        Player.inventoryTile = TileType.CHEST;
    }

    @Override
    public void removed(boolean finalRemove) {
        if (finalRemove) {
            Player.inventoryTile = TileType.AIR;
        }
    }
}
