package com.interfiber.lambda.client.items;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;

public class WoodPickaxeItem extends PickaxeItem {
    @Override
    public Texture getTextureName() {
        return Assets.manager.get("wooden_pickaxe_item.png", Texture.class);
    }

    @Override
    public String getItemDisplayName() {
        return "Wood Pick";
    }

    @Override
    public ItemType getType() {
        return ItemType.WOODEN_PICKAXE;
    }

    @Override
    public void interact(int slot) {
        Player.inventoryTile = TileType.ITEM;
    }

    @Override
    public void removed(boolean finalRemove) {
        if (finalRemove) {
            Player.inventoryTile = TileType.AIR;
        }
    }

    @Override
    public int getLevel() {
        return 1;
    }
}
