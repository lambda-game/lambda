package com.interfiber.lambda.client.items;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.farming.HoeUtils;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.WorldTile;

public class WoodHoeItem extends Item {

    @Override
    public Texture getTextureName() {
        return Assets.manager.get("wood_hoe_item.png");
    }

    @Override
    public String getItemDisplayName() {
        return "Wood Hoe";
    }

    @Override
    public ItemType getType() {
        return ItemType.WOOD_HOE;
    }

    @Override
    public void interact(int slot) {
        Player.inventoryTile = TileType.ITEM;
    }

    @Override
    public void removed(boolean finalRemove) {
        if (finalRemove) {
            Player.inventoryTile = TileType.AIR;
        }
    }

    @Override
    public void tileClicked(WorldTile worldTile) {
        if (HoeUtils.harvestIfPossible(worldTile)) {
            return;
        } else {
            HoeUtils.hoeSoil(worldTile);
        }
    }
}
