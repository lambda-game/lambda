package com.interfiber.lambda.client.items;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;

public class IronBarItem extends Item {
    @Override
    public Texture getTextureName() {
        return Assets.manager.get("iron_bar.png");
    }

    @Override
    public String getItemDisplayName() {
        return "Iron Bar";
    }

    @Override
    public ItemType getType() {
        return ItemType.IRON_BAR;
    }

    @Override
    public void interact(int slot) {
        Player.inventoryTile = TileType.ITEM;
    }

    @Override
    public void removed(boolean finalRemove) {
        if (finalRemove) {
            Player.inventoryTile = TileType.AIR;
        }
    }
}
