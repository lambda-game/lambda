package com.interfiber.lambda.client.items;

public abstract class PickaxeItem extends Item {
    public abstract int getLevel();
}
