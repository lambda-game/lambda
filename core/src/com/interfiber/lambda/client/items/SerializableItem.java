package com.interfiber.lambda.client.items;

import java.io.Serializable;

public class SerializableItem implements Serializable {
    public int amount;
    public ItemType type;
}