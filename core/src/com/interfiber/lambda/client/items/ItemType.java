package com.interfiber.lambda.client.items;

public enum ItemType {
    WOOD,
    EXAMPLE_ITEM,
    WORKBENCH,
    STICK,
    STONE_PICKAXE,
    WOODEN_PICKAXE,
    TORCH,
    FURNACE,
    COAL,
    ROCK,
    IRON_BAR,
    IRON_ORE,
    IRON_PICKAXE,
    CHEST,
    WOOD_HOE,
    WHEAT,
    WHEAT_SEEDS,
    WATERING_CAN,
    BREAD,
    CARROT_SEEDS,
    CARROT,
    CARROT_BREAD
}
