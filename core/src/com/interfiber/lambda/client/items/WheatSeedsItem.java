package com.interfiber.lambda.client.items;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.farming.CropType;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.WorldTile;

public class WheatSeedsItem extends Item {

    @Override
    public Texture getTextureName() {
        return Assets.manager.get("wheat_seeds_item.png");
    }

    @Override
    public String getItemDisplayName() {
        return "Wheat Seeds";
    }

    @Override
    public ItemType getType() {
        return ItemType.WHEAT_SEEDS;
    }

    @Override
    public void interact(int slot) {
        Player.inventoryTile = TileType.ITEM;
    }

    @Override
    public void removed(boolean finalRemove) {
        if (finalRemove) {
            Player.inventoryTile = TileType.AIR;
        }
    }

    @Override
    public void tileClicked(WorldTile tile) {
        CropManager.plantCrop(CropType.WHEAT, tile.x, tile.y, true);
    }
}
