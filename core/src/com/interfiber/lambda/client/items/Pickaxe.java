package com.interfiber.lambda.client.items;

import com.interfiber.lambda.client.player.Player;

public class Pickaxe {

    public static boolean requirePickaxeLevel(int level) {
        if (level == 0) {
            return true;
        }

        if (Player.inventory.getCurrentlySelectedItem() instanceof PickaxeItem) {
            PickaxeItem pickaxeItem = (PickaxeItem) Player.inventory.getCurrentlySelectedItem();
            int itemLevel = pickaxeItem.getLevel();
            return level <= itemLevel;
        } else {
            return false;
        }
    }
}
