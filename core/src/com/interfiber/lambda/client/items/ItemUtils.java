package com.interfiber.lambda.client.items;

public class ItemUtils {
    public static Item getItemFromId(ItemType type) {
        switch (type) {
            case WOOD:
                return new WoodItem();
            case STICK:
                return new StickItem();
            case WORKBENCH:
                return new WorkbenchItem();
            case EXAMPLE_ITEM:
                return new ExampleItem();
            case STONE_PICKAXE:
                return new StonePickaxeItem();
            case ROCK:
                return new RockItem();
            case WOODEN_PICKAXE:
                return new WoodPickaxeItem();
            case TORCH:
                return new TorchItem();
            case FURNACE:
                return new FurnaceItem();
            case COAL:
                return new CoalItem();
            case IRON_ORE:
                return new IronOreItem();
            case IRON_BAR:
                return new IronBarItem();
            case IRON_PICKAXE:
                return new IronPickaxeItem();
            case CHEST:
                return new ChestItem();
            case WOOD_HOE:
                return new WoodHoeItem();
            case WHEAT:
                return new WheatItem();
            case WHEAT_SEEDS:
                return new WheatSeedsItem();
            case WATERING_CAN:
                return new WateringCanItem();
            case BREAD:
                return new BreadItem();
            case CARROT_SEEDS:
                return new CarrotSeedsItem();
            case CARROT:
                return new CarrotItem();
            case CARROT_BREAD:
                return new CarrotBreadItem();
        }
        return null;
    }
}
