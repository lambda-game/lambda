package com.interfiber.lambda.client.items;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;

public class ExampleItem extends Item {

    @Override
    public Texture getTextureName() {
        return Assets.manager.get("example_item.png", Texture.class);
    }

    @Override
    public String getItemDisplayName() {
        return "Example Item";
    }

    @Override
    public ItemType getType() {
        return ItemType.EXAMPLE_ITEM;
    }

    @Override
    public void interact(int slot) {
        Player.inventoryTile = TileType.ITEM;
    }

    @Override
    public void removed(boolean finalRemove) {
        if (finalRemove) {
            Player.inventoryTile = TileType.AIR;
        }
    }
}
