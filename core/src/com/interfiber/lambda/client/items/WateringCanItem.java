package com.interfiber.lambda.client.items;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.WaterSoilPacket;

public class WateringCanItem extends Item {

    @Override
    public Texture getTextureName() {
        return Assets.manager.get("watering_can.png");
    }

    @Override
    public String getItemDisplayName() {
        return "Watering Can";
    }

    @Override
    public ItemType getType() {
        return ItemType.WATERING_CAN;
    }

    @Override
    public void interact(int slot) {
        Player.inventoryTile = TileType.ITEM;
    }

    @Override
    public void removed(boolean finalRemove) {
        if (finalRemove) {
            Player.inventoryTile = TileType.AIR;
        }
    }

    @Override
    public void tileClicked(WorldTile tile) {
        if (tile.type == TileType.FARMLAND) {
            if (CropManager.getCropAtPosition(tile.x, tile.y) != null) {
                Game.getCurrentWorld().getWorldData().remove(tile);
                tile.type = TileType.WATERED_FARMLAND;
                Game.getCurrentWorld().getWorldData().add(tile);
                CropManager.setCropAtPositionToBeWatered(tile.x, tile.y);

                if (Game.multiplayerEnabled) {
                    ParsedPacket packet = new ParsedPacket();
                    packet.initGuard(MultiplayerGameState.player.authUUID);
                    packet.content.put("tileX", tile.x);
                    packet.content.put("tileY", tile.y);

                    new WaterSoilPacket(packet).writeData(MultiplayerGameState.serverConnection);
                }

            } else {
                Game.alertSystem.createToast("You can only water crops!");
            }
        }
    }
}
