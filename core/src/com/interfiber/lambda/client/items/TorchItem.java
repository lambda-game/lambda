package com.interfiber.lambda.client.items;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;

public class TorchItem extends Item {
    @Override
    public Texture getTextureName() {
        return Assets.manager.get("torch.png", Texture.class);
    }

    @Override
    public String getItemDisplayName() {
        return "Torch";
    }

    @Override
    public ItemType getType() {
        return ItemType.TORCH;
    }

    @Override
    public void interact(int slot) {
        Player.inventoryTile = TileType.TORCH;
    }

    @Override
    public void removed(boolean finalRemove) {
        if (finalRemove) {
            Player.inventoryTile = TileType.AIR;
        }
    }
}
