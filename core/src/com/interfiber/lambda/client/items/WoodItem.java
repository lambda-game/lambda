package com.interfiber.lambda.client.items;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;

public class WoodItem extends Item {
    @Override
    public Texture getTextureName() {
        return Assets.manager.get("wood_item.png", Texture.class);
    }

    @Override
    public String getItemDisplayName() {
        return "Wood";
    }

    @Override
    public void interact(int slot) {
        Player.inventoryTile = TileType.WOOD_PLANKS;
    }

    @Override
    public void removed(boolean finalRemove) {
        if (finalRemove) {
            Player.inventoryTile = TileType.AIR;
        }
    }

    @Override
    public ItemType getType() {
        return ItemType.WOOD;
    }
}
