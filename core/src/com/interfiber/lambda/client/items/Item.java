package com.interfiber.lambda.client.items;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.world.WorldTile;

import java.io.Serializable;

public abstract class Item implements Serializable {
    public int amount = 0;

    public abstract Texture getTextureName();

    public abstract String getItemDisplayName();

    public abstract ItemType getType();

    public abstract void interact(int slot); // called when the item is selected in the inventory

    public abstract void removed(boolean finalRemove);

    public SerializableItem getSerializableItem() {
        SerializableItem si = new SerializableItem();
        si.amount = this.amount;
        si.type = this.getType();
        return si;
    }

    public void tileClicked(WorldTile clickedTile) {
    }
}
