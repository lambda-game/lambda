package com.interfiber.lambda.client;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGeneratorLoader;
import com.badlogic.gdx.graphics.g2d.freetype.FreetypeFontLoader;
import org.tinylog.Logger;

public class Assets {
    public static final AssetManager manager = new AssetManager();

    public static final String STONE_TEXTURE = "stone.png";
    public static final String GRASS_TEXTURE = "grass.png";
    public static final String CAVE_ENTRANCE_TEXTURE = "cave_entrance.png";
    public static final String COAL_TEXTURE = "coal.png";
    public static final String FURNACE_TEXTURE = "furnace.png";
    public static final String FOG_TEXTURE = "fog.png";
    public static final String INVENTORY_UNSELECTED_TEXTURE = "inventory_unselected.png";
    public static final String INVENTORY_SELECTED_TEXTURE = "inventory_selected.png";
    public static final String CAVE_WALL_STONE_TEXTURE = "cave_wall_stone.png";
    public static final String WOOD_PLANKS_TEXTURE = "wood_planks.png";
    public static final String WATER_TEXTURE = "water.png";
    public static final String ROCK_TEXTURE = "rock.png";
    public static final String TREE_TEXTURE = "tree.png";
    public static final String TORCH_TEXTURE = "torch.png";
    public static final String WOOD_ITEM_TEXTURE = "wood_item.png";
    public static final String WOOD_PICKAXE_ITEM_TEXTURE = "wooden_pickaxe_item.png";
    public static final String WORKBENCH_ITEM_TEXTURE = "workbench_item.png";
    public static final String WORKBENCH_TEXTURE = "workbench.png";
    public static final String FURNACE_ITEM_TEXTURE = "furnace_item.png";
    public static final String ROCK_ITEM_TEXTURE = "rock_item.png";
    public static final String EXAMPLE_ITEM_TEXTURE = "example_item.png";
    public static final String STICK_ITEM_TEXTURE = "stick_item.png";
    public static final String STONE_PICKAXE_ITEM_TEXTURE = "stone_pickaxe_item.png";
    public static final String CLICK_3_AUDIO = "audio/sfx/click3.ogg";
    public static final String CAVES_THEME_AMBIENT_MUSIC = "audio/music/cave_theme_ambient.ogg";
    public static final String IRON_ORE_WALL_TEXTURE = "iron_ore_wall.png";
    public static final String IRON_ORE_TEXTURE = "iron_ore.png";
    public static final String IRON_BAR_TEXTURE = "iron_bar.png";
    public static final String LANAPIXEL_FONT = "LanaPixel.ttf";
    public static final String IRON_PICKAXE_ITEM_TEXTURE = "iron_pickaxe_item.png";
    public static final String CHEST_TEXTURE = "chest.png";
    public static final String CHEST_ITEM_TEXTURE = "chest_item.png";
    public static final String THUNDERSTORM_AMBIENT_MUSIC = "audio/music/thunderstorm_ambient.ogg";
    public static final String RAINDROP_TEXTURE = "raindrop.png";
    public static final String HEART_TEXTURE = "heart.png";
    public static final String FARMLAND_TEXTURE = "farmland.png";
    public static final String WOOD_HOE_ITEM_TEXTURE = "wood_hoe_item.png";
    public static final String WHEAT_SEEDS_TEXTURE = "wheat_seeds.png";
    public static final String WHEAT_TEXTURE = "wheat.png";
    public static final String WHEAT_SEEDS_ITEM_TEXTURE = "wheat_seeds_item.png";
    public static final String SAND_TEXTURE = "sand.png";
    public static final String LEAFY_GRASS_TEXTURE = "leafy_grass.png";
    public static final String ALERT_TEXTURE = "alert.png";
    public static final String WATERING_CAN_TEXTURE = "watering_can.png";
    public static final String FARMLAND_WATER = "farmland_water.png";
    public static final String BREAD_ITEM_TEXTURE = "bread.png";
    public static final String BEACH_THUNDER_BG_TEXTURE = "beach_thunder_bg.png";
    public static final String RIVER_THUNDER_BG_TEXTURE = "river_thunder_bg.png";
    public static final String CARROT_ITEM_TEXTURE = "carrot.png";
    public static final String CARROT_SEEDS_TEXTURE = "carrot_seeds.png";
    public static final String CARROT_BREAD_TEXTURE = "carrot_bread.png";
    public static final String PLAYER_WALK_LEFT_WATER_TEXTURE = "player_walk_left_water.png";
    public static final String PLAYER_WALK_RIGHT_WATER_TEXTURE = "player_walk_right_water.png";
    public static final String PLAYER_WALK_UP_WATER_TEXTURE = "player_walk_up_water.png";
    public static final String PLAYER_WALK_DOWN_WATER_TEXTURE = "player_walk_down_water.png";
    public static final String PLAYER_WALK_DOWN_TEXTURE = "player_walk_down.png";
    public static final String PLAYER_WALK_UP_TEXTURE = "player_walk_up.png";
    public static final String PLAYER_WALK_LEFT_TEXTURE = "player_walk_left.png";
    public static final String PLAYER_WALK_RIGHT_TEXTURE = "player_walk_right.png";

    public static void loadAssets() {
        Logger.info("Loading game assets");
        manager.load(STONE_TEXTURE, Texture.class);
        manager.load(GRASS_TEXTURE, Texture.class);
        manager.load(CAVE_ENTRANCE_TEXTURE, Texture.class);
        manager.load(COAL_TEXTURE, Texture.class);
        manager.load(FURNACE_TEXTURE, Texture.class);
        manager.load(FOG_TEXTURE, Texture.class);
        manager.load(INVENTORY_SELECTED_TEXTURE, Texture.class);
        manager.load(INVENTORY_UNSELECTED_TEXTURE, Texture.class);
        manager.load(CAVE_WALL_STONE_TEXTURE, Texture.class);
        manager.load(WOOD_PLANKS_TEXTURE, Texture.class);
        manager.load(WATER_TEXTURE, Texture.class);
        manager.load(ROCK_TEXTURE, Texture.class);
        manager.load(TREE_TEXTURE, Texture.class);
        manager.load(TORCH_TEXTURE, Texture.class);
        manager.load(WOOD_ITEM_TEXTURE, Texture.class);
        manager.load(WOOD_PICKAXE_ITEM_TEXTURE, Texture.class);
        manager.load(WORKBENCH_ITEM_TEXTURE, Texture.class);
        manager.load(WORKBENCH_TEXTURE, Texture.class);
        manager.load(FURNACE_ITEM_TEXTURE, Texture.class);
        manager.load(ROCK_ITEM_TEXTURE, Texture.class);
        manager.load(EXAMPLE_ITEM_TEXTURE, Texture.class);
        manager.load(STICK_ITEM_TEXTURE, Texture.class);
        manager.load(STONE_PICKAXE_ITEM_TEXTURE, Texture.class);
        manager.load(IRON_ORE_WALL_TEXTURE, Texture.class);
        manager.load(IRON_ORE_TEXTURE, Texture.class);
        manager.load(IRON_BAR_TEXTURE, Texture.class);
        manager.load(IRON_PICKAXE_ITEM_TEXTURE, Texture.class);
        manager.load(CHEST_TEXTURE, Texture.class);
        manager.load(CHEST_ITEM_TEXTURE, Texture.class);
        manager.load(RAINDROP_TEXTURE, Texture.class);
        manager.load(HEART_TEXTURE, Texture.class);
        manager.load(FARMLAND_TEXTURE, Texture.class);
        manager.load(WOOD_HOE_ITEM_TEXTURE, Texture.class);
        manager.load(WHEAT_SEEDS_TEXTURE, Texture.class);
        manager.load(WHEAT_TEXTURE, Texture.class);
        manager.load(WHEAT_SEEDS_ITEM_TEXTURE, Texture.class);
        manager.load(SAND_TEXTURE, Texture.class);
        manager.load(LEAFY_GRASS_TEXTURE, Texture.class);
        manager.load(ALERT_TEXTURE, Texture.class);
        manager.load(WATERING_CAN_TEXTURE, Texture.class);
        manager.load(FARMLAND_WATER, Texture.class);
        manager.load(BREAD_ITEM_TEXTURE, Texture.class);
        manager.load(BEACH_THUNDER_BG_TEXTURE, Texture.class);
        manager.load(RIVER_THUNDER_BG_TEXTURE, Texture.class);
        manager.load(CARROT_ITEM_TEXTURE, Texture.class);
        manager.load(CARROT_SEEDS_TEXTURE, Texture.class);
        manager.load(CARROT_BREAD_TEXTURE, Texture.class);
        manager.load(PLAYER_WALK_DOWN_TEXTURE, Texture.class);
        manager.load(PLAYER_WALK_DOWN_WATER_TEXTURE, Texture.class);
        manager.load(PLAYER_WALK_UP_TEXTURE, Texture.class);
        manager.load(PLAYER_WALK_UP_WATER_TEXTURE, Texture.class);
        manager.load(PLAYER_WALK_RIGHT_TEXTURE, Texture.class);
        manager.load(PLAYER_WALK_RIGHT_WATER_TEXTURE, Texture.class);
        manager.load(PLAYER_WALK_LEFT_TEXTURE, Texture.class);
        manager.load(PLAYER_WALK_LEFT_WATER_TEXTURE, Texture.class);

        // audio or music
        manager.load(CLICK_3_AUDIO, Sound.class);
        manager.load(CAVES_THEME_AMBIENT_MUSIC, Music.class);
        manager.load(THUNDERSTORM_AMBIENT_MUSIC, Music.class);

        // fonts
        FileHandleResolver resolver = new InternalFileHandleResolver();
        manager.setLoader(FreeTypeFontGenerator.class, new FreeTypeFontGeneratorLoader(resolver));
        manager.setLoader(BitmapFont.class, ".ttf", new FreetypeFontLoader(resolver));

        FreetypeFontLoader.FreeTypeFontLoaderParameter size1Params =
                new FreetypeFontLoader.FreeTypeFontLoaderParameter();
        size1Params.fontFileName = LANAPIXEL_FONT; // name of file on disk
        size1Params.fontParameters.size = 30;

        manager.load(LANAPIXEL_FONT, BitmapFont.class, size1Params);

        // load the assets
        while (!manager.update()) {
            Logger.info("Asset load progress: " + manager.getProgress() * 100 + "%");
        }
        Logger.info("Asset loading completed");
    }

    public static void dispose() {
        manager.dispose();
    }
}
