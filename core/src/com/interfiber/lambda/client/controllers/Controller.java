package com.interfiber.lambda.client.controllers;

public abstract class Controller {
    public abstract void sendEvent(ControllerEventType event, Object payload);
}
