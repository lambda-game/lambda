package com.interfiber.lambda.client.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.info.TileHardness;
import com.interfiber.lambda.client.world.World;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.RemoveTilePacket;
import org.tinylog.Logger;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

public class PlayerRemoveTileController extends Controller {

    public static Instant startTime;
    public static int oldTileX = -1;
    public static int oldTileY = -1;

    @Override
    public void sendEvent(ControllerEventType event, Object payload) {
        Vector3 newLocation = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
        Player.camera.unproject(newLocation);
        int tileNumX = (int) Math.floor(newLocation.x / Game.tileSize);
        int tileNumY = (int) Math.floor(newLocation.y / Game.tileSize);

        World currentWorld = Game.getCurrentWorld();

        if (Vector2.dst(Player.x, Player.y, tileNumX * Game.tileSize,
                tileNumY * Game.tileSize) > 241) {
            return;
        }


        // make sure we remove tiles that do not exist
        WorldTile tile = currentWorld.getUserTileAtPosition(tileNumX * Game.tileSize,
                tileNumY * Game.tileSize);
        if (tile != null) {
            if (startTime == null) {
                // set start time
                startTime = Instant.now();
                Game.tileBreakProgress = 0;
                Logger.debug("Tile hardness: " + TileHardness.getHardnessForTile(tile.type));
            } else {

                if (Objects.equals(oldTileX, -1)) {
                    oldTileX = tileNumX * Game.tileSize;
                }
                if (Objects.equals(oldTileY, -1)) {
                    oldTileY = tileNumY * Game.tileSize;
                }
                if (oldTileX != tileNumX * Game.tileSize || oldTileY != tileNumY * Game.tileSize) {
                    startTime = null;
                    Game.tileBreakProgress = 0;
                    Player.isBreakingTile = false;
                    oldTileX = 0;
                    oldTileY = 0;
                    return;
                }
                // get break time of tile
                int breakTimeSeconds = TileHardness.getHardnessForTile(tile.type) * 1000;
                long timeUntilBreak = Duration.between(startTime, Instant.now()).toMillis();
                if (!Player.isBreakingTile) {
                    Logger.info("Not breaking tile, resetting");
                    startTime = null;
                    Game.tileBreakProgress = 0;
                    oldTileX = -1;
                    oldTileY = -1;
                    return;
                }
                Game.tileBreakProgress = ((float) timeUntilBreak / (float) breakTimeSeconds) * 100;

                if (Duration.between(startTime, Instant.now()).toMillis() >= breakTimeSeconds
                        && Player.isBreakingTile) {
                    Logger.info("Breaking tile");
                    // break the tile
                    currentWorld.removeUserPlacedTile(tileNumX * Game.tileSize,
                            tileNumY * Game.tileSize);
                    startTime = null;
                    Game.tileBreakProgress = 0;
                    Player.isBreakingTile = false;
                    oldTileX = -1;
                    oldTileY = -1;

                    if (Game.multiplayerEnabled) {
                        ParsedPacket packetPayload = new ParsedPacket();
                        packetPayload.initGuard(MultiplayerGameState.player.authUUID);

                        packetPayload.content.put("xPosition", tileNumX * Game.tileSize);
                        packetPayload.content.put("yPosition", tileNumY * Game.tileSize);

                        new RemoveTilePacket(packetPayload).writeData(MultiplayerGameState.serverConnection);
                    }

                }
            }
        } else {
            Player.isBreakingTile = false;
            startTime = null;
            oldTileX = -1;
            oldTileY = -1;
            Game.tileBreakProgress = 0;
        }
    }

}
