package com.interfiber.lambda.client.controllers;

import java.util.HashMap;

public class ControllerManager {

    private static HashMap<String, Controller> controllerList = new HashMap<>();

    public static void addController(String controllerID, Controller controller) {
        controllerList.put(controllerID, controller);
    }

    public static Controller getController(String controllerID) {
        return controllerList.get(controllerID);
    }

}
