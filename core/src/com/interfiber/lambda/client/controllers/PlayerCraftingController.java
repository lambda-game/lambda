package com.interfiber.lambda.client.controllers;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.crafting.CraftingUtils;
import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeManager;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.CraftRecipePacket;
import org.tinylog.Logger;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerCraftingController extends Controller {

    @Override
    public void sendEvent(ControllerEventType event, Object payload) {
        if (event == ControllerEventType.PLAYER_CRAFT) {

            if (Game.multiplayerEnabled) {
                // send craft recipe packet, the server will handle it instead
                ParsedPacket packet = new ParsedPacket();
                packet.initGuard(MultiplayerGameState.player.authUUID);

                packet.content.put("recipeName", payload.toString());

                new CraftRecipePacket(packet).writeData(MultiplayerGameState.serverConnection);
                return;
            }

            // check if user can craft item
            Recipe craftRecipe = RecipeManager.getRecipe((String) payload);
            if (!CraftingUtils.canCraftItem(craftRecipe)) {
                Logger.error("Cannot craft item!");
                Game.alertSystem.createToast("Cannot craft " + craftRecipe.getRecipeName());
                return;
            }
            ConcurrentHashMap<Integer, Item> playerItems = Player.inventory.getItems();

            // remove items
            for (Map.Entry<Integer, Item> set : playerItems.entrySet()) {
                Item playerItem = set.getValue();
                Ingredient ingredient = new Ingredient();
                ingredient.itemType = playerItem.getType();
                ingredient.amount = playerItem.amount;
                int slot = 0;
                int removeAmount = 0;
                for (Ingredient ingredient1 : craftRecipe.getItems()) {
                    if (ingredient.amount >= ingredient1.amount && ingredient.itemType == ingredient1.itemType) {
                        slot = set.getKey();
                        removeAmount = ingredient1.amount;
                        break;
                    }
                }
                Logger.info("Removing item at slot: " + slot);
                Player.inventory.removeItem(slot, removeAmount);
            }

            Item outputItem = craftRecipe.getOutputItem();
            Player.inventory.pickupItem(outputItem);
        }
    }

}
