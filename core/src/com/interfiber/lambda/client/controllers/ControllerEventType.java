package com.interfiber.lambda.client.controllers;

public enum ControllerEventType {
    PLAYER_DESTROY_TILE,
    PLAYER_PLACE_TILE,
    PLAYER_MOVE,
    PLAYER_KEYBINDINGS,
    PLAYER_CRAFT,
    PLAYER_SEND_NOTIFICATION
}
