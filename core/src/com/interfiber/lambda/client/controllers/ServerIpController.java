package com.interfiber.lambda.client.controllers;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.interfiber.lambda.client.Game;
import org.tinylog.Logger;

public class ServerIpController implements InputProcessor {

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.DEL) {
            if (Game.serverIp.length() - 1 < 0) {
                return true;
            }
            Game.serverIp = Game.serverIp.substring(0, Game.serverIp.length() - 1);
            Logger.debug("New server ip is : " + Game.serverIp);
        } else if (keycode == Input.Keys.SPACE) {
            Game.serverIp += " ";
        } else if (keycode == Input.Keys.SHIFT_LEFT) {
            return true;
        } else if (keycode == Input.Keys.SHIFT_RIGHT) {
            return true;
        } else if (keycode == Input.Keys.ENTER) {
            return true;
        } else {
            String stringChar = Input.Keys.toString(keycode);
            if (stringChar.length() > 1) {
                return true;
            }
            Game.serverIp += stringChar;
            Logger.debug("New server ip is: " + Game.serverIp);
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }
}
