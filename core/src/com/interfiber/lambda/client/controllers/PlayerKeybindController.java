package com.interfiber.lambda.client.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.crafting.CraftingMenuMode;
import com.interfiber.lambda.client.crafting.CraftingUtils;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.SwitchSelectedInventorSlotPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.SwitchSelectedInventoryPacket;
import org.tinylog.Logger;

public class PlayerKeybindController extends Controller {
    @Override
    public void sendEvent(ControllerEventType event, Object payload) {
        if (event == ControllerEventType.PLAYER_KEYBINDINGS) {
            // switch inventory hot bar slot

            if (Gdx.input.isKeyJustPressed(Input.Keys.T) && Game.multiplayerEnabled) {
                Game.chatOpen = true;
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {

                if (Game.chatOpen) {
                    Game.chatOpen = false;
                    return;
                }

                Logger.info("Toggling pause menu");
                Game.pauseMenuOpen = !Game.pauseMenuOpen;
                Game.hiddenCursor = !Game.hiddenCursor;
            }

            if (Game.chatOpen) {
                return;
            }

            int startIndex = Player.currentInventory * Game.inventoryBottomBarSlots;
            if (startIndex != 0) {
                startIndex += 1;
            }
            for (int e = 0; e < Game.inventoryBottomBarSlots; e++) {
                if (Gdx.input.isKeyJustPressed(Input.Keys.valueOf(Integer.toString(e + 1))) && !Player.isBreakingTile) {
                    Player.selectedInventorySlot = e + startIndex;

                    if (Game.multiplayerEnabled) {
                        ParsedPacket parsedPacket = new ParsedPacket();
                        parsedPacket.initGuard(MultiplayerGameState.player.authUUID);
                        parsedPacket.content.put("selectedSlot", Player.selectedInventorySlot);

                        new SwitchSelectedInventorSlotPacket(parsedPacket).writeData(MultiplayerGameState.serverConnection);
                    }
                }
            }


            // cycle through inventory's
            if (Gdx.input.isKeyJustPressed(Input.Keys.TAB)) {
                boolean increase = false;
                if (Player.currentInventory + 1 <= Player.playerInventoryCount) {
                    increase = true;
                }
                if (increase) {
                    Player.currentInventory += 1;
                } else {
                    Player.currentInventory = 0;
                }
                Logger.info("Switched to inventory: " + Player.currentInventory);
                if (Game.multiplayerEnabled) {
                    ParsedPacket packet = new ParsedPacket();
                    packet.initGuard(MultiplayerGameState.player.authUUID);
                    packet.content.put("selectedInventory", Player.currentInventory);

                    new SwitchSelectedInventoryPacket(packet).writeData(MultiplayerGameState.serverConnection);
                }
            }

            // open crafting menu
            if (Gdx.input.isKeyJustPressed(Input.Keys.C)) {
                Logger.info("Toggling crafting menu");
                Game.craftingMenuMode = CraftingMenuMode.PORTABLE_CRAFTING;
                CraftingUtils.rebuildOverlayRecipeCache();
                Game.craftingMenuOpen = !Game.craftingMenuOpen;
            }
        }
    }
}
