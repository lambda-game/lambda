package com.interfiber.lambda.client.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.interfiber.lambda.client.Game;

public class ChatMessageController implements InputProcessor {

    @Override
    public boolean keyDown(int keycode) {
        if (keycode == Input.Keys.DEL) {
            if (Game.chatMessage.length() - 1 < 0) {
                return true;
            }

            Game.chatMessage = Game.chatMessage.substring(0, Game.chatMessage.length() - 1);
        } else if (keycode == Input.Keys.SPACE) {
            Game.chatMessage += " ";
        } else if (keycode == Input.Keys.SHIFT_LEFT) {
            return true;
        } else if (keycode == Input.Keys.SHIFT_RIGHT) {
            return true;
        } else if (keycode == Input.Keys.ENTER) {
            return true;
        } else {
            String stringChar = Input.Keys.toString(keycode);
            if (stringChar.length() > 1) {
                return true;
            }


            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT)) {
                stringChar = stringChar.toUpperCase();
                stringChar = stringChar.replace("/", "?").replace("1", "!").replace("-", "_");
            } else {
                stringChar = stringChar.toLowerCase();
            }
            Game.chatMessage += stringChar;
        }
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(float amountX, float amountY) {
        return false;
    }
}