package com.interfiber.lambda.client.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.world.collision.CollisionRectangle;
import com.interfiber.lambda.client.world.collision.WorldMap;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.PlayerPositionUpdatePacket;
import org.tinylog.Logger;

import java.util.List;

public class PlayerMovementController extends Controller {

    private boolean canMoveTo(int x, int y) {
        if (Game.developerModeEnabled && !Game.debugColliderEnabled) {
            return true;
        }
        List<String> renderedColliderIds = Game.getCurrentWorld().getRenderedColliders();
        boolean canMove = true;
        if (y <= 0 || x <= 0) {
            return false;
        }

        // no moving while the pause menu is open, or chat
        if (Game.pauseMenuOpen) {
            return false;
        }

        if (Game.chatOpen) {
            return false;
        }

        // no moving while the crafting menu is open
        if (Game.craftingMenuOpen) {
            return false;
        }
        for (String colliderId : renderedColliderIds) {
            // get the collider
            if (WorldMap.colliderExists(colliderId)) {
                CollisionRectangle info = WorldMap.getColliderInfo(colliderId);
                CollisionRectangle rec = new CollisionRectangle(x, y, Player.width, Player.height);
                if (rec.collidesWithObject(info)) {
                    canMove = false;
                    break;
                }
            } else {
                Logger.warn("Invalid collider ID: " + colliderId);
            }
        }
        return canMove;
    }

    @Override
    public void sendEvent(ControllerEventType event, Object payload) {
        int speed = Math.round(Player.speed * Gdx.graphics.getDeltaTime());
        if (Player.isSwimming) {
            speed = Math.round(Player.swimmingSpeed * Gdx.graphics.getDeltaTime());
        }

        // region player movement
        if (Gdx.input.isKeyPressed(Input.Keys.W) && canMoveTo(Player.x, Player.y + speed)) {
            Player.y += speed;
            Player.facing = "up";
        }
        if (Gdx.input.isKeyPressed(Input.Keys.S) && canMoveTo(Player.x, Player.y - speed)) {
            Player.y -= speed;
            Player.facing = "down";
        }
        if (Gdx.input.isKeyPressed(Input.Keys.D) && canMoveTo(Player.x + speed, Player.y)) {
            Player.x += speed;
            Player.facing = "right";
        }
        if (Gdx.input.isKeyPressed(Input.Keys.A) && canMoveTo(Player.x - speed, Player.y)) {
            Player.x -= speed;
            Player.facing = "left";
        }

        if (Game.multiplayerEnabled) {
            ParsedPacket packet = new ParsedPacket();
            packet.packetId = Packet.PacketType.PLAYER_POSITION_UPDATE.getId();
            packet.initGuard(MultiplayerGameState.player.authUUID);
            packet.content.put("xPosition", Player.x);
            packet.content.put("yPosition", Player.y);
            packet.content.put("facing", Player.facing);
            packet.content.put("isSwimming", Player.isSwimming);

            new PlayerPositionUpdatePacket(packet).writeData(MultiplayerGameState.serverConnection);
        }
    }

}
