package com.interfiber.lambda.client.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.entitys.EntityManager;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.StorageContainer;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.StorageContainerWorldTile;
import com.interfiber.lambda.client.world.World;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.client.world.collision.CollisionRectangle;
import com.interfiber.lambda.client.world.collision.WorldMap;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.PlaceTilePacket;
import org.tinylog.Logger;

import java.util.concurrent.ConcurrentHashMap;

public class PlayerPlaceTileController extends Controller {
    @Override
    public void sendEvent(ControllerEventType event, Object payload) {
        if (event == ControllerEventType.PLAYER_PLACE_TILE) {
            // check if we are placing the block on the player
            Vector3 tmpCoords = new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0);
            Player.camera.unproject(tmpCoords);
            CollisionRectangle playerCollider =
                    WorldMap.getCollider(EntityManager.getEntity("player").getColliderId());
            int tileNumX = (int) Math.floor(tmpCoords.x / Game.tileSize);
            int tileNumY = (int) Math.floor(tmpCoords.y / Game.tileSize);

            World currentWorld = Game.getCurrentWorld();

            // make sure we can't place the tile on the player, or place two blocks at the same
            // position as each other
            boolean hasAlreadyPlaced = false;
            boolean isOnPlayer = false;
            boolean distanceIsTooFar = false;
            if (currentWorld.getUserTileAtPosition(tileNumX * Game.tileSize,
                    tileNumY * Game.tileSize) != null) {
                if (currentWorld.getUserTileAtPosition(tileNumX * Game.tileSize,
                        tileNumY * Game.tileSize).userPlaced) {
                    hasAlreadyPlaced = true;
                }
            }

            if (Vector2.dst(Player.x, Player.y, tileNumX * Game.tileSize,
                    tileNumY * Game.tileSize) > 241) {
                distanceIsTooFar = true;
            }

            if (WorldMap.overlaps(tileNumX * Game.tileSize, tileNumY * Game.tileSize, Game.tileSize,
                    Game.tileSize, playerCollider)) {
                isOnPlayer = true;
            }

            if (Player.inventoryTile == TileType.ITEM && Player.inventory.getCurrentlySelectedItem() != null) {
                WorldTile tile = currentWorld.getTileAtPosition(tileNumX * Game.tileSize,
                        tileNumY * Game.tileSize);
                if (tile != null) {
                    Player.inventory.getCurrentlySelectedItem().tileClicked(tile);
                }
                return;
            }
            if (!hasAlreadyPlaced && !isOnPlayer && !distanceIsTooFar) {
                if (Player.inventoryTile != TileType.AIR
                        && Player.inventory.getCurrentlySelectedItem() != null) {
                    boolean canPlace = TileLoader.getTile(Player.inventoryTile)
                            .onPlace(tileNumX * Game.tileSize, tileNumY * Game.tileSize);

                    if (TileLoader.getTile(Player.inventoryTile) instanceof StorageContainer) {
                        // add the storage container world tile to the world

                        StorageContainerWorldTile containerTile = new StorageContainerWorldTile();

                        containerTile.x = tileNumX * Game.tileSize;
                        containerTile.y = tileNumY * Game.tileSize;
                        containerTile.userPlaced = true;
                        containerTile.type = Player.inventoryTile;
                        containerTile.contents = new ConcurrentHashMap<>();

                        currentWorld.getWorldData().add(containerTile);
                    } else {
                        currentWorld.placeTile(tileNumX * Game.tileSize, tileNumY * Game.tileSize,
                                Player.inventoryTile.toString().toLowerCase(), true);
                    }

                    if (Game.multiplayerEnabled) {
                        ParsedPacket packetPayload = new ParsedPacket();
                        packetPayload.initGuard(MultiplayerGameState.player.authUUID);

                        WorldTile tile = new WorldTile();
                        tile.x = tileNumX * Game.tileSize;
                        tile.y = tileNumY * Game.tileSize;
                        tile.type = Player.inventoryTile;

                        try {
                            packetPayload.content.put("tile", new ObjectMapper().writeValueAsString(tile));
                        } catch (JsonProcessingException e) {
                            Logger.error(e);
                            Logger.error("Failed to format packet");
                            System.exit(-1);
                        }

                        new PlaceTilePacket(packetPayload).writeData(MultiplayerGameState.serverConnection);
                    }
                    Player.inventory.removeItem(Player.selectedInventorySlot, 1);
                }
            }
        }

    }

}
