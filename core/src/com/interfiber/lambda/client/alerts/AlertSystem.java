package com.interfiber.lambda.client.alerts;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.interfiber.lambda.client.Assets;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class AlertSystem {

    private final List<AlertInfo> alertList = new ArrayList<>();
    private final int TOAST_HEIGHT = 120;
    private final int maxAlertLimit = 4;
    private int height = 0;

    public void createToast(String content) {
        AlertInfo i = new AlertInfo();
        i.content = content;
        i.createTime = Instant.now();
        alertList.add(i);
    }

    private Toast createToastFromInfo(AlertInfo alertInfo) {
        height += TOAST_HEIGHT;
        alertInfo.y = height;
        Toast t = new Toast(alertInfo.content, Toast.Length.SHORT, Assets.manager.get("LanaPixel.ttf"), Color.BLACK, 0.5f, 0.65f, Color.YELLOW, alertInfo.y, null);
        return t;
    }


    public void rebuildAlertPositions() {
        int currentHeight = 0;
        for (AlertInfo alert : alertList) {
            currentHeight += TOAST_HEIGHT;
            alert.y = currentHeight;
        }
    }

    public void render() {
        height = 0;
        boolean shouldRebuild = false;
        Instant currentTime = Instant.now();

        for (int i = 0; i < alertList.size(); i++) {

            // make sure we cannot display too many alerts at the same time, NO SCREEN CLUTTER!!
            if (i >= maxAlertLimit) {
                break;
            }
            AlertInfo alertInfo = alertList.get(i);

            // create toast
            Toast t = this.createToastFromInfo(alertInfo);
            t.render(Gdx.graphics.getDeltaTime());

            long distance = Duration.between(alertInfo.createTime, currentTime).toMillis();

            if (distance > 3000) {
                alertList.remove(alertInfo);
                shouldRebuild = true;
            }
        }

        if (shouldRebuild) {
            this.rebuildAlertPositions();
        }
    }
}
