package com.interfiber.lambda.client.options;

public abstract class GameOption {
    public abstract String getStorageKey();

    public abstract String getDisplayName();

    public abstract String getDisplayDesc();

    public abstract OptionType getOptionType();

    public abstract String getDefaultValue();
}
