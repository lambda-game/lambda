package com.interfiber.lambda.client.options;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.world.WorldSaver;
import org.tinylog.Logger;

import java.io.*;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class OptionStorage {
    public static String getOptionsFile() {
        String storageDir = WorldSaver.getStorageDir();
        if (!new File(storageDir).exists()) {
            new File(storageDir).mkdirs();
        }
        // LAF: Lambda Options File
        String optionsFile = String.valueOf(Paths.get(storageDir, "options.laf"));
        if (!new File(optionsFile).exists()) {
            try {
                new File(optionsFile).createNewFile();
            } catch (IOException e) {
                Logger.error(e);
                throw new IllegalStateException("Failed to create game options file at: " + optionsFile);
            }
        }
        return optionsFile;
    }

    public static void loadOptions() {
        Logger.info("Loading game options");
        String optionsFile = getOptionsFile();
        Logger.info("Parsing options file...");
        File optionsFileInput = new File(optionsFile);
        Scanner reader;
        try {
            reader = new Scanner(optionsFileInput);
        } catch (FileNotFoundException e) {
            Logger.error(e);
            throw new IllegalStateException("Options file existed, but now cannot be found!");
        }
        int line = 1;
        while (reader.hasNext()) {
            String out = reader.next();
            if (out.equals("setBooleanOption")) {
                String optionKey = reader.next();
                String optionValue = reader.next();
                Logger.info("Found boolean option: " + optionKey + " with value: " + optionValue);
                Game.options.setBooleanOption(optionKey, Boolean.parseBoolean(optionValue));
            } else if (out.equals("setStringOption")) {
                String optionKey = reader.next();
                String optionValue = reader.next();
                Logger.error("setStringOption is not yet supported by LAF!");
            }
            line++;
        }

    }

    public static void writeOptionsFile() {
        Logger.info("Updating options on disk");
        String optionsFile = getOptionsFile();
        try {
            new FileWriter(optionsFile, false).close();
        } catch (IOException e) {
            Logger.error(e);
            throw new RuntimeException("Failed to clear contents of options file");
        }

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(optionsFile);
        } catch (FileNotFoundException e) {
            Logger.error(e);
            throw new RuntimeException("Failed to open options file!");
        }
        HashMap<String, GameOption> booleanOptions = OptionManager.getOptions();
        for (Map.Entry<String, GameOption> set : booleanOptions.entrySet()) {
            if (set.getValue().getOptionType() == OptionType.BOOLEAN_VALUE) {
                boolean value;
                if (!Game.options.booleanOptionIsSet(set.getValue().getStorageKey())) {
                    value = Boolean.parseBoolean(set.getValue().getDefaultValue());
                } else {
                    value = Game.options.getBooleanOption(set.getValue().getStorageKey());
                }
                writer.print("setBooleanOption " + set.getValue().getStorageKey() + " " + value + "\n");
            } else {
                Logger.error("Invalid option type: " + set.getValue().getOptionType());
            }
        }
        writer.close();
    }
}
