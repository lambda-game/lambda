package com.interfiber.lambda.client.options.gameoptions;

import com.interfiber.lambda.client.options.GameOption;
import com.interfiber.lambda.client.options.OptionType;

public class EnableDiscordRPCOption extends GameOption {

    @Override
    public String getDefaultValue() {
        return "true";
    }

    @Override
    public String getDisplayDesc() {
        return "If enabled, lambda will set your discord status";
    }

    @Override
    public String getDisplayName() {
        return "Enable Discord RPC";
    }

    @Override
    public OptionType getOptionType() {
        return OptionType.BOOLEAN_VALUE;
    }

    @Override
    public String getStorageKey() {
        return "lambda.discord.enable";
    }
}
