package com.interfiber.lambda.client.options.gameoptions;

import com.interfiber.lambda.client.options.GameOption;
import com.interfiber.lambda.client.options.OptionType;

public class UseVsyncOption extends GameOption {

    @Override
    public String getStorageKey() {
        return "lambda.graphics.vsync.enabled";
    }

    @Override
    public String getDisplayName() {
        return "VSync";
    }

    @Override
    public String getDisplayDesc() {
        return "Vsync will cap the games FPS at your monitors refresh rate";
    }

    @Override
    public OptionType getOptionType() {
        return OptionType.BOOLEAN_VALUE;
    }

    @Override
    public String getDefaultValue() {
        return "true";
    }

}
