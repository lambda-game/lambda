package com.interfiber.lambda.client.options.gameoptions;

import com.interfiber.lambda.client.options.GameOption;
import com.interfiber.lambda.client.options.OptionType;

public class UseSystemCursorOption extends GameOption {
    @Override
    public String getStorageKey() {
        return "lambda.graphics.useOsCursor";
    }

    @Override
    public String getDisplayName() {
        return "Use OS cursor";
    }

    @Override
    public String getDisplayDesc() {
        return "If enabled the lambda custom cursor will not be used";
    }

    @Override
    public OptionType getOptionType() {
        return OptionType.BOOLEAN_VALUE;
    }

    @Override
    public String getDefaultValue() {
        return "false";
    }
}
