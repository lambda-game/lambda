package com.interfiber.lambda.client.options;

public enum OptionType {
    NUMBER_VALUE,
    STRING_VALUE,
    BOOLEAN_VALUE
}
