package com.interfiber.lambda.client.options;

import java.util.HashMap;

public class GameOptions {
    private HashMap<String, Boolean> booleanOptions = new HashMap<>();

    public void setBooleanOption(String key, boolean value) {
        booleanOptions.put(key, value);
    }

    public boolean getBooleanOption(String key) {
        return booleanOptions.get(key);
    }

    public boolean booleanOptionIsSet(String key) {
        return booleanOptions.containsKey(key);
    }
}
