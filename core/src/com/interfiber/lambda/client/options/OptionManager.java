package com.interfiber.lambda.client.options;

import org.tinylog.Logger;

import java.util.HashMap;
import java.util.Map;

public class OptionManager {
    private static HashMap<String, GameOption> gameOptions = new HashMap<String, GameOption>();

    public static void addOption(String optionName, GameOption option) {
        gameOptions.put(optionName, option);
    }

    public static HashMap<String, GameOption> getOptions() {
        return gameOptions;
    }

    public static GameOption getOptionWithKey(String storageKey) {
        GameOption option = null;
        for (Map.Entry<String, GameOption> set : gameOptions.entrySet()) {
            if (set.getValue().getStorageKey() == storageKey) {
                option = set.getValue();
                break;
            }
        }
        if (option == null) {
            Logger.error("Failed to find option with key: " + storageKey);
        }
        return option;
    }

    public static GameOption getOption(String name) {
        return gameOptions.get(name);
    }
}
