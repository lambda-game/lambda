package com.interfiber.lambda.client.multiplayer;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.server.player.ClientPlayer;
import org.tinylog.Logger;

import java.util.Objects;

public class MultiplayerRendering {

    private static BitmapFont font;

    public static void prepRendering() {
        Logger.info("Loading bitmap font for multiplayer rendering");
        font = new BitmapFont();
        FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter usernameFontGenerated = new FreeTypeFontGenerator.FreeTypeFontParameter();
        usernameFontGenerated.size = 9;
        font = fontGenerator.generateFont(usernameFontGenerated);
    }

    public static void renderPlayers(SpriteBatch batch) {
        for (ClientPlayer player : MultiplayerGameState.players) {

            if (player.currentWorld == Game.currentWorld) {

                String upTexture = "player_walk_up.png";
                String downTexture = "player_walk_down.png";
                String leftTexture = "player_walk_left.png";
                String rightTexture = "player_walk_right.png";

                if (player.isSwimming) {
                    upTexture = "player_walk_up_water.png";
                    downTexture = "player_walk_down_water.png";
                    leftTexture = "player_walk_left_water.png";
                    rightTexture = "player_walk_right_water.png";
                }

                // render player
                if (Objects.equals(player.facing, "down")) {
                    batch.draw(Assets.manager.get(downTexture, Texture.class), player.x, player.y);
                } else if (Objects.equals(player.facing, "up")) {
                    batch.draw(Assets.manager.get(upTexture, Texture.class), player.x, player.y);
                } else if (Objects.equals(player.facing, "left")) {
                    batch.draw(Assets.manager.get(leftTexture, Texture.class), player.x, player.y);
                } else if (Objects.equals(player.facing, "right")) {
                    batch.draw(Assets.manager.get(rightTexture, Texture.class), player.x, player.y);
                }

                // draw username above player
                font.draw(batch, player.username, player.x - player.username.length(), player.y + 32);
            }

        }

        // draw our own username
        font.draw(batch, MultiplayerGameState.player.username, Player.x - MultiplayerGameState.player.username.length(), Player.y + 32);
    }

    public static void dispose() {
        font.dispose();
    }
}
