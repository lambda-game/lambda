package com.interfiber.lambda.client.multiplayer;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.server.MultiplayerGame;
import org.tinylog.Logger;

public class MultiplayerThread implements Runnable {
    @Override
    public void run() {
        Logger.info("Starting multiplayer");

        // can't use shift in the server ip input, so we convert it here
        String ip = Game.serverIp.replace(";", ":");
        int port = MultiplayerGame.port;

        if (ip.contains(":")) {
            String[] split = ip.split(":");
            ip = split[0];
            port = Integer.parseInt(split[1]);
        }
        try {
            LambdaMultiplayerClient.connectToServer(ip, port);
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Error occurred during multiplayer client runtime!");
            System.exit(-1);
        }
    }
}
