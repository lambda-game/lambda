package com.interfiber.lambda.client.multiplayer.networking;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.interfiber.lambda.server.networking.PacketManager;
import com.interfiber.lambda.server.networking.packets.Packet;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.*;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.tinylog.Logger;

public class ClientHandler extends SimpleChannelInboundHandler<String> {
    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {

        // parse the content
        ParsedPacket parsedPacket = new ObjectMapper().readValue(msg, ParsedPacket.class);
        Packet.PacketType packetType = PacketManager.lookupPacket(parsedPacket.packetId);

        if (packetType == null) {
            throw new IllegalStateException("PacketType is null!");
        }
        if (packetType == Packet.PacketType.PLAY) {
            new PlayPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.PLAYER_ADDED) {
            new PlayerAddedPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.PLAYER_POSITION_UPDATED_BROADCAST) {
            new PlayerPositionUpdateBroadcastPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.PLAYER_REMOVED) {
            new PlayerRemovedPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.SEND_TOAST) {
            new SendClientToastPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.TILE_PLACED) {
            new TilePlacedPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.KICKED) {
            new KickPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.TILE_REMOVED) {
            new TileRemovedPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.INVENTORY_UPDATE) {
            new PlayerInventoryUpdatePacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.CHAT_MESSAGE) {
            new ChatMessagePacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.SYSTEM_CHAT_MESSAGE) {
            new SystemChatMessagePacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.UPDATED_WEATHER) {
            new WeatherUpdatedPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.CHEST_UPDATED) {
            new StorageContainerUpdatedPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.SWITCHED_WORLD) {
            new SwitchedWorldPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.PLAYER_SWITCHED_WORLD) {
            new PlayerSwitchedWorldPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.STORAGE_CONTAINER_PLACED) {
            new StorageContainerPlacedPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.SOIL_HOED) {
            new SoilHoedPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.SOIL_WATERED) {
            new SoilWateredPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.CROP_PLANTED) {
            new CropPlantedPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.CROP_GROWN) {
            new CropGrownPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.CROP_HARVESTED) {
            new CropHarvestedPacket(parsedPacket).handlePacket(ctx.channel());
        } else if (packetType == Packet.PacketType.TIME_UPDATED) {
            new TimeUpdatedPacket(parsedPacket).handlePacket(ctx.channel());
        }else {
            Logger.error("Invalid packet from server! Packet ID is: " + parsedPacket.packetId);
            Logger.debug("Packet content: " + msg);
            System.exit(-1);
        }

    }
}
