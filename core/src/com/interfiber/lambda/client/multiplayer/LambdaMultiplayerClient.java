package com.interfiber.lambda.client.multiplayer;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.multiplayer.networking.ClientHandler;
import com.interfiber.lambda.server.networking.PacketManager;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.PlayerLoginPacket;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import org.tinylog.Logger;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

public class LambdaMultiplayerClient {
    public static void connectToServer(String serverIp, int port) throws IOException, ExecutionException, InterruptedException, TimeoutException {
        Logger.info("Connecting to server: " + serverIp);
        Logger.info("Loading packet list");
        PacketManager.init();
        Game.multiplayerEnabled = true;

        EventLoopGroup group = new NioEventLoopGroup();
        try {
            Bootstrap b = new Bootstrap();
            b.group(group) // Set EventLoopGroup to handle all eventsf for client.
                    .channel(NioSocketChannel.class)// Use NIO to accept new connections.
                    .handler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        public void initChannel(SocketChannel ch) throws Exception {
                            ChannelPipeline p = ch.pipeline();
                            p.addLast(new DelimiterBasedFrameDecoder(Integer.MAX_VALUE, Delimiters.lineDelimiter()));
                            p.addLast(new StringDecoder());
                            p.addLast(new StringEncoder());

                            // This is our custom client handler which will have logic for chat.
                            p.addLast(new ClientHandler());

                        }
                    });

            // Start the client.
            ChannelFuture f = b.connect(serverIp, port).sync();


            Channel channel = f.sync().channel();
            MultiplayerGameState.serverConnection = channel;

            ParsedPacket packet = new ParsedPacket();
            packet.content.put("authToken", Game.authToken); // game server will assume the auth token is from the official auth server

            new PlayerLoginPacket(packet).writeData(channel);
            Logger.info("Sent login packet");
            Game.serverConnectStatus = "Downloading server world...";
            // Wait until the connection is closed.
            f.channel().closeFuture().sync();
        } finally {
            // Shut down the event loop to terminate all threads.
            group.shutdownGracefully();
        }

    }
}
