package com.interfiber.lambda.client.multiplayer;

import com.interfiber.lambda.server.player.ClientPlayer;
import com.interfiber.lambda.server.player.OnlinePlayer;
import io.netty.channel.Channel;

import java.util.ArrayList;

public class MultiplayerGameState {
    public static ArrayList<ClientPlayer> players = new ArrayList<>();
    public static OnlinePlayer player;
    public static Channel serverConnection;
}
