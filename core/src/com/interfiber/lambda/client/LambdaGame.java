package com.interfiber.lambda.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.alerts.AlertSystem;
import com.interfiber.lambda.client.audio.AudioManager;
import com.interfiber.lambda.client.entitys.EntityManager;
import com.interfiber.lambda.client.entitys.PlayerEntity;
import com.interfiber.lambda.client.entitys.controllers.PlayerEntityController;
import com.interfiber.lambda.client.lighting.LightManager;
import com.interfiber.lambda.client.misc.Cursor;
import com.interfiber.lambda.client.options.OptionManager;
import com.interfiber.lambda.client.options.OptionStorage;
import com.interfiber.lambda.client.options.gameoptions.UseSystemCursorOption;
import com.interfiber.lambda.client.options.gameoptions.UseVsyncOption;
import com.interfiber.lambda.client.overlays.Overlay;
import com.interfiber.lambda.client.overlays.OverlayManager;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.player.PlayerInventory;
import com.interfiber.lambda.client.screens.GameScreen;
import com.interfiber.lambda.client.screens.ScreenManager;
import com.interfiber.lambda.client.screens.SplashScreen;
import com.interfiber.lambda.client.ticks.TickRunnable;
import org.tinylog.Logger;

import java.util.ArrayList;

public class LambdaGame extends com.badlogic.gdx.Game {

    ArrayList<Overlay> overlays = OverlayManager.getOverlays();
    SpriteBatch overlaySpriteBatch;
    Pixmap pixmap;
    boolean renderOverlays = true;

    @Override
    public void create() {

        Game.screenManager = new ScreenManager(this);

        // load all assets
        Logger.info("Loading game assets");
        Assets.loadAssets();

        // load game cursor
        Logger.info("Loading game cursor");
        Cursor.create();

        // load alert system
        Logger.info("Loading alert system");
        Game.alertSystem = new AlertSystem();


        // load game options
        OptionStorage.loadOptions();
        if (Game.options.booleanOptionIsSet(OptionManager.getOption("vsync").getStorageKey())) {
            Gdx.graphics
                    .setVSync(Game.options.getBooleanOption(new UseVsyncOption().getStorageKey()));
        }
        if (Game.options
                .booleanOptionIsSet(OptionManager.getOption("discordrpc").getStorageKey())) {
            if (Game.options
                    .getBooleanOption(OptionManager.getOption("discordrpc").getStorageKey())) {
                Logger.info("Loading discord RPC");
                Thread discordThread = new Thread(new Discord());
                discordThread.setName("Discord-RPC-Thread");
                discordThread.start();
            }
        } else {
            Logger.info("Loading discord RPC");
            Thread discordThread = new Thread(new Discord());
            discordThread.setName("Discord-RPC-Thread");
            discordThread.start();
        }

        overlaySpriteBatch = new SpriteBatch();
        // create overlays
        Logger.info("Creating overlays...");
        for (Overlay overlay : overlays) {
            overlay.create();
        }

        // load audio
        Logger.info("Loading audio...");
        AudioManager.addSfx("click3", Assets.manager.get("audio/sfx/click3.ogg"));
        AudioManager.addMusic("caves_theme_ambient",
                Assets.manager.get("audio/music/cave_theme_ambient.ogg"));
        AudioManager.addMusic("thunderstorm_ambient", Assets.manager.get("audio/music/thunderstorm_ambient.ogg"));

        // lighting system
        Logger.info("Loading lighting system");
        Game.lightManager = new LightManager();
        Game.lightManager.init();

        // add all the entity's into memory, and load the player
        Logger.info("Loading player entity");
        PlayerEntity player = new PlayerEntity();
        player.attachController(new PlayerEntityController());
        player.attachCollider(Player.height, Player.width);
        EntityManager.addEntity("player", player);
        Player.inventory = new PlayerInventory();

        if (Game.options.booleanOptionIsSet(OptionManager.getOption("oscursor").getStorageKey())) {
            if (!Game.options.getBooleanOption(new UseSystemCursorOption().getStorageKey())) {
                Cursor.setCustomCursor();
            }
        } else {
            // if we dont have the option set than enable the custom cursor
            Cursor.setCustomCursor();
        }

        // set screen
        setScreen(new SplashScreen());
    }


    @Override
    public void render() {
        Gdx.input.setCursorCatched(Game.hiddenCursor);

        super.render();
        if (getScreen() instanceof GameScreen) {
            GameScreen currentGameScreen = (GameScreen) getScreen();
            renderOverlays = currentGameScreen.renderOverlays();
        }
        if (renderOverlays) {
            for (Overlay overlay : overlays) {
                overlay.render(overlaySpriteBatch);
            }
        }

        Game.alertSystem.render();
        // run game tick
        TickRunnable.run();
    }

    @Override
    public void dispose() {
        super.dispose();

        // dispose overlay sprite batches
        overlaySpriteBatch.dispose();

        // dispose the cursor pixmap
        if (pixmap != null) {
            pixmap.dispose();
        }

        // dispose overlays
        for (Overlay overlay : overlays) {
            overlay.dispose();
        }

        // dispose light manager
        Game.lightManager.dispose();

        // dispose item mouse
        Cursor.dispose();

        // dispose game assets
        Assets.dispose();
    }

    @Override
    public void resize(int width, int height) {
    }
}
