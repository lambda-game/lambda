package com.interfiber.lambda.client.ticks;

import com.badlogic.gdx.utils.TimeUtils;
import com.interfiber.lambda.client.farming.Crop;
import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.farming.SerializableCrop;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.clientbound.CropGrownPacket;
import org.tinylog.Logger;

public class TickableFarm implements Tickable {

    @Override
    public void tick() {
        for (int x = 0; x < CropManager.getCropData().size(); x++) {
            SerializableCrop c = CropManager.getCropData().get(x);
            Crop crop = CropManager.cropTypeToCrop(c.cropType);

            int timeToGrow = crop.getGrowTime();
            if (TimeUtils.timeSinceMillis(c.startTime) >= timeToGrow * 1000 && !c.hasGrown && c.hasBeenWatered) {
                Logger.info("Crop: " + crop.getCropType() + " has grown");
                CropManager.getCropData().remove(c);
                c.hasGrown = true;
                CropManager.getCropData().add(c);

                if (MultiplayerGame.server != null) {
                    ParsedPacket packet = new ParsedPacket();
                    packet.content.put("cropX", c.x);
                    packet.content.put("cropY", c.y);

                    MultiplayerGame.sendPacketToPlayers(new CropGrownPacket(packet));
                }
            }
        }
    }
}
