package com.interfiber.lambda.client.ticks;

public interface Tickable {
    void tick();
}
