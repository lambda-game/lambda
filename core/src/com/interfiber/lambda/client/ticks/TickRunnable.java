package com.interfiber.lambda.client.ticks;

import com.badlogic.gdx.utils.TimeUtils;
import com.interfiber.lambda.client.Game;
import org.tinylog.Logger;

public class TickRunnable {
    private static long startTime = -1;

    public static void run() {
        if (startTime == -1) {
            startTime = TimeUtils.millis();
        }
        try {
            if (TimeUtils.timeSinceMillis(startTime) > 1000) {
                // run tick events
                if (Game.currentSaveFile != -1) {
                    Game.gameTicks += 1;
                    Game.tickManager.runTickEvents();
                }
            }
        } catch (Exception e) {
            Logger.error(e);
            Logger.error("Error occurred while a game tick was in progress");
        }
    }
}
