package com.interfiber.lambda.client.ticks;

import com.interfiber.lambda.client.Game;

import java.util.ArrayList;

public class TickManager {
    private final ArrayList<Tickable> ticks = new ArrayList<>();

    public void addTickSubscriber(Tickable tick) {
        ticks.add(tick);
    }

    public void runTickEvents() {
        if (Game.multiplayerEnabled) return;

        for (Tickable tick : ticks) {
            tick.tick();
        }
    }

}
