package com.interfiber.lambda.client.crafting;

import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class RecipeManager {
    private static final List<Recipe> recipes = new ArrayList<>();

    public static void addRecipe(Recipe recipe) {
        recipes.add(recipe);
    }

    public static List<Recipe> getWorkbenchRecipes() {
        List<Recipe> outputList = new ArrayList<>();

        for (Recipe recipe : recipes) {
            if (recipe.getRecipeType() == RecipeType.WORKBENCH_RECIPE) {
                outputList.add(recipe);
            }
        }
        return outputList;
    }

    public static List<Recipe> getPortableRecipes() {
        List<Recipe> outputList = new ArrayList<>();

        for (Recipe recipe : recipes) {
            if (recipe.canCraftInPortableCrafting() && recipe.getRecipeType() == RecipeType.WORKBENCH_RECIPE) {
                outputList.add(recipe);
            }
        }
        return outputList;
    }


    public static Recipe getRecipe(String recipeName) {
        Recipe outputRecipe = null;
        for (Recipe recipe : recipes) {
            if (Objects.equals(recipe.getRecipeName(), recipeName)) {
                outputRecipe = recipe;
                break;
            }
        }
        if (outputRecipe == null) {
            Logger.warn("Failed to index recipes: no result found for query");
        }
        return outputRecipe;
    }

    public static List<Recipe> getFurnaceRecipes() {
        List<Recipe> outputList = new ArrayList<>();

        for (Recipe recipe : recipes) {
            if (!recipe.canCraftInPortableCrafting() && recipe.getRecipeType() == RecipeType.FURNACE_RECIPE) {
                outputList.add(recipe);
            }
        }
        return outputList;
    }
}
