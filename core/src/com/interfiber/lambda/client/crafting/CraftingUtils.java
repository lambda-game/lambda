package com.interfiber.lambda.client.crafting;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.overlays.CraftingOverlay;
import com.interfiber.lambda.client.overlays.OverlayManager;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.player.PlayerInventory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CraftingUtils {
    private static final CraftingOverlay craftingOverlay = new CraftingOverlay();

    public static boolean canCraftItem(Recipe craftRecipe) {
        return canCraftItem(craftRecipe, Player.inventory);
    }

    public static boolean canCraftItem(Recipe craftRecipe, PlayerInventory inventory) {
        int check = 0;
        ConcurrentHashMap<Integer, Item> playerItems = inventory.getItems();
        for (Map.Entry<Integer, Item> set : playerItems.entrySet()) {
            Item playerItem = set.getValue();
            Ingredient ingredient = new Ingredient();
            ingredient.itemType = playerItem.getType();
            ingredient.amount = playerItem.amount;
            for (Ingredient ingredient1 : craftRecipe.getItems()) {
                // BUG HERE
                if (ingredient.amount >= ingredient1.amount && ingredient.itemType == ingredient1.itemType) {
                    check += 1;
                    break;
                }
            }
        }
        return check == craftRecipe.getItems().size();
    }

    public static void rebuildOverlayRecipeCache() {
        CraftingOverlay newOverlay = (CraftingOverlay) OverlayManager.getOverlay(craftingOverlay);
        if (Game.craftingMenuMode == CraftingMenuMode.PORTABLE_CRAFTING) {
            newOverlay.recipes = RecipeManager.getPortableRecipes();
            newOverlay.items = RecipeManager.getPortableRecipes().size();
        } else if (Game.craftingMenuMode == CraftingMenuMode.WORKBENCH_CRAFTING) {
            newOverlay.recipes = RecipeManager.getWorkbenchRecipes();
            newOverlay.items = RecipeManager.getWorkbenchRecipes().size();
        } else if (Game.craftingMenuMode == CraftingMenuMode.FURNACE_CRAFTING) {
            newOverlay.recipes = RecipeManager.getFurnaceRecipes();
            newOverlay.items = RecipeManager.getFurnaceRecipes().size();
        } else {
            throw new RuntimeException("Unsupported crafting menu mode value: " + Game.craftingMenuMode);
        }
    }
}
