package com.interfiber.lambda.client.crafting;

import com.interfiber.lambda.client.items.ItemType;

public class Ingredient {
    public ItemType itemType;
    public int amount;
}
