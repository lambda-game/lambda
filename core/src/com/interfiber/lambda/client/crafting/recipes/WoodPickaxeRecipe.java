package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;
import com.interfiber.lambda.client.items.WoodPickaxeItem;

import java.util.ArrayList;
import java.util.List;

public class WoodPickaxeRecipe extends Recipe {
    @Override
    public List<Ingredient> getItems() {
        Ingredient stickItem = new Ingredient();
        stickItem.itemType = ItemType.STICK;
        stickItem.amount = 2;

        Ingredient woodItem = new Ingredient();
        woodItem.itemType = ItemType.WOOD;
        woodItem.amount = 3;

        List<Ingredient> items = new ArrayList<>();
        items.add(stickItem);
        items.add(woodItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "Wood Pick";
    }

    @Override
    public Item getOutputItem() {
        return new WoodPickaxeItem();
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.WORKBENCH_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return false;
    }
}
