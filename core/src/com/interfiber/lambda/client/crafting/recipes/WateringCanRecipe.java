package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;
import com.interfiber.lambda.client.items.WateringCanItem;

import java.util.ArrayList;
import java.util.List;

public class WateringCanRecipe extends Recipe {

    @Override
    public List<Ingredient> getItems() {
        Ingredient ironItem = new Ingredient();
        ironItem.itemType = ItemType.IRON_BAR;
        ironItem.amount = 5;


        List<Ingredient> items = new ArrayList<>();
        items.add(ironItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "Watering Can";
    }

    @Override
    public Item getOutputItem() {
        return new WateringCanItem();
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.WORKBENCH_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return false;
    }
}
