package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.BreadItem;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;

import java.util.ArrayList;
import java.util.List;

public class BreadRecipe extends Recipe {
    @Override
    public List<Ingredient> getItems() {
        Ingredient wheatItem = new Ingredient();
        wheatItem.itemType = ItemType.WHEAT;
        wheatItem.amount = 4;
        List<Ingredient> items = new ArrayList<>();
        items.add(wheatItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "Bread";
    }

    @Override
    public Item getOutputItem() {
        Item i = new BreadItem();
        i.amount = 2;
        return i;
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.WORKBENCH_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return false;
    }
}
