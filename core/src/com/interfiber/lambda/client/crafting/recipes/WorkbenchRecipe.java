package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;
import com.interfiber.lambda.client.items.WorkbenchItem;

import java.util.ArrayList;
import java.util.List;

public class WorkbenchRecipe extends Recipe {

    @Override
    public List<Ingredient> getItems() {
        Ingredient woodItem = new Ingredient();
        woodItem.itemType = ItemType.WOOD;
        woodItem.amount = 4;
        List<Ingredient> items = new ArrayList<>();
        items.add(woodItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "Workbench";
    }

    @Override
    public Item getOutputItem() {
        return new WorkbenchItem();
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.WORKBENCH_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return true;
    }

}
