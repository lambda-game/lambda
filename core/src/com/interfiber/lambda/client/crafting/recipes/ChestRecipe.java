package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.ChestItem;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;

import java.util.ArrayList;
import java.util.List;

public class ChestRecipe extends Recipe {
    @Override
    public List<Ingredient> getItems() {
        Ingredient ironItem = new Ingredient();
        ironItem.itemType = ItemType.IRON_BAR;
        ironItem.amount = 1;

        Ingredient woodItem = new Ingredient();
        woodItem.itemType = ItemType.WOOD;
        woodItem.amount = 5;

        List<Ingredient> items = new ArrayList<>();
        items.add(woodItem);
        items.add(ironItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "CHEST";
    }

    @Override
    public Item getOutputItem() {
        return new ChestItem();
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.WORKBENCH_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return false;
    }
}
