package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.FurnaceItem;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;

import java.util.ArrayList;
import java.util.List;

public class FurnaceRecipe extends Recipe {
    @Override
    public List<Ingredient> getItems() {
        Ingredient stoneItem = new Ingredient();
        stoneItem.itemType = ItemType.ROCK;
        stoneItem.amount = 9;
        List<Ingredient> items = new ArrayList<>();
        items.add(stoneItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "Furnace";
    }

    @Override
    public Item getOutputItem() {
        return new FurnaceItem();
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.WORKBENCH_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return false;
    }
}
