package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;
import com.interfiber.lambda.client.items.StonePickaxeItem;

import java.util.ArrayList;
import java.util.List;

public class StonePickaxeRecipe extends Recipe {
    @Override
    public List<Ingredient> getItems() {
        Ingredient stickItem = new Ingredient();
        stickItem.itemType = ItemType.STICK;
        stickItem.amount = 2;

        Ingredient rockItem = new Ingredient();
        rockItem.itemType = ItemType.ROCK;
        rockItem.amount = 3;

        List<Ingredient> items = new ArrayList<>();
        items.add(stickItem);
        items.add(rockItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "Stone Pick";
    }

    @Override
    public Item getOutputItem() {
        return new StonePickaxeItem();
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.WORKBENCH_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return false;
    }
}
