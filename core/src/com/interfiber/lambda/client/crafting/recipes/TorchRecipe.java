package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;
import com.interfiber.lambda.client.items.TorchItem;

import java.util.ArrayList;
import java.util.List;

public class TorchRecipe extends Recipe {
    @Override
    public List<Ingredient> getItems() {
        Ingredient coalItem = new Ingredient();
        coalItem.itemType = ItemType.COAL;
        coalItem.amount = 2;

        Ingredient stickItem = new Ingredient();
        stickItem.itemType = ItemType.STICK;
        stickItem.amount = 2;

        List<Ingredient> items = new ArrayList<>();
        items.add(coalItem);
        items.add(stickItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "Torch";
    }

    @Override
    public Item getOutputItem() {
        TorchItem torchItem = new TorchItem();
        torchItem.amount = 6;

        return torchItem;
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.WORKBENCH_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return true;
    }
}
