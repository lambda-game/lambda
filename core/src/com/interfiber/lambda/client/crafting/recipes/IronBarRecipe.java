package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.IronBarItem;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;

import java.util.ArrayList;
import java.util.List;

public class IronBarRecipe extends Recipe {
    @Override
    public List<Ingredient> getItems() {
        Ingredient ironOreItem = new Ingredient();
        ironOreItem.itemType = ItemType.IRON_ORE;
        ironOreItem.amount = 5;
        List<Ingredient> items = new ArrayList<>();
        items.add(ironOreItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "Iron Bar";
    }

    @Override
    public Item getOutputItem() {
        return new IronBarItem();
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.FURNACE_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return false;
    }
}
