package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.CoalItem;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;

import java.util.ArrayList;
import java.util.List;

public class CharcoalRecipe extends Recipe {
    @Override
    public List<Ingredient> getItems() {
        Ingredient woodItem = new Ingredient();
        woodItem.itemType = ItemType.WOOD;
        woodItem.amount = 10;
        List<Ingredient> items = new ArrayList<>();
        items.add(woodItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "Charcoal";
    }

    @Override
    public Item getOutputItem() {
        CoalItem item = new CoalItem();
        item.amount = 5;
        return item;
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.FURNACE_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return false;
    }
}
