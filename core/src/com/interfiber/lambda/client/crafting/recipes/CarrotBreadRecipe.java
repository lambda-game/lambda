package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.CarrotBreadItem;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;

import java.util.ArrayList;
import java.util.List;

public class CarrotBreadRecipe extends Recipe {

    @Override
    public List<Ingredient> getItems() {
        Ingredient wheatItem = new Ingredient();
        wheatItem.itemType = ItemType.BREAD;
        wheatItem.amount = 2;

        Ingredient carrotItem = new Ingredient();
        carrotItem.itemType = ItemType.CARROT;
        carrotItem.amount = 2;

        List<Ingredient> items = new ArrayList<>();
        items.add(wheatItem);
        items.add(carrotItem);
        return items;
    }

    @Override
    public String getRecipeName() {
        return "Carrot Bread";
    }

    @Override
    public Item getOutputItem() {
        Item i = new CarrotBreadItem();
        i.amount = 2;
        return i;
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.WORKBENCH_RECIPE;
    }

    @Override
    public boolean canCraftInPortableCrafting() {
        return false;
    }
}
