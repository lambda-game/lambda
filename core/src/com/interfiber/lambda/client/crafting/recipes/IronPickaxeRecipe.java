package com.interfiber.lambda.client.crafting.recipes;

import com.interfiber.lambda.client.crafting.Ingredient;
import com.interfiber.lambda.client.crafting.Recipe;
import com.interfiber.lambda.client.crafting.RecipeType;
import com.interfiber.lambda.client.items.IronPickaxeItem;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;

import java.util.ArrayList;
import java.util.List;

public class IronPickaxeRecipe extends Recipe {

    @Override
    public boolean canCraftInPortableCrafting() {
        return false;
    }

    @Override
    public List<Ingredient> getItems() {
        Ingredient stickItem = new Ingredient();
        stickItem.itemType = ItemType.STICK;
        stickItem.amount = 2;

        Ingredient ironBarItem = new Ingredient();
        ironBarItem.itemType = ItemType.IRON_BAR;
        ironBarItem.amount = 3;

        List<Ingredient> items = new ArrayList<>();
        items.add(stickItem);
        items.add(ironBarItem);
        return items;
    }

    @Override
    public Item getOutputItem() {
        return new IronPickaxeItem();
    }

    @Override
    public String getRecipeName() {
        return "Iron Pick";
    }

    @Override
    public RecipeType getRecipeType() {
        return RecipeType.WORKBENCH_RECIPE;
    }
}
