package com.interfiber.lambda.client.crafting;

import com.interfiber.lambda.client.items.Item;

import java.util.List;

public abstract class Recipe {
    public abstract List<Ingredient> getItems();

    public abstract String getRecipeName();

    public abstract Item getOutputItem();

    public abstract RecipeType getRecipeType();

    public abstract boolean canCraftInPortableCrafting();
}
