package com.interfiber.lambda.client.crafting;

public enum RecipeType {
    FURNACE_RECIPE,
    WORKBENCH_RECIPE
}
