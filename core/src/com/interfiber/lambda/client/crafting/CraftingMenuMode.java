package com.interfiber.lambda.client.crafting;

public enum CraftingMenuMode {
    PORTABLE_CRAFTING, // for using the portable crafting menu(open with C)
    WORKBENCH_CRAFTING, // for crafting in a workbench
    FURNACE_CRAFTING // for crafting with a furnace
}
