package com.interfiber.lambda.client.bootstrap;

import com.badlogic.gdx.Gdx;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemType;
import com.interfiber.lambda.client.items.ItemUtils;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.weather.ThunderstormWeather;
import com.interfiber.lambda.client.weather.WeatherUtils;
import com.interfiber.lambda.client.world.WorldSaver;
import org.tinylog.Logger;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Objects;

class DeveloperModeConsoleRunnable implements Runnable {

    @Override
    public void run() {
        JFrame window = new JFrame("Lambda developer console");
        BorderLayout borderLayout = new BorderLayout();
        window.setLayout(borderLayout);


        final JTextArea console = new JTextArea(10, 20);
        console.setEditable(false);
        JScrollPane sp = new JScrollPane(console);
        window.add(sp, BorderLayout.CENTER);

        final JTextField commandArea = new JTextField();
        window.add(commandArea, BorderLayout.SOUTH);
        commandArea.addActionListener(e -> {
            String s = e.getActionCommand();
            String cmd = commandArea.getText();
            console.setText(console.getText() + "command::info :: got text \"" + cmd + "\"\n");
            final String[] commands = cmd.split(" ");
            if (Objects.equals(commands[0], "save-world")) {
                console.setText(console.getText() + "command::info :: saving current world\n");
                if (Game.currentSaveFile == -1) {
                    console.setText(console.getText() + "command::error :: no world loaded!");
                    return;
                }
                Gdx.app.postRunnable(() -> {
                    try {
                        WorldSaver.runSave(Game.currentSaveFile);
                    } catch (IOException ex) {
                        throw new RuntimeException(ex);
                    }
                });
                console.setText(console.getText() + "command::info :: sent world save request\n");
            } else if (Objects.equals(commands[0], "toggle-gui-debug")) {
                Game.debugOverlayEnabled = !Game.debugOverlayEnabled;
                console.setText(console.getText() + "command::info :: toggled gui debug\n");
            } else if (Objects.equals(commands[0], "help")) {
                console.setText(console.getText()
                        + "Commands:\nhelp: display this message\nsave-world: save the current world\ntoggle-gui-debug: toggle the gui debug overlay\ncurrent-scene: print current scene ID\nfps: print FPS\ngive-item <item> <amount>: give the player an item\nget-seed: get the world seed\n");
            } else if (Objects.equals(commands[0], "fps")) {
                Gdx.app.postRunnable(() -> console.setText(console.getText()
                        + "command::info :: fps : " + Gdx.graphics.getFramesPerSecond() + "\n"));
            } else if (Objects.equals(commands[0], "give-item") && commands.length == 3) {
                String itemName = commands[1];
                int amount = Integer.parseInt(commands[2]);
                console.setText(console.getText() + "command::info :: giving item\n");
                Item item = ItemUtils.getItemFromId(ItemType.valueOf(itemName.toUpperCase()));
                if (item == null) {
                    console.setText(console.getText() + "command::info :: no such item type!\n");
                } else {
                    item.amount = amount;
                    Player.inventory.pickupItem(item);
                }
            } else if (Objects.equals(commands[0], "toggle-colliders")) {
                console.setText(console.getText() + "command::info :: toggling colliders\n");
                Game.debugColliderEnabled = !Game.debugColliderEnabled;
            } else if (Objects.equals(commands[0], "set-speed") && commands.length == 2) {
                console.setText(console.getText() + "command::info :: setting speed\n");
                Player.speed = Integer.parseInt(commands[1]);
            } else if (Objects.equals(commands[0], "enable-anti-water-slow")) {
                Player.swimmingSpeed = Player.speed;
            } else if (Objects.equals(commands[0], "disable-anti-water-slow")) {
                Player.swimmingSpeed = 35;
            } else if (Objects.equals(commands[0], "get-seed")) {
                console.setText(
                        console.getText() + "command::info :: seed : " + Game.worldSeed + "\n");
            } else if (Objects.equals(commands[0], "set-thunderstorm")) {
                console.setText(console.getText() + "command::info :: setting thunderstorm\n");
                WeatherUtils.setWeather(new ThunderstormWeather());
            } else {
                console.setText(console.getText()
                        + "command::info :: invalid developer console command! \n");
            }
            commandArea.setText("");
        });


        window.pack();
        window.setSize(500, 500);
        window.setVisible(true);
    }
}


public class DeveloperModeBootstrap {
    public static void bootstrapDeveloperMode() {
        Logger.info("Running developer mode bootstrap");

        Logger.debug("Starting developer mode console thread");

        SwingUtilities.invokeLater(new DeveloperModeConsoleRunnable());
    }
}
