package com.interfiber.lambda.client.bootstrap;

import com.interfiber.lambda.client.tiles.*;

public class TileBootstrap {
    public static void bootstrap() {
        TileLoader.addTile(TileType.COBBLESTONE, new CobblestoneTile());
        TileLoader.addTile(TileType.STONE, new StoneTile());
        TileLoader.addTile(TileType.GRASS, new GrassTile());
        TileLoader.addTile(TileType.WATER, new WaterTile());
        TileLoader.addTile(TileType.FOG, new FogTile());
        TileLoader.addTile(TileType.TREE, new TreeTile());
        TileLoader.addTile(TileType.WORKBENCH, new WorkbenchTile());
        TileLoader.addTile(TileType.INVENTORYSELECTED, new InventorySelectedTile());
        TileLoader.addTile(TileType.INVENTORYUNSELECTED, new InventoryUnselectedTile());
        TileLoader.addTile(TileType.WOOD_PLANKS, new WoodPlanksTile());
        TileLoader.addTile(TileType.ROCK, new RockTile());
        TileLoader.addTile(TileType.TORCH, new TorchTile());
        TileLoader.addTile(TileType.FURNACE, new FurnaceTile());
        TileLoader.addTile(TileType.CAVE_ENTRANCE, new CaveEntranceTile());
        TileLoader.addTile(TileType.CAVE_WALL_STONE, new CaveWallStoneTile());
        TileLoader.addTile(TileType.IRON_ORE_WALL, new IronOreWallTile());
        TileLoader.addTile(TileType.CHEST, new ChestTile());
        TileLoader.addTile(TileType.FARMLAND, new FarmLandTile());
        TileLoader.addTile(TileType.SAND, new SandTile());
        TileLoader.addTile(TileType.LEAFY_GRASS, new LeafyGrassTile());
        TileLoader.addTile(TileType.WATERED_FARMLAND, new WateredFarmLandTile());
    }
}
