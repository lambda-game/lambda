package com.interfiber.lambda.client.bootstrap;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.controllers.*;
import com.interfiber.lambda.client.misc.AutoGCService;
import com.interfiber.lambda.client.options.GameOptions;
import com.interfiber.lambda.client.options.OptionManager;
import com.interfiber.lambda.client.options.gameoptions.EnableDiscordRPCOption;
import com.interfiber.lambda.client.options.gameoptions.UseSystemCursorOption;
import com.interfiber.lambda.client.options.gameoptions.UseVsyncOption;
import com.interfiber.lambda.client.overlays.*;
import com.interfiber.lambda.client.ticks.TickManager;
import com.interfiber.lambda.client.ticks.TickableFarm;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.tiles.tilehandlers.*;
import org.tinylog.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GameBootstrap {
    public static void bootstrapGame() {

        if (Game.developerModeEnabled) {
            DeveloperModeBootstrap.bootstrapDeveloperMode();
        }
        // load auto garbage collect, this runs every 2 minutes to collect memory
        // garbage
        Logger.info("Loading auto GC");
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
        scheduledExecutorService.scheduleWithFixedDelay(AutoGCService::runGC, 2, 2,
                TimeUnit.MINUTES);

        // add the controllers
        Logger.info("Loading controllers");
        ControllerManager.addController("player_movement", new PlayerMovementController());
        ControllerManager.addController("player_place_tile", new PlayerPlaceTileController());
        ControllerManager.addController("player_remove_tile", new PlayerRemoveTileController());
        ControllerManager.addController("player_keybind", new PlayerKeybindController());
        ControllerManager.addController("player_craft", new PlayerCraftingController());

        // load the overlay manager
        Logger.info("Loading overlay manager");
        OverlayManager.initManager();

        // load overlays
        Logger.info("Loading overlays");
        OverlayManager.addOverlay(new PauseMenuOverlay());
        OverlayManager.addOverlay(new WeatherOverlay());
        OverlayManager.addOverlay(new StorageContainerOverlay());
        OverlayManager.addOverlay(new CraftingOverlay());
        OverlayManager.addOverlay(new FogOverlay());
        OverlayManager.addOverlay(new InventoryOverlay());
        OverlayManager.addOverlay(new FPSOverlay());
        OverlayManager.addOverlay(new ChatOverlay());
        // tool is enabled

        // add all the tiles into memory, textures not loaded yet
        Logger.info("Loading tiles");
        TileBootstrap.bootstrap();

        // load all crafting recipes into memory
        Logger.info("Loading crafting recipes");
        RecipeBootstrap.bootstrap();

        // load available game options into memory
        Logger.info("Loading available game config options");
        Game.options = new GameOptions();
        OptionManager.addOption("vsync", new UseVsyncOption());
        OptionManager.addOption("oscursor", new UseSystemCursorOption());
        OptionManager.addOption("discordrpc", new EnableDiscordRPCOption());

        // load tile handlers
        Logger.info("Loading tile handlers");
        TileHandlerManager.addHandler(TileType.WORKBENCH, new WorkbenchTileHandler());
        TileHandlerManager.addHandler(TileType.FURNACE, new FurnaceTileHandler());
        TileHandlerManager.addHandler(TileType.CAVE_ENTRANCE, new CaveEntranceTileHandler());
        TileHandlerManager.addHandler(TileType.CHEST, new StorageContainerTileHandler());

        // load tick manager
        Logger.info("Loading tick manager");
        Game.tickManager = new TickManager();
        Game.tickManager.addTickSubscriber(new TickableFarm());

    }
}
