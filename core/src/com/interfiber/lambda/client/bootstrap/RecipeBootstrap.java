package com.interfiber.lambda.client.bootstrap;

import com.interfiber.lambda.client.crafting.RecipeManager;
import com.interfiber.lambda.client.crafting.recipes.*;

public class RecipeBootstrap {
    public static void bootstrap() {
        RecipeManager.addRecipe(new WorkbenchRecipe());
        RecipeManager.addRecipe(new StickRecipe());
        RecipeManager.addRecipe(new StonePickaxeRecipe());
        RecipeManager.addRecipe(new WoodPickaxeRecipe());
        RecipeManager.addRecipe(new FurnaceRecipe());
        RecipeManager.addRecipe(new CharcoalRecipe());
        RecipeManager.addRecipe(new TorchRecipe());
        RecipeManager.addRecipe(new IronBarRecipe());
        RecipeManager.addRecipe(new IronPickaxeRecipe());
        RecipeManager.addRecipe(new ChestRecipe());
        RecipeManager.addRecipe(new WoodHoeRecipe());
        RecipeManager.addRecipe(new WateringCanRecipe());
        RecipeManager.addRecipe(new BreadRecipe());
        RecipeManager.addRecipe(new CarrotBreadRecipe());
    }
}
