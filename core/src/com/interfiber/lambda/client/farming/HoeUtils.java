package com.interfiber.lambda.client.farming;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.HarvestCropPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.HoeSoilPacket;

public class HoeUtils {
    public static void hoeSoil(WorldTile worldTile) {
        WorldTile userPlacedTile =
                Game.getCurrentWorld().getUserTileAtPosition(worldTile.x, worldTile.y);
        if (worldTile.type != TileType.GRASS || worldTile.userPlaced || userPlacedTile != null) {
            Game.alertSystem.createToast("You cannot hoe that!");
            return;
        }
        Game.getCurrentWorld().getWorldData().remove(worldTile);
        WorldTile farmTile = new WorldTile();
        farmTile.type = TileType.FARMLAND;
        farmTile.userPlaced = false;
        farmTile.x = worldTile.x;
        farmTile.y = worldTile.y;
        farmTile.collider = null;
        Game.getCurrentWorld().getWorldData().add(farmTile);


        if (Game.multiplayerEnabled) {
            ParsedPacket payload = new ParsedPacket();
            payload.initGuard(MultiplayerGameState.player.authUUID);

            payload.content.put("tileX", worldTile.x);
            payload.content.put("tileY", worldTile.y);

            new HoeSoilPacket(payload).writeData(MultiplayerGameState.serverConnection);
        }

    }

    public static boolean harvestIfPossible(WorldTile worldTile) {

        if (Game.multiplayerEnabled) {
            ParsedPacket packet = new ParsedPacket();
            packet.initGuard(MultiplayerGameState.player.authUUID);

            packet.content.put("cropX", worldTile.x);
            packet.content.put("cropY", worldTile.y);

            new HarvestCropPacket(packet).writeData(MultiplayerGameState.serverConnection);
        }

        if (CropManager.getSerializableCropAtPosition(worldTile.x, worldTile.y) == null) {
            return false;
        }

        SerializableCrop sCrop =
                CropManager.getSerializableCropAtPosition(worldTile.x, worldTile.y);
        if (sCrop.hasGrown) {
            CropManager.getCropData().remove(sCrop);
            Player.inventory.pickupItem(CropManager.cropTypeToCrop(sCrop.cropType).getProduce());

            // crops always stay wet during a thunderstorm
            if (Game.weather == null) {
                Game.getCurrentWorld().getWorldData().remove(worldTile);
                worldTile.type = TileType.FARMLAND;
                Game.getCurrentWorld().getWorldData().add(worldTile);
            }
            return true;
        } else {
            return false;
        }
    }
}
