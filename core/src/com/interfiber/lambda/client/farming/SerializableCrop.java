package com.interfiber.lambda.client.farming;

import java.io.Serializable;

public class SerializableCrop implements Serializable {
    public long startTime = 0;
    public CropType cropType;
    public boolean hasGrown = false;
    public boolean hasBeenWatered = false;
    public int x;
    public int y;
}
