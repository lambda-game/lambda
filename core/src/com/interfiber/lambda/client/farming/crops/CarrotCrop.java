package com.interfiber.lambda.client.farming.crops;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.farming.Crop;
import com.interfiber.lambda.client.farming.CropType;
import com.interfiber.lambda.client.items.CarrotItem;
import com.interfiber.lambda.client.items.Item;

public class CarrotCrop extends Crop {

    @Override
    public Texture getPlantedTexture() {
        return Assets.manager.get("carrot_seeds.png");
    }

    @Override
    public int getGrowTime() {
        return 60 * 2;
    }

    @Override
    public Item getProduce() {
        return new CarrotItem();
    }

    @Override
    public CropType getCropType() {
        return CropType.CARROT;
    }
}
