package com.interfiber.lambda.client.farming.crops;

import com.badlogic.gdx.graphics.Texture;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.farming.Crop;
import com.interfiber.lambda.client.farming.CropType;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.WheatItem;

public class WheatCrop extends Crop {

    @Override
    public Texture getPlantedTexture() {
        return Assets.manager.get("wheat_seeds.png");
    }

    @Override
    public int getGrowTime() {
//        return 60 * 3;
        return 1;
    }

    @Override
    public Item getProduce() {
        return new WheatItem();
    }

    @Override
    public CropType getCropType() {
        return CropType.WHEAT;
    }
}
