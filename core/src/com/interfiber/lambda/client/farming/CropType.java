package com.interfiber.lambda.client.farming;

public enum CropType {
    WHEAT,
    CARROT
}
