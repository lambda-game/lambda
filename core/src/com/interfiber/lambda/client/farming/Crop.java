package com.interfiber.lambda.client.farming;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.TimeUtils;
import com.interfiber.lambda.client.items.Item;
import org.tinylog.Logger;

public abstract class Crop {
    private long startTime = 0;

    public abstract Texture getPlantedTexture(); // get the planted texture, this texture is
    // overlayed ontop of the plated soil texture

    public abstract int getGrowTime(); // how many seconds does the crop take to grow?

    public abstract Item getProduce(); // get the result item once the crop has grown

    public abstract CropType getCropType(); // get the crop type

    public SerializableCrop getSerializableCrop(int x, int y) {
        SerializableCrop crop = new SerializableCrop();
        crop.cropType = this.getCropType();
        crop.startTime = this.startTime;
        crop.x = x;
        crop.y = y;
        return crop;
    }

    public void plant() {
        Logger.info("Crop planted");
    }

    public void water() {
        Logger.info("Crop watered");
        startTime = TimeUtils.millis();
    }
}
