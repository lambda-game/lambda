package com.interfiber.lambda.client.farming;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.farming.crops.CarrotCrop;
import com.interfiber.lambda.client.farming.crops.WheatCrop;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.PlantCropPacket;
import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.HashMap;

public class CropManager {
    private static ArrayList<SerializableCrop> plantedCrops = new ArrayList<>();
    private static HashMap<Vector2, CropHarvestableAlert> alertTweens = new HashMap<>();

    public static Crop getCropAtPosition(int x, int y) {
        Crop foundCrop = null;
        for (SerializableCrop crop : plantedCrops) {
            if (crop.x == x && crop.y == y) {
                foundCrop = cropTypeToCrop(crop.cropType);
                break;
            }
        }
        return foundCrop;
    }

    public static SerializableCrop getSerializableCropAtPosition(int x, int y) {
        SerializableCrop foundCrop = null;
        for (SerializableCrop crop : plantedCrops) {
            if (crop.x == x && crop.y == y) {
                foundCrop = crop;
                break;
            }
        }
        return foundCrop;
    }

    public static Crop cropTypeToCrop(CropType type) {
        switch (type) {
            case WHEAT:
                return new WheatCrop();
            case CARROT:
                return new CarrotCrop();
        }
        return null;
    }

    public static void plantCrop(CropType cropType, int x, int y, boolean userPlaced) {

        if (Game.multiplayerEnabled) {

            ParsedPacket plantPacket = new ParsedPacket();
            plantPacket.initGuard(MultiplayerGameState.player.authUUID);
            plantPacket.content.put("cropX", x);
            plantPacket.content.put("cropY", y);
            plantPacket.content.put("cropType", cropType);

            new PlantCropPacket(plantPacket).writeData(MultiplayerGameState.serverConnection);

            return;
        }

        boolean autoWater = false;

        if (Game.getCurrentWorld().getTileAtPosition(x, y) != null) {
            if (Game.getCurrentWorld().getTileAtPosition(x, y).type == TileType.WATERED_FARMLAND) {
                autoWater = true;
            } else if (Game.getCurrentWorld().getTileAtPosition(x, y).type != TileType.FARMLAND) {
                Game.alertSystem.createToast("You can only place crops on tilled soil!");
                return;
            }
        }
        if (getCropAtPosition(x, y) != null) {
            Logger.warn("Crop is already at that position");
            return;
        } else {
            Crop crop = cropTypeToCrop(cropType);
            crop.plant();
            SerializableCrop sCrop = crop.getSerializableCrop(x, y);
            if (autoWater) {
                sCrop.hasBeenWatered = true;
                crop.water();
            }
            plantedCrops.add(sCrop);
            if (userPlaced) {
                // remove item
                Player.inventory.removeItem(Player.selectedInventorySlot, 1);
            }
        }
    }

    public static void setCropAtPositionToBeWatered(int x, int y) {
        SerializableCrop sCrop = getSerializableCropAtPosition(x, y);
        Crop crop = getCropAtPosition(x, y);
        plantedCrops.remove(sCrop);
        crop.water();
        sCrop = crop.getSerializableCrop(x, y);
        sCrop.hasBeenWatered = true;
        plantedCrops.add(sCrop);
    }

    public static void renderCrops(SpriteBatch worldBatch) {
        for (int x = 0; x < plantedCrops.size(); x++) {
            SerializableCrop sCrop = plantedCrops.get(x);
            Crop crop = cropTypeToCrop(sCrop.cropType);
            boolean shouldRender = Game.getCurrentWorld().pointIsVisibleByCamera(sCrop.x, sCrop.y);
            if (shouldRender) {
                worldBatch.draw(crop.getPlantedTexture(), sCrop.x, sCrop.y);
                if (sCrop.hasGrown) {
                    Vector2 posVec = new Vector2(sCrop.x, sCrop.y);
                    int tweenY = 0;
                    if (alertTweens.get(posVec) == null) {
                        CropHarvestableAlert alert = new CropHarvestableAlert();
                        alert.x = 0;
                        alert.directionTween = "up";
                        alertTweens.put(posVec, alert);
                    } else {
                        int oldPOS = alertTweens.get(posVec).x;
                        tweenY = oldPOS;
                        String direction = alertTweens.get(posVec).directionTween;
                        if (tweenY > 26) {
                            direction = "down";
                        }
                        if (tweenY < -4 || direction == "up") {
                            direction = "up";
                        }

                        if (direction == "up") {
                            tweenY += 1;
                        }
                        if (direction == "down") {
                            tweenY -= 1;
                        }
                        alertTweens.remove(posVec);

                        CropHarvestableAlert alert = new CropHarvestableAlert();
                        alert.x = tweenY;
                        alert.directionTween = direction;
                        alertTweens.put(posVec, alert);
                    }
                    worldBatch.draw(Assets.manager.get("alert.png", Texture.class), sCrop.x,
                            (sCrop.y + Game.tileSize / 2) + tweenY);
                }
            }
        }
    }

    public static ArrayList<SerializableCrop> getCropData() {
        return plantedCrops;
    }

    public static void loadCropData(ArrayList<SerializableCrop> data) {
        plantedCrops = data;
    }

    public static void removeCropAtPosition(int x, int y) {
        for (SerializableCrop crop : plantedCrops) {
            if (crop.x == x && crop.y == y) {
                plantedCrops.remove(crop);
                break;
            }
        }
    }
}
