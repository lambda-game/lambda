package com.interfiber.lambda.client;

import club.minnced.discord.rpc.DiscordEventHandlers;
import club.minnced.discord.rpc.DiscordRPC;
import club.minnced.discord.rpc.DiscordRichPresence;
import org.tinylog.Logger;

public class Discord implements Runnable {
    @Override
    public void run() {
        Logger.info("Connecting to discord");
        DiscordRPC lib = DiscordRPC.INSTANCE;
        String applicationId = "999420862702833694";
        String steamId = "";
        DiscordEventHandlers handlers = new DiscordEventHandlers();
        handlers.ready = (user) -> Logger.info("DiscordRPC ready!!");
        lib.Discord_Initialize(applicationId, handlers, true, steamId);
        DiscordRichPresence presence = new DiscordRichPresence();
        presence.startTimestamp = System.currentTimeMillis() / 1000; // epoch second
        presence.details = "Currently in: Loading...";
        presence.largeImageKey = "logo";
        presence.largeImageText = "Lambda";
        lib.Discord_UpdatePresence(presence);
        while (!Thread.currentThread().isInterrupted()) {
            String worldName = Game.currentWorld.toString().toLowerCase();
            if (worldName == "unknown") {
                worldName = "menus";
            }
            presence.details = "Currently in: " + worldName;
            lib.Discord_RunCallbacks();
            lib.Discord_UpdatePresence(presence);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
