package com.interfiber.lambda.client.player;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemUtils;
import com.interfiber.lambda.client.items.SerializableItem;
import org.tinylog.Logger;

import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

public class PlayerInventory extends Inventory {

    private final ConcurrentHashMap<Integer, Item> items = new ConcurrentHashMap<>();

    @Override
    public void addItem(Item item) {
        int slot = items.size();
        if (items.get(slot) != null) {
            if (Objects.equals(items.get(slot).getItemDisplayName(), item.getItemDisplayName())) {
                Item existingItem = items.get(slot);
                existingItem.amount += item.amount;
                items.remove(slot);
                items.put(slot, existingItem);
            }
        } else {
            items.put(slot, item);
        }
    }

    @Override
    public void addItem(Item item, int slot) {
        if (items.get(slot) != null) {
            if (Objects.equals(items.get(slot).getItemDisplayName(), item.getItemDisplayName())) {
                Item existingItem = items.get(slot);
                existingItem.amount += item.amount;
                items.remove(slot);
                items.put(slot, existingItem);
            }
        } else {
            items.put(slot, item);
        }
    }

    @Override
    public void removeItem(int slot, int amount) {
        if (items.get(slot) != null) {
            int amountAfterRemove = items.get(slot).amount - amount;
            Item newItem = items.get(slot);
            if (amountAfterRemove <= 0) {
                newItem.removed(true);
                items.remove(slot);
                return;
            } else {
                newItem.removed(false);
            }
            newItem.amount -= amount;
            items.put(slot, newItem);
        } else {
            Logger.warn("Failed to remove item with slot ID: " + slot + ", no such item");
        }
    }

    @Override
    public ConcurrentHashMap<Integer, Item> getItems() {
        return items;
    }

    public ConcurrentHashMap<Integer, Item> getInventoryItems(int inventory) {
        ConcurrentHashMap<Integer, Item> inventoryItems = new ConcurrentHashMap<>();

        // find index to start and end the query at

        int startIndex = inventory * (Game.inventoryBottomBarSlots + 1);
        int endIndex = startIndex + Game.inventoryBottomBarSlots;

        // query the player inventory
        for (int i = startIndex; i < endIndex; i++) {
            if (items.containsKey(i)) {
                inventoryItems.put(i, items.get(i));
            }
        }

        return inventoryItems;
    }

    public ConcurrentHashMap<Integer, Item> getInventoryItems() {
        return this.getInventoryItems(Player.currentInventory);
    }

    @Override
    public ConcurrentHashMap<Integer, SerializableItem> getStorableItems() {
        ConcurrentHashMap<Integer, SerializableItem> store = new ConcurrentHashMap<>();
        for (Map.Entry<Integer, Item> set : items.entrySet()) {
            store.put(set.getKey(), set.getValue().getSerializableItem());
        }
        return store;
    }

    @Override
    public void setInventoryContent(ConcurrentHashMap<Integer, SerializableItem> content) {
        this.items.clear();

        for (Map.Entry<Integer, SerializableItem> set : content.entrySet()) {
            Item item = ItemUtils.getItemFromId(set.getValue().type);
            assert item != null;
            item.amount = set.getValue().amount;
            this.items.put(set.getKey(), item);
        }
    }

    public void selectedSlot(int slot) {
        if (this.items.get(slot) != null) {
            this.items.get(slot).interact(slot);
        }
    }

    public void pickupItem(Item pickup, int inventory) {
        if (pickup.amount <= 0) {
            pickup.amount = 1;
        }

        int startIndex = inventory * (Game.inventoryBottomBarSlots + 1);
        int endIndex = startIndex + Game.inventoryBottomBarSlots;
        Logger.info("startIndex = " + startIndex);
        Logger.info("inventory = " + inventory);

        if (items.size() == 0) {
            pickup.interact(startIndex);
            this.addItem(pickup, startIndex);
            return;
        }
        // loop over every slot in the inventory, if we find a slot with the same item as us, stack it.
        boolean addedItem = false;

        for (int x = startIndex; x < endIndex; x++) {
            if (items.containsKey(x)) {
                Item item = items.get(x);
                if (Objects.equals(item.getType(), pickup.getType())) {
//                    Logger.info(item.amount + pickup.amount);
                    if ((item.amount + pickup.amount) <= Game.maxStackSize) {
                        Logger.info("x: " + x);
                        this.addItem(pickup, x);
                        addedItem = true;
                        break;
                    }
                }
            }
        }

        if (!addedItem) {
            // loop over every slot in the inventory until we find an empty one to place the item

            for (int i = 0; i < Player.playerInventoryCount; i++) {
                boolean shouldBreak = false;

                for (int y = startIndex; y < endIndex; y++) {
                    if (!items.containsKey(y)) {
                        Logger.info(y);
                        this.addItem(pickup, y);
                        shouldBreak = true;
                        addedItem = true;
                        break;
                    }
                }
                if (shouldBreak) {
                    break;
                }
            }
        }

        // only display popup when in client mode
        if (Game.alertSystem != null) {
            if (addedItem) {
                Game.alertSystem.createToast("+" + pickup.amount + " " + pickup.getItemDisplayName());
            } else {
                Game.alertSystem.createToast("Inventory full!");
            }
        }
    }

    public void pickupItem(Item item) {
        this.pickupItem(item, Player.currentInventory);
    }

    public void loadItems(ConcurrentHashMap<Integer, SerializableItem> items) {
        for (Map.Entry<Integer, SerializableItem> set : items.entrySet()) {
            Item item = ItemUtils.getItemFromId(set.getValue().type);
            if (item == null) {
                Logger.debug("ItemType: " + set.getValue().type.toString());
                Logger.debug("Result: null");
                throw new IllegalStateException("Failed to convert enum item to Item class, this is a bug");
            }
            item.amount = set.getValue().amount;
            this.items.put(set.getKey(), item);
        }
    }

    public Item getCurrentlySelectedItem() {
        int selectedSlot = Player.selectedInventorySlot;
        return items.get(selectedSlot);
    }

}
