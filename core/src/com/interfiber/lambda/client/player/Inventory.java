package com.interfiber.lambda.client.player;

import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.SerializableItem;

import java.util.concurrent.ConcurrentHashMap;

public abstract class Inventory {
    public abstract void addItem(Item item);

    public abstract void addItem(Item item, int slot);

    public abstract void removeItem(int slot, int amount);

    public abstract ConcurrentHashMap<Integer, Item> getItems();

    public abstract ConcurrentHashMap<Integer, SerializableItem> getStorableItems();

    public abstract void setInventoryContent(ConcurrentHashMap<Integer, SerializableItem> content);
}