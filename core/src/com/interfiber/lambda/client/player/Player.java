package com.interfiber.lambda.client.player;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.tiles.TileType;

public class Player {
    public static int x = 0; // player X cord
    public static int y = 0; // player Y cord
    public static int caveEnterX = 0; // cave entrance X
    public static int caveEnterY = 0; // cave entrance Y
    public static int maxHealth = 20; // player max health
    public static int health = 20; // player health
    public static int healthPerHeart = 5; // health per heart
    public static int speed = 92; // player movement speed
    public static boolean isBreakingTile = false; // if we are breaking a tile
    public static boolean isSwimming = false; // if the player is swimming
    public static int swimmingSpeed = 80; // speed of the player while swimming/in water
    public static int height = 13; // height of the player
    public static int width = 22; // width of the player
    public static String facing = "down"; // left, right, up, down
    public static OrthographicCamera camera; // players camera
    public static int selectedInventorySlot = Game.inventoryBottomBarSlots - 1; // selected hot bar
    // slot
    public static int currentInventory = 0; // current inventory, cycle with TAB
    public static int playerInventoryCount = 1; // amount of inventory's the player has
    public static PlayerInventory inventory; // current player inventory
    public static TileType inventoryTile = TileType.AIR; // current tile type for the inventory, set
    // by placeable blocks
}
