package com.interfiber.lambda.client.overlays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.tiles.TileType;

public class FogOverlay extends Overlay {

    public static int tiles = 0;

    @Override
    public void create() {
    }

    @Override
    public void render(SpriteBatch renderBatch) {
        tiles = 0;
        renderBatch.begin();
        // render fog around the outside of the screen
        int screenX = Gdx.graphics.getWidth();
        int screenY = Gdx.graphics.getHeight();
        for (int y = 0; y < screenY; y++) {
            TileLoader.getTile(TileType.FOG).render(renderBatch, screenX - 60, y);
            TileLoader.getTile(TileType.FOG).render(renderBatch, screenX - Game.tileSize, y);
            TileLoader.getTile(TileType.FOG).render(renderBatch, Game.tileSize, y);
            TileLoader.getTile(TileType.FOG).render(renderBatch, 0, y);
            tiles += 3;
        }
        for (int x = 0; x < screenX; x++) {
            TileLoader.getTile(TileType.FOG).render(renderBatch, x, screenY - 60);
            TileLoader.getTile(TileType.FOG).render(renderBatch, x, screenY - Game.tileSize);
            TileLoader.getTile(TileType.FOG).render(renderBatch, x, Game.tileSize);
            TileLoader.getTile(TileType.FOG).render(renderBatch, x, 0);
            tiles += 4;
        }
        renderBatch.end();
    }

    @Override
    public void dispose() {

    }
}