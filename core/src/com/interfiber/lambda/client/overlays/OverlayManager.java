package com.interfiber.lambda.client.overlays;

import java.util.ArrayList;

public class OverlayManager {
    private static ArrayList<Overlay> overlays;

    public static void addOverlay(Overlay overlay) {
        overlays.add(overlay);
    }

    public static ArrayList<Overlay> getOverlays() {
        return overlays;
    }

    public static void initManager() {
        overlays = new ArrayList<>();
    }

    public static Overlay getOverlay(Overlay overlay) {
        Class<? extends Overlay> overlayClass = overlay.getClass();
        Overlay overlayResult = null;
        for (Overlay overlay1 : overlays) {
            if (overlayClass == overlay1.getClass()) {
                overlayResult = overlay1;
                break;
            }
        }
        return overlayResult;
    }
}
