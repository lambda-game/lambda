package com.interfiber.lambda.client.overlays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.controllers.ChatMessageController;
import com.interfiber.lambda.client.multiplayer.ClientChatMessage;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.SendChatMessagePacket;

public class ChatOverlay extends Overlay {

    public static FreeTypeFontGenerator.FreeTypeFontParameter fontGenerated;
    public static FreeTypeFontGenerator.FreeTypeFontParameter fontGenerated2;
    GlyphLayout titleLayout;
    GlyphLayout promptLayout;
    GlyphLayout contentTextLayout;
    GlyphLayout messageLayout;
    ShapeRenderer shapeRenderer;
    private BitmapFont font;
    private BitmapFont font2;

    @Override
    public void render(SpriteBatch overlaySpriteBatch) {

        Gdx.input.setInputProcessor(new ChatMessageController());
        titleLayout = new GlyphLayout(font, "Chat");
        promptLayout = new GlyphLayout(font, "> ");
        contentTextLayout = new GlyphLayout(font, Game.chatMessage + "_");

        overlaySpriteBatch.begin();
        int i = 2;
        for (ClientChatMessage message : Game.chatHistory) {
            if (!message.isSystem) {
                messageLayout = new GlyphLayout(font2, "<" + message.fromPlayer.username + "> " + message.content);
            } else {
                messageLayout = new GlyphLayout(font2, "[#" + message.color.toString() + "]" + message.content);
            }
            int height = (50 * i);

            font2.draw(overlaySpriteBatch, messageLayout, promptLayout.width + 100, Gdx.graphics.getHeight() - height);

            i++;
        }
        overlaySpriteBatch.end();

        if (!Game.chatOpen) {
            Gdx.input.setInputProcessor(null);
            return;
        } // only render chat when its opened


        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {

            boolean isCommand = Game.chatMessage.charAt(0) == '/';

            // send message
            ParsedPacket packet = new ParsedPacket();
            packet.initGuard(MultiplayerGameState.player.authUUID);

            packet.content.put("messageContent", Game.chatMessage);
            packet.content.put("isCommand", isCommand);
            packet.content.put("isPrivate", false);
            packet.content.put("toPlayer", "");

            new SendChatMessagePacket(packet).writeData(MultiplayerGameState.serverConnection);
            Game.chatMessage = "";
            Game.chatOpen = false;
        }


        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.BLACK);
        shapeRenderer.rect(0, 0, Gdx.graphics.getWidth(), 120);
        shapeRenderer.end();

        overlaySpriteBatch.begin();
        font.draw(overlaySpriteBatch, titleLayout, (Gdx.graphics.getWidth() - titleLayout.width) / 2, Gdx.graphics.getHeight() - 150);
        font.draw(overlaySpriteBatch, promptLayout, promptLayout.width + 100, 100);
        font.draw(overlaySpriteBatch, contentTextLayout, promptLayout.width + 150, 100);

        overlaySpriteBatch.end();
    }

    @Override
    public void create() {
        FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        fontGenerated = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontGenerated.size = 48;
        font = fontGenerator.generateFont(fontGenerated);

        FreeTypeFontGenerator fontGenerator2 = new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        fontGenerated2 = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontGenerated2.size = 26;
        font2 = fontGenerator2.generateFont(fontGenerated2);
        font2.getData().markupEnabled = true;

        shapeRenderer = new ShapeRenderer();
    }

    @Override
    public void dispose() {
        font.dispose();
        shapeRenderer.dispose();
    }
}
