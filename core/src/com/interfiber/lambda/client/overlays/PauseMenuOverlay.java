package com.interfiber.lambda.client.overlays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.screens.GameConfigScreen;
import com.interfiber.lambda.client.world.WorldSaver;
import org.tinylog.Logger;

import java.io.IOException;

public class PauseMenuOverlay extends Overlay {
    String title = "Game paused, press ESC to exit";
    GlyphLayout titleLayout;
    GlyphLayout saveLayout;
    GlyphLayout exitLayout;
    GlyphLayout optionsLayout;
    BitmapFont menuFont;
    private int selectedMenu = 0;
    private int tick = 0;
    private int maxSelectedMenu = 3;

    @Override
    public void render(SpriteBatch overlaySpriteBatch) {
        if (Game.pauseMenuOpen) {
            if (tick != 0) {
                tick += 1;
            }
            if (tick == 5) {
                try {
                    WorldSaver.runSave(Game.currentSaveFile);
                } catch (IOException e) {
                    Logger.error(e);
                    throw new RuntimeException("Failed to save world to disk");
                }
                title = "Game paused, press ESC to exit";
                tick = 0;
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
                if (selectedMenu + 1 >= maxSelectedMenu) {
                    selectedMenu = 0;
                } else {
                    selectedMenu += 1;
                }
            }

            if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
                if (selectedMenu - 1 < 0) {
                    selectedMenu = maxSelectedMenu - 1;
                } else {
                    selectedMenu -= 1;
                }
            }
            if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
                if (selectedMenu == 0 && tick == 0) {
                    title = "Saving world...";
                    tick = 1;
                } else if (selectedMenu == 2) {
                    Logger.info("Exiting game session");
                    System.exit(0);
                } else if (selectedMenu == 1) {
                    Logger.warn("Switching to game options scene");
                    Game.screenManager.switchScene(new GameConfigScreen());
                }
            }

            titleLayout = new GlyphLayout(menuFont, title);

            if (selectedMenu == 0) {
                saveLayout = new GlyphLayout(menuFont, ">   1. Save world");
            } else {
                saveLayout = new GlyphLayout(menuFont, "   1. Save world");
            }
            if (selectedMenu == 1) {
                optionsLayout = new GlyphLayout(menuFont, ">   2. Game options");
            } else {
                optionsLayout = new GlyphLayout(menuFont, "   2. Game options");
            }
            if (selectedMenu == 2) {
                exitLayout = new GlyphLayout(menuFont, ">   3. Exit game without save");
            } else {
                exitLayout = new GlyphLayout(menuFont, "   3. Exit game without save");
            }
            overlaySpriteBatch.begin();
            menuFont.draw(overlaySpriteBatch, titleLayout, (Gdx.graphics.getWidth() - titleLayout.width) / 2, Gdx.graphics.getHeight() - 100);
            menuFont.draw(overlaySpriteBatch, saveLayout, (Gdx.graphics.getWidth() - saveLayout.width) / 2, Gdx.graphics.getHeight() - 200);
            menuFont.draw(overlaySpriteBatch, optionsLayout, (Gdx.graphics.getWidth() - optionsLayout.width) / 2, Gdx.graphics.getHeight() - 300);
            menuFont.draw(overlaySpriteBatch, exitLayout, (Gdx.graphics.getWidth() - exitLayout.width) / 2, Gdx.graphics.getHeight() - 400);
            overlaySpriteBatch.end();
        }
    }

    @Override
    public void create() {
        FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter menuFontGenerated = new FreeTypeFontGenerator.FreeTypeFontParameter();
        menuFontGenerated.size = 60;
        menuFont = fontGenerator.generateFont(menuFontGenerated);
    }

    @Override
    public void dispose() {

    }
}
