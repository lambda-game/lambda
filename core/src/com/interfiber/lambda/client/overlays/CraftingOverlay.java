package com.interfiber.lambda.client.overlays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.audio.AudioManager;
import com.interfiber.lambda.client.controllers.ControllerEventType;
import com.interfiber.lambda.client.controllers.ControllerManager;
import com.interfiber.lambda.client.crafting.CraftingMenuMode;
import com.interfiber.lambda.client.crafting.CraftingUtils;
import com.interfiber.lambda.client.crafting.Recipe;
import org.tinylog.Logger;

import java.util.List;

public class CraftingOverlay extends Overlay {

    public List<Recipe> recipes;
    public int items;
    BitmapFont font;
    FreeTypeFontGenerator.FreeTypeFontParameter fontGenerated;
    int currentlySelectedItem = 1;
    boolean isCrafting = false;
    boolean canCraftItem = false;
    int tick = 0;
    private Stage stage;
    private Dialog dialog;
    private LabelStyle labelStyle;
    private Texture bgImageTexture;
    private Table table;
    private ScrollPane tableScrollPane;

    @Override
    public void render(SpriteBatch overlaySpriteBatch) {
        // keybinds
        // UP : Go up in list
        // DOWN : Go down in list
        // ENTER : Select recipe
        // SPACE : Craft recipe


        Gdx.input.setInputProcessor(stage);
        if (!Game.craftingMenuOpen) {
            return; // if we don't have the crafting menu open don't render, or run any of the
            // overlay code
        }
        CraftingMenuMode craftingMode = Game.craftingMenuMode;

        // region keybinds
        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            if (currentlySelectedItem - 1 <= 0) {
                currentlySelectedItem = items;
                this.tableScrollPane.setScrollY(this.tableScrollPane.getScrollHeight());
            } else {
                currentlySelectedItem -= 1;
                this.tableScrollPane
                        .setScrollY(this.tableScrollPane.getScrollY() - currentlySelectedItem * 4);
            }
            AudioManager.getSfx("click3").play();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            if (currentlySelectedItem + 1 > items) {
                currentlySelectedItem = 1;
                this.tableScrollPane.setScrollY(-this.tableScrollPane.getScrollHeight());
            } else {
                currentlySelectedItem += 1;
                this.tableScrollPane.setScrollY(this.tableScrollPane.getScrollY() + currentlySelectedItem * 4);

            }
            AudioManager.getSfx("click3").play();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            Logger.info("Selected crafting recipe is: " + currentlySelectedItem);
            Logger.debug("Crafting recipe name: "
                    + recipes.get(currentlySelectedItem - 1).getRecipeName());
            isCrafting = true;
            canCraftItem = CraftingUtils.canCraftItem(recipes.get(currentlySelectedItem - 1));
            AudioManager.getSfx("click3").play();
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            ControllerManager.getController("player_craft").sendEvent(
                    ControllerEventType.PLAYER_CRAFT,
                    recipes.get(currentlySelectedItem - 1).getRecipeName());
            AudioManager.getSfx("click3").play();
        }
        // endregion

        // region render crafting recipes
        table.clear();
        int i = 1;
        for (Recipe recipe : recipes) {
            int amount = recipe.getOutputItem().amount;
            if (amount == 0) {
                amount = 1;
            }


            if (i == currentlySelectedItem) {
                String msgString = "> Craft: " + recipe.getRecipeName().toUpperCase() + " " + amount
                        + "x" + " <";
                String titleString =
                        ">> Press SPACE to craft: " + recipe.getRecipeName().toUpperCase() + " <<";
                table.add(new Label(msgString, labelStyle));

                // render title bar & stuff
                tick += 1;
                dialog.getTitleLabel().setWidth((titleString.length() * 30));

                // region blinking title bar text
                if (tick <= 50) {
                    dialog.getTitleLabel().setColor(Color.GRAY);
                    dialog.getTitleLabel().setText(titleString);
                } else {
                    dialog.getTitleLabel().setColor(Color.YELLOW);
                    dialog.getTitleLabel().setText(titleString);
                }
                if (tick >= 70) {
                    tick = 0;
                }
                // endregion

            } else {
                table.add(new Label(
                        "Craft: " + recipe.getRecipeName().toUpperCase() + " " + amount + "x",
                        labelStyle));
            }
            table.row();
            i++;
        }
        // endregion

        // region render scene
        dialog.setPosition(500, 500);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
        // endregion
    }

    @Override
    public void create() {

        // region create crafting menu font
        FreeTypeFontGenerator fontGenerator =
                new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        fontGenerated = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontGenerated.size = 40;
        fontGenerated.genMipMaps = true;
        font = fontGenerator.generateFont(fontGenerated);
        fontGenerator.dispose();
        // endregion

        stage = new Stage(); // create the scene stage
        Gdx.input.setInputProcessor(stage); // set the input processor


        Skin skin = new Skin(); // create a skin
        WindowStyle style = new WindowStyle(); // create window style
        bgImageTexture = new Texture(Gdx.files.internal("fog.png")); // use for the background
        // image, might change later
        Image bgImage = new Image(bgImageTexture); // create an image for the background
        style.background = bgImage.getDrawable(); // get the drawable for the background
        style.titleFontColor = Color.WHITE; // set the title font color

        // styles
        labelStyle = new LabelStyle(); // style the labels
        TextButtonStyle buttonStyle = new TextButtonStyle(); // style the text buttons
        buttonStyle.font = font;
        buttonStyle.fontColor = Color.RED;
        labelStyle.font = font;
        style.titleFont = font;

        // add skin classes
        skin.add("dialog", style);
        skin.add("default", labelStyle);
        skin.add("default", buttonStyle);

        // dialog
        dialog = new Dialog("Crafting", skin, "dialog") {
            public void result(Object obj) {
                System.out.print(obj);
            }
        };
        table = new Table();
        dialog.pad(40);

        table.setDebug(false);
        dialog.text("Select recipe:");
        tableScrollPane = new ScrollPane(table);
        dialog.getContentTable().add(tableScrollPane).width(600).height(400);
        dialog.show(stage);
    }

    @Override
    public void dispose() {
        // dispose
        stage.dispose();
        font.dispose();
        bgImageTexture.dispose();
    }
}
