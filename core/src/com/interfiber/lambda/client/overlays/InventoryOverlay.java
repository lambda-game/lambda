package com.interfiber.lambda.client.overlays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.tiles.TileType;

import java.util.concurrent.ConcurrentHashMap;

public class InventoryOverlay extends Overlay {
    public static FreeTypeFontGenerator.FreeTypeFontParameter fontGenerated;
    private BitmapFont inventoryFont;
    private Texture selectedTexture;
    private Texture unselectedTexture;

    @Override
    public void render(SpriteBatch overlaySpriteBatch) {
        Player.inventory.selectedSlot(Player.selectedInventorySlot);
        overlaySpriteBatch.begin();
        int screenY = Gdx.graphics.getHeight();

        ConcurrentHashMap<Integer, Item> inventoryItems = Player.inventory.getInventoryItems();
        int startIndex = Player.currentInventory * (Game.inventoryBottomBarSlots + 1);
        int endIndex = startIndex + Game.inventoryBottomBarSlots;

        int x = 0;
        for (int i = startIndex; i < endIndex; i++) {
            if (i == Player.selectedInventorySlot) {
                overlaySpriteBatch.draw(selectedTexture, (x * Game.tileSize) + 55,
                        screenY - Game.tileSize - 20);
            } else {
                overlaySpriteBatch.draw(unselectedTexture, (x * Game.tileSize) + 55,
                        screenY - Game.tileSize - 20);
            }
            if (inventoryItems.containsKey(i)) {
                overlaySpriteBatch.draw(inventoryItems.get(i).getTextureName(),
                        (x * Game.tileSize) + 55, screenY - Game.tileSize - 20);
                this.inventoryFont.draw(overlaySpriteBatch,
                        Integer.toString(inventoryItems.get(i).amount), (x * Game.tileSize) + 55,
                        screenY - Game.tileSize - 20);
            }
            x++;
        }

        int heartsToRender = Player.health / Player.healthPerHeart;
        for (int i = 0; i < heartsToRender; i++) {
            overlaySpriteBatch.draw(Assets.manager.get("heart.png", Texture.class), x * Game.tileSize + 60 + (i * 23), screenY - Game.tileSize - 20, 32, 32);
        }


        overlaySpriteBatch.end();
    }

    @Override
    public void create() {
        new BitmapFont();
        FreeTypeFontGenerator fontGenerator =
                new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        fontGenerated = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontGenerated.size = 18;
        inventoryFont = fontGenerator.generateFont(fontGenerated);
        selectedTexture = TileLoader.getTile(TileType.INVENTORYSELECTED).getTextureName();
        unselectedTexture = TileLoader.getTile(TileType.INVENTORYUNSELECTED).getTextureName();
    }

    @Override
    public void dispose() {
        inventoryFont.dispose();
        selectedTexture.dispose();
        unselectedTexture.dispose();
    }
}
