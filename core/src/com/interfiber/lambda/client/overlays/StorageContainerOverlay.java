package com.interfiber.lambda.client.overlays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.ui.Window.WindowStyle;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.items.ItemUtils;
import com.interfiber.lambda.client.items.SerializableItem;
import com.interfiber.lambda.client.misc.StorageContainerUtils;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.world.StorageContainerWorldTile;
import com.interfiber.lambda.client.world.World;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.AddItemToChestPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.RemoveItemFromChestPacket;
import org.tinylog.Logger;

import java.util.HashMap;
import java.util.Map;

public class StorageContainerOverlay extends Overlay {

    public StorageContainerWorldTile storageWorldTile;
    private Stage stage;
    private Dialog dialog;
    private Table storageContainerTable;
    private ScrollPane storageScrollPane;
    private Table inventoryTable;
    private ScrollPane inventoryScrollPane;
    private Label.LabelStyle labelStyle;
    private Texture bgImageTexture;
    private BitmapFont font;
    private FreeTypeFontGenerator.FreeTypeFontParameter fontGenerated;
    private int selectedChestIndex = 0;
    private int selectedInventoryIndex = 0;
    private int maxInventoryIndex = 0;
    private int maxChestIndex = 0;
    private int selectedPanel = 0;

    @Override
    public void render(SpriteBatch overlaySpriteBatch) {
        Gdx.input.setInputProcessor(stage);
        if (!Game.chestOpen) {
            if (storageWorldTile != null) {
                storageWorldTile = null;
            }
            return;
        }
        if (storageWorldTile == null) {
            World currentWorld = Game.getCurrentWorld();
            if (Game.chestX == -1 || Game.chestY == -1) {
                Logger.error("Chest open, but no coords set!");
                return;
            }
            this.storageWorldTile = (StorageContainerWorldTile) currentWorld
                    .getUserTileAtPosition(Game.chestX, Game.chestY);
        }

        // clear everything
        storageContainerTable.clear();
        inventoryTable.clear();

        maxChestIndex = storageWorldTile.contents.size();

        int x = 0;
        HashMap<Integer, SerializableItem> chestContent = new HashMap<>();
        HashMap<Integer, Integer> chestRealIndex = new HashMap<>();
        for (Map.Entry<Integer, SerializableItem> set : storageWorldTile.contents.entrySet()) {
            String itemName = ItemUtils.getItemFromId(set.getValue().type).getItemDisplayName();
            if (x == selectedChestIndex) {
                storageContainerTable.add(
                        new Label("> " + itemName + " " + set.getValue().amount + "x", labelStyle));
            } else {
                storageContainerTable
                        .add(new Label(itemName + " " + set.getValue().amount + "x", labelStyle));
            }
            chestContent.put(x, set.getValue());
            chestRealIndex.put(x, set.getKey());
            storageContainerTable.row();
            x++;
        }

        maxInventoryIndex = Player.inventory.getItems().size();
        x = 0;
        // hash map ordered by index
        HashMap<Integer, Item> inventoryContent = new HashMap<>();
        HashMap<Integer, Integer> inventoryRealIndex = new HashMap<>();
        for (Map.Entry<Integer, Item> set : Player.inventory.getItems().entrySet()) {
            if (x == selectedInventoryIndex) {
                inventoryTable.add(new Label("> " + set.getValue().getItemDisplayName() + " "
                        + set.getValue().amount + "x", labelStyle));
            } else {
                inventoryTable.add(new Label(
                        set.getValue().getItemDisplayName() + " " + set.getValue().amount + "x",
                        labelStyle));
            }
            inventoryTable.row();
            inventoryContent.put(x, set.getValue());
            inventoryRealIndex.put(x, set.getKey());
            x++;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            if (selectedPanel == 0) {
                if (selectedChestIndex + 1 >= maxChestIndex) {
                    selectedChestIndex = 0;
                    this.storageScrollPane.setScrollY(-this.storageScrollPane.getScrollHeight());
                } else {
                    selectedChestIndex += 1;
                    this.storageScrollPane.setScrollY(
                            this.storageScrollPane.getScrollY() + selectedChestIndex * 4);
                }
            } else if (selectedPanel == 1) {
                if (selectedInventoryIndex + 1 >= maxInventoryIndex) {
                    selectedInventoryIndex = 0;
                    this.inventoryScrollPane
                            .setScrollY(-this.inventoryScrollPane.getScrollHeight());
                } else {
                    this.inventoryScrollPane.setScrollY(
                            this.inventoryScrollPane.getScrollY() + selectedInventoryIndex * 4);
                    selectedInventoryIndex += 1;
                }
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            if (selectedPanel == 0) {
                if (selectedChestIndex - 1 < 0) {
                    selectedChestIndex = maxChestIndex - 1;
                    this.storageScrollPane.setScrollY(this.storageScrollPane.getScrollHeight());
                } else {
                    selectedChestIndex -= 1;
                    this.storageScrollPane.setScrollY(
                            this.storageScrollPane.getScrollY() - selectedChestIndex * 4);
                }
            } else if (selectedPanel == 1) {
                if (selectedInventoryIndex - 1 < 0) {
                    selectedInventoryIndex = maxInventoryIndex - 1;
                    this.inventoryScrollPane.setScrollY(this.inventoryScrollPane.getScrollHeight());
                } else {
                    selectedInventoryIndex -= 1;
                    this.inventoryScrollPane.setScrollY(
                            this.inventoryScrollPane.getScrollY() - selectedInventoryIndex * 4);
                }
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.LEFT)) {
            selectedPanel = 0;
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.RIGHT)) {
            selectedPanel = 1;
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            if (selectedPanel == 0) {
                if (chestContent.get(selectedChestIndex) == null) {
                    Game.alertSystem.createToast("Selected item does not exist!");
                    return;
                }
                SerializableItem serializableItem = chestContent.get(selectedChestIndex);
                Item item = ItemUtils
                        .getItemFromId(serializableItem.type);
                int amount = serializableItem.amount;
                String alertContent = "Moved " + amount + "x " + item.getItemDisplayName()
                        + "s into your inventory";
                // add item to inventory
                item.amount = amount;
                Player.inventory.pickupItem(item);
                // remove item from chest
                storageWorldTile.contents.remove(chestRealIndex.get(selectedChestIndex));

                if (Game.multiplayerEnabled) {
                    ParsedPacket content = new ParsedPacket();
                    content.initGuard(MultiplayerGameState.player.authUUID);
                    content.content.put("chestX", Game.chestX);
                    content.content.put("chestY", Game.chestY);
                    content.content.put("index", chestRealIndex.get(selectedChestIndex));

                    new RemoveItemFromChestPacket(content).writeData(MultiplayerGameState.serverConnection);
                }
                Game.alertSystem.createToast(alertContent);
            } else if (selectedPanel == 1) {
                if (inventoryContent.get(selectedInventoryIndex) == null) {
                    Game.alertSystem.createToast("Selected item does not exist!");
                    return;
                }
                // remove item from inventory
                Item item = inventoryContent.get(selectedInventoryIndex);
                int amount = item.amount;
                String alertContent = "Moved " + amount + "x " + item.getItemDisplayName() + "s into the chest";
                StorageContainerUtils.addItemToChest(Game.chestX, Game.chestY,
                        storageWorldTile.contents.size() + 1,
                        item);
                Player.inventory.removeItem(inventoryRealIndex.get(selectedInventoryIndex), amount);

                if (Game.multiplayerEnabled) {
                    ParsedPacket content = new ParsedPacket();
                    content.initGuard(MultiplayerGameState.player.authUUID);
                    content.content.put("chestX", Game.chestX);
                    content.content.put("chestY", Game.chestY);
                    content.content.put("index", inventoryRealIndex.get(selectedInventoryIndex));

                    new AddItemToChestPacket(content).writeData(MultiplayerGameState.serverConnection);
                }
                Game.alertSystem.createToast(alertContent);
            }
        }

        dialog.getTitleLabel().setColor(Color.GOLD);
        if (selectedPanel == 0) {
            dialog.getTitleLabel().setText("Chest - Chest contents");
        } else if (selectedPanel == 1) {
            dialog.getTitleLabel().setColor(Color.BLUE);
            dialog.getTitleLabel().setText("Chest - Inventory contents");
        }

        // draw the stage
        dialog.setPosition(500, 500);
        stage.act(Gdx.graphics.getDeltaTime());
        stage.draw();
    }

    @Override
    public void create() {

        // init stuff
        stage = new Stage();
        storageContainerTable = new Table();
        inventoryTable = new Table();

        // load fonts

        FreeTypeFontGenerator fontGenerator =
                new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        fontGenerated = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontGenerated.size = 40;
        fontGenerated.genMipMaps = true;
        font = fontGenerator.generateFont(fontGenerated);
        fontGenerator.dispose();

        // style dialog
        WindowStyle style = new WindowStyle();
        bgImageTexture = new Texture(Gdx.files.internal("fog.png"));
        Image bgImage = new Image(bgImageTexture);
        style.background = bgImage.getDrawable();
        style.titleFontColor = Color.WHITE;

        labelStyle = new Label.LabelStyle();
        TextButton.TextButtonStyle buttonStyle = new TextButton.TextButtonStyle();
        buttonStyle.font = font;
        buttonStyle.fontColor = Color.RED;
        labelStyle.font = font;
        style.titleFont = font;

        Skin skin = new Skin();

        skin.add("dialog", style);
        skin.add("default", labelStyle);
        skin.add("default", buttonStyle);

        dialog = new Dialog("Chest", skin, "dialog");
        dialog.pad(40);
        this.inventoryScrollPane = new ScrollPane(inventoryTable);
        this.storageScrollPane = new ScrollPane(storageContainerTable);
        dialog.getContentTable().add(new Label(
                "Use LEFT, and RIGHT to switch pane. Press enter to move item", labelStyle));
        dialog.getContentTable().row();
        dialog.getContentTable().add(this.storageScrollPane).width(600).height(600);
        dialog.getContentTable().add(this.inventoryScrollPane).width(600).height(600);
        dialog.show(stage);
    }

    @Override
    public void dispose() {
        stage.dispose();
        font.dispose();
        bgImageTexture.dispose();
    }
}
