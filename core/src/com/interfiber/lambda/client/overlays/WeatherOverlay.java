package com.interfiber.lambda.client.overlays;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.weather.WeatherUtils;

public class WeatherOverlay extends Overlay {

    @Override
    public void render(SpriteBatch overlaySpriteBatch) {
        if (Game.weather == null) {
            return;
        }
        WeatherUtils.renderWeather(overlaySpriteBatch);
    }

    @Override
    public void create() {
    }

    @Override
    public void dispose() {
        if (Game.weather != null) {
            WeatherUtils.disposeWeather();
        }
    }
}
