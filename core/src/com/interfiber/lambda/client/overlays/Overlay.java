package com.interfiber.lambda.client.overlays;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Overlay {

    public abstract void render(SpriteBatch overlaySpriteBatch);

    public abstract void create();

    public abstract void dispose();
}
