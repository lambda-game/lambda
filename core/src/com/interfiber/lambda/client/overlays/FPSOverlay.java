package com.interfiber.lambda.client.overlays;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.world.World;

// NOTE: This is basically just a debug overlay, not just fps
public class FPSOverlay extends Overlay {
    public static FreeTypeFontGenerator.FreeTypeFontParameter fpsFontGenerated;
    private BitmapFont fpsFont;
    private int renderCalls;

    @Override
    public void create() {
        fpsFont = new BitmapFont();
        FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        fpsFontGenerated = new FreeTypeFontGenerator.FreeTypeFontParameter();
        fpsFontGenerated.size = 18;
        fpsFont = fontGenerator.generateFont(fpsFontGenerated);
        renderCalls = 0;
    }

    @Override
    public void render(SpriteBatch overlaySpriteBatch) {
        if (!Game.debugOverlayEnabled) return;
        renderCalls += 1;
        overlaySpriteBatch.begin();
        fpsFont.draw(overlaySpriteBatch, "FPS: " + Gdx.graphics.getFramesPerSecond(), 1, Gdx.graphics.getHeight());
        fpsFont.draw(overlaySpriteBatch, "Last Accessed Tile: " + TileLoader.getLastAccessedTile(), 1, Gdx.graphics.getHeight() - fpsFont.getLineHeight());
        fpsFont.draw(overlaySpriteBatch, "X,Y: " + Player.x + ", " + Player.y, 1, Gdx.graphics.getHeight() - (fpsFont.getLineHeight() * 2));
        fpsFont.draw(overlaySpriteBatch, "Facing: " + Player.facing, 1, Gdx.graphics.getHeight() - (fpsFont.getLineHeight() * 3));
        fpsFont.draw(overlaySpriteBatch, "Render Calls: " + renderCalls, 1, Gdx.graphics.getHeight() - (fpsFont.getLineHeight() * 4));
        fpsFont.draw(overlaySpriteBatch, "Tiles rendered: " + World.tiles, 1, Gdx.graphics.getHeight() - (fpsFont.getLineHeight() * 5));
        fpsFont.draw(overlaySpriteBatch, "Used memory: " + Runtime.getRuntime().freeMemory() / 1000000 + "/" + Runtime.getRuntime().totalMemory() / 1000000 + " MB", 1, Gdx.graphics.getHeight() - (fpsFont.getLineHeight() * 6));
        fpsFont.draw(overlaySpriteBatch, "World Seed: " + Game.worldSeed, 1, Gdx.graphics.getHeight() - (fpsFont.getLineHeight() * 7));
        overlaySpriteBatch.end();
    }

    @Override
    public void dispose() {
        fpsFont.dispose();
    }
}
