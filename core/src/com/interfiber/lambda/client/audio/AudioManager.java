package com.interfiber.lambda.client.audio;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import java.util.HashMap;

public class AudioManager {
    private static final HashMap<String, Sound> sfxList = new HashMap<>();
    private static final HashMap<String, Music> musicList = new HashMap<>();

    public static void addSfx(String id, Sound sound) {
        sfxList.put(id, sound);
    }

    public static void addMusic(String id, Music music) {
        musicList.put(id, music);
    }

    public static Sound getSfx(String id) {
        return sfxList.get(id);
    }

    public static Music getMusic(String id) {
        return musicList.get(id);
    }
}
