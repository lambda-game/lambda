package com.interfiber.lambda.client.entitys.controllers;

public abstract class EntityController {
    // executed before entities are rendered, but AI code here
    public abstract void controllerCallback();

    // executed when colliders are updated
    public abstract void colliderUpdate(String colliderId);
}
