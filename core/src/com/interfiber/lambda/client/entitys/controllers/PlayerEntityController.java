package com.interfiber.lambda.client.entitys.controllers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector2;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.controllers.ControllerEventType;
import com.interfiber.lambda.client.controllers.ControllerManager;
import com.interfiber.lambda.client.controllers.PlayerRemoveTileController;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.world.collision.WorldMap;

public class PlayerEntityController extends EntityController {


    @Override
    public void controllerCallback() {
        // player movement
        ControllerManager.getController("player_movement")
                .sendEvent(ControllerEventType.PLAYER_MOVE, null);
        // region player place tile
        if (Gdx.input.isButtonJustPressed(Input.Buttons.RIGHT)) {
            ControllerManager.getController("player_place_tile")
                    .sendEvent(ControllerEventType.PLAYER_PLACE_TILE, null);
        }
        // endregion

        // region player remove tile
        if (Gdx.input.isButtonPressed(Input.Buttons.LEFT)) {
            Player.isBreakingTile = true;
            ControllerManager.getController("player_remove_tile")
                    .sendEvent(ControllerEventType.PLAYER_DESTROY_TILE, null);
        } else {
            // reset if we are not breaking the tile
            Player.isBreakingTile = false;
            Game.tileBreakProgress = 0;
            PlayerRemoveTileController controller = (PlayerRemoveTileController) ControllerManager
                    .getController("player_remove_tile_controller");
            controller.startTime = null;
            controller.oldTileX = -1;
            controller.oldTileY = -1;
        }
        // endregion

        ControllerManager.getController("player_keybind")
                .sendEvent(ControllerEventType.PLAYER_KEYBINDINGS, null);

    }

    @Override
    public void colliderUpdate(String colliderId) {
        // move the collider to the new player position every frame
        WorldMap.moveColliderPosition(colliderId, new Vector2(Player.x, Player.y));
    }
}
