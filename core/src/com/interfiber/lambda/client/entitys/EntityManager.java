package com.interfiber.lambda.client.entitys;

import org.tinylog.Logger;

import java.util.HashMap;
import java.util.Map;

public class EntityManager {
    private static final HashMap<String, Entity> entityList = new HashMap<>();
    private static boolean hasRanEntityRenderPrep = false;

    public static void addEntity(String entityId, Entity entity) {
        entityList.put(entityId, entity);
    }

    public static void runRenderPrep() {
        if (hasRanEntityRenderPrep) {
            Logger.warn("Not rerunning entity render prep, already ran!");
            return;
        }

        hasRanEntityRenderPrep = true;
        for (Map.Entry<String, Entity> set : entityList.entrySet()) {
            set.getValue().prepWithRender();
        }
    }

    public static Entity getEntity(String entityId) {
        Entity entity = entityList.get(entityId);
        if (entity == null) {
            throw new RuntimeException("Failed to get entity: " + entityId);
        } else {
            return entity;
        }
    }

    public static HashMap<String, Entity> getEntityList() {
        return entityList;
    }

    public static void executeControllers() {
        for (Map.Entry<String, Entity> set : entityList.entrySet()) {
            if (set.getValue().getEntityController() != null) {
                set.getValue().getEntityController().controllerCallback();
                set.getValue().getEntityController().colliderUpdate(set.getValue().getColliderId());
            }
        }
    }
}
