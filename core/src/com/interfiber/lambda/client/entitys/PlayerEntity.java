package com.interfiber.lambda.client.entitys;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.World;
import com.interfiber.lambda.client.world.WorldTile;
import org.tinylog.Logger;

import java.util.Objects;

public class PlayerEntity extends Entity {

    public int frame = 0;
    public int tick = 0;
    Texture playerWalkLeftWater;
    Texture playerWalkRightWater;
    Texture playerWalkDownWater;
    Texture playerWalkUpWater;
    Texture playerWalkUp;
    Texture playerWalkDown;
    Texture playerWalkRight;
    Texture playerWalkLeft;

    @Override
    public EntitySettings getEntitySettings() {
        EntitySettings settings = new EntitySettings();
        settings.isPlayer = true;
        return settings;
    }

    @Override
    public void prepWithRender() {
        playerWalkLeftWater = Assets.manager.get("player_walk_left_water.png", Texture.class);
        playerWalkRightWater = Assets.manager.get("player_walk_right_water.png", Texture.class);
        playerWalkDownWater = Assets.manager.get("player_walk_down_water.png", Texture.class);
        playerWalkUpWater = Assets.manager.get("player_walk_up_water.png", Texture.class);
        playerWalkUp = Assets.manager.get("player_walk_up.png", Texture.class);
        playerWalkDown = Assets.manager.get("player_walk_down.png", Texture.class);
        playerWalkLeft = Assets.manager.get("player_walk_left.png", Texture.class);
        playerWalkRight = Assets.manager.get("player_walk_right.png", Texture.class);
    }

    @Override
    public void render(SpriteBatch entityRenderBatch, int x, int y) {
        // just render the player at the pos from the player info class.
        // don't render where we are said we should
        // render the player to the screen
        // TODO Animations for the player

        // check if player is standing on water, if so change the player movement speed
        // and also change the texture of the player
        World world = Game.getCurrentWorld();

        int tileNumX = (int) Math.floor(Player.x / Game.tileSize);
        int tileNumY = (int) Math.floor(Player.y / Game.tileSize);

        WorldTile playerTile = world.getTileAtPosition(tileNumX * Game.tileSize, tileNumY * Game.tileSize);

        boolean inWater = false;
        if (playerTile != null) {
            if (Objects.equals(playerTile.type, TileType.WATER)) {
                inWater = true;
                Player.isSwimming = true;
            } else {
                Player.isSwimming = false;
            }
        } else {
            Logger.warn("Player is not in tile!");
        }


        if (Objects.equals(Player.facing, "left") && inWater) {
            entityRenderBatch.draw(playerWalkLeftWater, Player.x, Player.y);
        } else if (Objects.equals(Player.facing, "right") && inWater) {
            entityRenderBatch.draw(playerWalkRightWater, Player.x, Player.y);
        } else if (Objects.equals(Player.facing, "down") && inWater) {
            entityRenderBatch.draw(playerWalkDownWater, Player.x, Player.y);
        } else if (Objects.equals(Player.facing, "up") && inWater) {
            entityRenderBatch.draw(playerWalkUpWater, Player.x, Player.y);
        } else if (Objects.equals(Player.facing, "up")) {
            entityRenderBatch.draw(playerWalkUp, Player.x, Player.y);
        } else if (Objects.equals(Player.facing, "down")) {
            entityRenderBatch.draw(playerWalkDown, Player.x, Player.y);
        } else if (Objects.equals(Player.facing, "left")) {
            entityRenderBatch.draw(playerWalkLeft, Player.x, Player.y);
        } else if (Objects.equals(Player.facing, "right")) {
            entityRenderBatch.draw(playerWalkRight, Player.x, Player.y);
        } else {
            throw new IllegalStateException("Invalid Player.facing value");
        }
    }
}
