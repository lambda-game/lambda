package com.interfiber.lambda.client.entitys;

public class EntitySettings {
    // Name
    public String name = "lamba.engine.unknown.entity";
    // Base config
    public boolean isPlayer = false;
    public boolean isMonster = false;
    public boolean dropsLoot = false;
    public boolean hasAi = false;
}
