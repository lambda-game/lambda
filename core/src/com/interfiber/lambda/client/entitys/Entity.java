package com.interfiber.lambda.client.entitys;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.entitys.controllers.EntityController;
import com.interfiber.lambda.client.world.collision.WorldMap;

import java.util.UUID;

public abstract class Entity {
    private static EntityController entityController;
    private static String entityColliderId = "none";

    private static String generateUUID() {
        return UUID.randomUUID().toString();
    }

    public abstract EntitySettings getEntitySettings();

    public abstract void render(SpriteBatch entityRenderBatch, int x, int y);

    public void attachController(EntityController controller) {
        entityController = controller;
    }

    public void attachCollider(int height, int width) {
        // generate a collider ID
        String colliderID = generateUUID();
        WorldMap.addCollider(colliderID, 0, 0, height, width);
        entityColliderId = colliderID;
    }


    public String getColliderId() {
        return entityColliderId;
    }

    // runs in the create method of the scene
    // allows the entity to pre-render textures
    public abstract void prepWithRender();

    public EntityController getEntityController() {
        return entityController;
    }
}
