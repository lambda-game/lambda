package com.interfiber.lambda.client.world;

import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.client.weather.WeatherType;
import org.tinylog.Logger;

import javax.swing.*;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public class SaveFileEditor {
    public static void openSave(String path) throws IOException, ClassNotFoundException {
        Logger.info("Opening save file: " + path);
        ObjectInputStream in = new ObjectInputStream(Files.newInputStream(Paths.get(path)));
        GameSave worldData = (GameSave) in.readObject();
        Logger.info("Opened save file");

        try {
            UIManager.setLookAndFeel(new MetalLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            throw new RuntimeException(e);
        }
        JFrame window = new JFrame("Lambda save file viewer");
        BorderLayout borderLayout = new BorderLayout();
        window.setLayout(borderLayout);
        // save file table
        String[] columnNames = {"Key", "Value", "Type"};
        String[][] data = {
                {"time", worldData.time.toString(), "Time"},
                {"saveFileNum", Integer.toString(worldData.saveFileNum), "int"},
                {"worldSeed", Integer.toString(worldData.worldSeed), "int"},
                {"playerItems", worldData.playerItems.toString(), "array<item>"},
                {"playerX", Integer.toString(worldData.playerX), "int"},
                {"playerY", Integer.toString(worldData.playerY), "int"},
                {"weather", worldData.weather.toString(), "Weather"}
        };
        JTable table = new JTable(data, columnNames);
        window.add(table, BorderLayout.CENTER);

        JButton save = new JButton("Save info");
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for (int count = 0; count < table.getRowCount(); count++) {
                    String key = data[count][0];
                    String value = table.getValueAt(count, 1).toString();
                    if (Objects.equals(key, "time")) {
                        worldData.time = Time.valueOf(value);
                    } else if (key.equals("saveFileNum")) {
                        worldData.saveFileNum = Integer.parseInt(value);
                    } else if (key.equals("worldSeed")) {
                        worldData.worldSeed = Integer.parseInt(value);
                    } else if (key.equals("playerItems")) {
                        Logger.warn("Cannot modify playerItems!");
                    } else if (key.equals("playerX")) {
                        worldData.playerX = Integer.parseInt(value);
                    } else if (key.equals("playerY")) {
                        worldData.playerY = Integer.parseInt(value);
                    } else if (key.equals("weather")) {
                        worldData.weather = WeatherType.valueOf(value);
                    } else {
                        throw new IllegalStateException("Invalid key!");
                    }
                }
                Logger.info("Saving info to disk...");
                ObjectOutputStream out = null;
                try {
                    out = new ObjectOutputStream(Files.newOutputStream(Paths.get(path)));
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
                try {
                    out.writeObject(worldData);
                    out.close();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        });
        window.add(save, BorderLayout.SOUTH);

        JLabel title = new JLabel("Lambda save file editor");
        title.setFont(new Font("Arial", Font.PLAIN, 40));
        window.add(title, BorderLayout.NORTH);

        window.pack();
        window.setSize(500, 500);
        window.setVisible(true);
    }
}
