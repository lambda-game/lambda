package com.interfiber.lambda.client.world;

import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.collision.CollisionRectangle;

import java.io.Serializable;

public class WorldTile implements Serializable {
    public int x = 0;
    public int y = 0;
    public TileType type = TileType.AIR;
    public CollisionRectangle collider;
    public boolean userPlaced = false;
}
