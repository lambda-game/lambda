package com.interfiber.lambda.client.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.farming.CropManager;
import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.weather.WeatherType;
import com.interfiber.lambda.client.weather.WeatherUtils;
import org.tinylog.Logger;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class WorldSaver {
    public static void runSave(int id) throws IOException {

        String worldSaveDir = getStorageDir();
        String gameDataFilePath = String.valueOf(Paths.get(worldSaveDir, "gameSave" + id + ".dat"));
        Logger.info("Running game save");

        // save file
        GameSave save = new GameSave();
        save.worldSeed = Game.worldSeed;
        save.playerX = Player.x;
        save.playerY = Player.y;
        save.saveFileNum = Game.currentSaveFile;
        save.time = Game.time;

        save.overworldData = Game.overworld.getWorldData();
        save.underworldData = Game.underworld.getWorldData();
        save.cropData = CropManager.getCropData();

        save.overworldX = (int) Game.overworldSpawnPoint.x;
        save.overworldY = (int) Game.overworldSpawnPoint.y;

        save.underworldX = (int) Game.underworldSpawnPoint.x;
        save.underworldY = (int) Game.underworldSpawnPoint.y;


        save.caveEntranceX = Player.caveEnterX;
        save.caveEntranceY = Player.caveEnterY;

        save.currentWorld = Game.currentWorld;
        save.playerItems = Player.inventory.getStorableItems();

        if (Game.weather != null) {
            save.weather = Game.weather.getWeatherType();
        } else {
            save.weather = WeatherType.CLEAR;
        }


        ObjectOutputStream out =
                new ObjectOutputStream(Files.newOutputStream(Paths.get(gameDataFilePath)));
        out.writeObject(save);
        out.close();

        Logger.info("Completed game save!");
    }

    public static void loadWorld(int id) {
        try {
            Logger.info("Loading game save");
            String worldSaveDir = getStorageDir();

            // load in the game save from disk

            ObjectInputStream in = new ObjectInputStream(
                    Files.newInputStream(Paths.get(worldSaveDir, "gameSave" + id + ".dat")));
            GameSave save = (GameSave) in.readObject();

            Overworld overworld = new Overworld();
            overworld.loadWorldData(save.overworldData);

            Underworld underworld = new Underworld();
            underworld.loadWorldData(save.underworldData);

            Game.overworld = overworld;
            Logger.info("Loaded overworld data");

            Game.underworld = underworld;
            Logger.info("Loaded underworld data");

            Player.x = save.playerX;
            Player.y = save.playerY;
            Player.inventory.loadItems(save.playerItems);

            Game.overworldSpawnPoint = new Vector2(save.overworldX, save.overworldY);
            Game.underworldSpawnPoint = new Vector2(save.underworldX, save.underworldY);

            Game.worldSeed = save.worldSeed;
            Game.currentSaveFile = save.saveFileNum;
            Game.time = save.time;
            Game.currentWorld = save.currentWorld;

            Player.caveEnterX = save.caveEntranceX;
            Player.caveEnterY = save.caveEntranceY;

            CropManager.loadCropData(save.cropData);

            if (save.weather != WeatherType.CLEAR) {
                WeatherUtils.setWeather(WeatherUtils.weatherTypeToWeather(save.weather));
            }

            Logger.info("Loaded game data");
        } catch (Exception error) {
            Logger.error(error);
            throw new RuntimeException("Failed to load world");
        }
    }

    public static void createWorld(int id) throws IOException {
        Logger.info("Generating overworld, seeding randomness off of current unix epoch");
        int seed = (int) System.currentTimeMillis();
        Logger.info("World seed: " + seed);

        Overworld overworld = new Overworld();
        overworld.generate(seed);

        Underworld underworld = new Underworld();
        underworld.generate(seed);

        Logger.info("Generated world, setting global");
        Game.overworld = overworld;
        Game.underworld = underworld;
        Game.currentWorld = WorldType.OVERWORLD;
        Game.time = Time.MORNING;
        Game.currentSaveFile = id;

        Logger.info("Setting world spawn point");
        Player.x = (int) Game.overworldSpawnPoint.x;
        Player.y = (int) Game.overworldSpawnPoint.y;

        runSave(Game.currentSaveFile);
    }

    public static String getStorageDir() {
        String homeDir = Gdx.files.getExternalStoragePath();
        return String.valueOf(Paths.get(homeDir, ".lambda"));
    }

    public static void createWorldStorage() {
        String storageDir = getStorageDir();
        if (new File(storageDir).exists()) {
            Logger.info("World data already exists at: " + storageDir);
        } else {
            Logger.info("Creating world storage");
            boolean created = new File(storageDir).mkdirs();
            if (!created) {
                Logger.error("Failed to create world storage!");
                JOptionPane.showMessageDialog(null,
                        "Failed to create world storage directory at: " + storageDir,
                        "Lambda error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public static List<Integer> getWorlds() {
        File dir = new File(getStorageDir());
        File[] directoryListing = dir.listFiles();
        List<Integer> worlds = new ArrayList<>();
        if (directoryListing != null) {
            for (File child : directoryListing) {
                if (child.getName().contains("gameSave0")) {
                    int id = Integer
                            .parseInt(child.getName().replace("gameSave", "").replace(".dat", ""));
                    Logger.info("Found world with ID: " + id);
                    worlds.add(id);
                }
            }
        } else {
            Logger.error("Storage dir is null");
            JOptionPane.showMessageDialog(null,
                    "Failed to walk world directory, got result NULL. This might be a bug, please report it to the developers!!",
                    "Lambda error", JOptionPane.ERROR_MESSAGE);
            System.exit(1);
        }
        return worlds;
    }

    public static int getNewWorldId() {
        List<Integer> worlds = getWorlds();
        if (worlds.size() == 0) {
            return 0;
        }
        return worlds.get(worlds.size() - 1) + 1;
    }
}
