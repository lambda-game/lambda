package com.interfiber.lambda.client.world;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.Tile;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.world.collision.CollisionRectangle;
import com.interfiber.lambda.client.world.collision.WorldMap;

import java.util.ArrayList;
import java.util.List;

public abstract class World {
    public static int tiles = 0;
    private final List<String> renderColliders = new ArrayList<>();

    public abstract void generate(int seed);

    public abstract List<WorldTile> getWorldData();

    public boolean pointIsVisibleByCamera(float x, float y) {
        OrthographicCamera camera = Player.camera;
        return camera.frustum.pointInFrustum(new Vector3(x + Game.tileSize, y + Game.tileSize, 0));
    }

    public abstract boolean tileExistsAtPosition(int x, int y);

    public abstract WorldTile getTileAtPosition(int x, int y);

    public List<String> getRenderedColliders() {
        return this.renderColliders;
    }

    public void render(SpriteBatch renderBatch) {
        renderColliders.clear();

        WorldMap.rebuildColliders(); // clear the global list of colliders, so we don't leak memory.
        // Found this bug at 2:10, Thursday 2020(idk what date, its
        // summer)

        tiles = 0;
        List<WorldTile> worldData = this.getWorldData();
        for (int x = 0; worldData.size() > x; x++) {
            WorldTile worldTile = worldData.get(x);
            Vector2 pos = new Vector2(worldTile.x, worldTile.y);
            boolean shouldRender = pointIsVisibleByCamera(pos.x, pos.y);
            if (!worldTile.userPlaced) {
                if (shouldRender) {
                    TileLoader.getTile(worldTile.type).render(renderBatch, (int) pos.x,
                            (int) pos.y);

                    CollisionRectangle rec = new CollisionRectangle((int) pos.x, (int) pos.y,
                            Game.tileSize, Game.tileSize);

                    WorldTile tileTemp = new WorldTile();
                    tileTemp.type = worldTile.type;
                    tileTemp.collider = rec;
                    tileTemp.x = (int) pos.x;
                    tileTemp.y = (int) pos.y;
                    tiles += 1;
                }
            } else {
                if (shouldRender) {
                    Tile renderTile = TileLoader.getTile(worldTile.type);
                    renderTile.attachCollider(worldTile.x, worldTile.y, Game.tileSize,
                            Game.tileSize);
                    renderColliders.add(renderTile.getColliderId());
                    renderTile.render(renderBatch, worldTile.x, worldTile.y);
                    tiles += 1;
                }
            }
        }
    }


    public abstract void placeTile(int x, int y, String tileType, boolean userPlaced);

    public abstract WorldTile getUserTileAtPosition(int x, int y);

    public abstract void removeUserPlacedTile(int x, int y);

    public abstract void loadWorldData(List<WorldTile> worldData);

    public abstract void deleteUserPlacedTile(int x, int y);

    public abstract void deleteTile(int x, int y);
}
