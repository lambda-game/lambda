package com.interfiber.lambda.client.world.collision;

import com.badlogic.gdx.math.Vector2;

public class CollisionRectangle {
    public int x, y;
    public int width, height;

    public CollisionRectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    public boolean collidesWithObject(CollisionRectangle rect) {
        return x < rect.x + rect.width && y < rect.y + rect.height && x + width > rect.x && y + height > rect.y;
    }

    public void setPosition(Vector2 vector2) {
        x = (int) vector2.x;
        y = (int) vector2.y;
    }
}
