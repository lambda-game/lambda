package com.interfiber.lambda.client.world.collision;

import com.badlogic.gdx.math.Vector2;
import com.interfiber.lambda.client.entitys.EntityManager;
import com.interfiber.lambda.client.player.Player;

import java.util.HashMap;

// Collision map of the world
public class WorldMap {
    private static final HashMap<String, CollisionRectangle> colliders = new HashMap<>();

    public static void addCollider(String colliderId, int x, int y, int height, int width) {
        CollisionRectangle coll = new CollisionRectangle(x, y, width, height);
        colliders.put(colliderId, coll);
    }

    public static HashMap<String, CollisionRectangle> getColliders() {
        return colliders;
    }

    public static void removeCollider(String colliderId) {
        colliders.remove(colliderId);
    }

    public static CollisionRectangle getCollider(String colliderId) {
        return colliders.get(colliderId);
    }

    public static void moveColliderPosition(String colliderId, Vector2 newPosition) {
        CollisionRectangle info = colliders.get(colliderId);
        info.setPosition(newPosition);
        colliders.remove(colliderId);
        colliders.put(colliderId, info);
    }

    public static void rebuildColliders() {
        colliders.clear();

        // add entity for player
        EntityManager.getEntity("player").attachCollider(Player.height, Player.width);
    }

    public static boolean overlaps(int x, int y, int height, int width, CollisionRectangle collider) {
        CollisionRectangle newRec = new CollisionRectangle(x, y, width, height);
        return newRec.collidesWithObject(collider);
    }

    public static boolean colliderExists(String colliderID) {
        return colliders.get(colliderID) != null;
    }

    public static CollisionRectangle getColliderInfo(String colliderId) {
        return colliders.get(colliderId);
    }
}