package com.interfiber.lambda.client.world;

import com.badlogic.gdx.math.Vector2;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.lighting.LightSource;
import com.interfiber.lambda.client.lighting.LightUtils;
import com.interfiber.lambda.client.overlays.FogOverlay;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.tiles.TileType;
import org.tinylog.Logger;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Underworld extends World {

    private static List<WorldTile> worldData = new ArrayList<>();

    @Override
    public void generate(int seed) {
        Random terrainSeed = new Random(seed); // ore generation, rocks, loot, and more!

        // use simplex noise for cave generation

        int HEIGHT = Game.mapHeight;
        int WIDTH = Game.mapWidth;
        boolean foundWorldSpawn = false;
        double FEATURE_SIZE = 24;
        boolean placedExitNode = false;

        OpenSimplexNoise noise = new OpenSimplexNoise(Game.worldSeed);
        Vector2 worldSpawn = new Vector2();

        for (int y = 0; y < HEIGHT; y++) {
            for (int x = 0; x < WIDTH; x++) {
                WorldTile tile = new WorldTile();
                WorldTile wallTile = new WorldTile();
                boolean placedWallTile = false;

                double value = noise.eval(x / FEATURE_SIZE, y / FEATURE_SIZE, 0.0);
                if (value < 0.2) {
                    // rock
                    tile.userPlaced = false;
                    tile.x = x * Game.tileSize;
                    tile.y = y * Game.tileSize;
                    tile.type = TileType.STONE;
                    if (terrainSeed.nextInt(100) == 20 && !placedExitNode && x != 0) {
                        foundWorldSpawn = true;
                        placedExitNode = true;

                        worldSpawn.x = x * Game.tileSize + Game.tileSize;
                        worldSpawn.y = y * Game.tileSize;

                        tile.userPlaced = true;
                        tile.type = TileType.CAVE_ENTRANCE;
                    }
                } else {
                    // then a wall
                    placedWallTile = true;

                    // iron ore spawn
                    wallTile.userPlaced = true;
                    wallTile.x = x * Game.tileSize;
                    wallTile.y = y * Game.tileSize;
                    if (terrainSeed.nextInt(50) == 20) {
                        wallTile.type = TileType.IRON_ORE_WALL;
                    } else {
                        wallTile.type = TileType.CAVE_WALL_STONE;
                    }

                    // place stone
                    tile.userPlaced = false;
                    tile.x = x * Game.tileSize;
                    tile.y = y * Game.tileSize;
                    tile.type = TileType.STONE;
                }
                worldData.add(tile);
                if (placedWallTile) {
                    worldData.add(wallTile);
                }
            }
        }

        Logger.info("World size is: " + worldData.size());
        if (!foundWorldSpawn) {
            Logger.warn("Failed to find world spawn, using 15 15 as default");
            JOptionPane.showMessageDialog(null,
                    "Failed to create world spawn point, using 0 0 as default.", "Lambda error",
                    JOptionPane.ERROR_MESSAGE);
        }

        if (!placedExitNode) {
            Logger.warn("Failed to place cave exit node, placing one at 0,35");
            WorldTile exitTile = new WorldTile();
            exitTile.userPlaced = true;
            exitTile.type = TileType.CAVE_ENTRANCE;
            exitTile.x = 0;
            exitTile.y = 35;
        }
        Game.underworldSpawnPoint = worldSpawn;
        // add fog tiles
        tiles += FogOverlay.tiles;
    }

    @Override
    public List<WorldTile> getWorldData() {
        return worldData;
    }

    @Override
    public boolean tileExistsAtPosition(int x, int y) {
        List<WorldTile> worldTiles = this.getWorldData();
        boolean foundTile = false;
        for (WorldTile tile : worldTiles) {
            if (tile.x == x && tile.y == y) {
                foundTile = true;
                break;
            }
        }
        return foundTile;
    }

    @Override
    public WorldTile getTileAtPosition(int x, int y) {
        List<WorldTile> worldTiles = this.getWorldData();
        WorldTile foundTile = null;
        for (WorldTile tile : worldTiles) {
            if (tile.x == x && tile.y == y) {
                foundTile = tile;
                break;
            }
        }
        return foundTile;
    }

    @Override
    public void placeTile(int x, int y, String tileType, boolean userPlaced) {
        List<WorldTile> worldTiles = this.getWorldData();

        WorldTile tile = new WorldTile();
        tile.userPlaced = userPlaced;
        tile.type = TileType.valueOf(tileType.toUpperCase());
        tile.x = x;
        tile.y = y;
        worldTiles.add(tile);
    }

    @Override
    public WorldTile getUserTileAtPosition(int x, int y) {
        List<WorldTile> worldTiles = this.getWorldData();
        WorldTile foundTile = null;
        for (WorldTile tile : worldTiles) {
            if (tile.x == x && tile.y == y && tile.userPlaced) {
                foundTile = tile;
                break;
            }
        }
        return foundTile;
    }

    @Override
    public void removeUserPlacedTile(int x, int y) {
        List<WorldTile> worldTiles = this.getWorldData();
        for (WorldTile tile : worldTiles) {
            if (tile.x == x && tile.y == y && tile.userPlaced) {
                boolean canRemove = TileLoader.getTile(tile.type).destroyTile();
                if (!canRemove) {
                    break;
                }
                worldTiles.remove(tile);
                if (TileLoader.getTile(tile.type) instanceof LightSource) {
                    LightUtils.rebuildLighting();
                }
                break;
            }
        }
    }

    @Override
    public void loadWorldData(List<WorldTile> data) {
        Logger.info("Loading underworld world data...");
        worldData = data;
        Logger.info("Loaded underworld world data");
    }

    @Override
    public void deleteUserPlacedTile(int x, int y) {
        List<WorldTile> worldTiles = this.getWorldData();
        worldTiles.removeIf(tile -> tile.x == x && tile.y == y && tile.userPlaced);
    }

    @Override
    public void deleteTile(int x, int y) {
        for (WorldTile tile : worldData) {
            if (tile.x == x && tile.y == y) {
                worldData.remove(tile);
                break;
            }
        }
    }
}
