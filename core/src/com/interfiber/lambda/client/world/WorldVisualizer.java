package com.interfiber.lambda.client.world;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.tiles.TileType;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class WorldVisualizer {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        System.out.println("Crappy world visualizer starting...");
        String worldDataPath = "";
        // read the world into worldtiles
        ObjectInputStream in = new ObjectInputStream(Files.newInputStream(Paths.get(worldDataPath)));
        List<WorldTile> worldData = (List<WorldTile>) in.readObject();
        BufferedImage img = new BufferedImage(Game.mapWidth, Game.mapHeight, BufferedImage.TYPE_INT_RGB);
        int[] pixels = new int[Game.mapWidth * Game.mapHeight];
        for (int y = 0; y < Game.mapHeight; y++) {
            for (int x = 0; x < Game.mapWidth; x++) {
                int i = x + y * Game.mapHeight;
                WorldTile v = worldData.get(i);
                if (v.type == TileType.GRASS) {
                    pixels[i] = 0x208020;
                } else {
                    pixels[i] = 0x000099;
                }
            }
        }
        img.setRGB(0, 0, Game.mapWidth, Game.mapWidth, pixels, 0, Game.mapWidth);
        JOptionPane.showMessageDialog(null, null, "Lambda terrain test: Result viewer", JOptionPane.INFORMATION_MESSAGE, new ImageIcon(img.getScaledInstance(250 * 3, 250 * 3, Image.SCALE_AREA_AVERAGING)));
    }
}
