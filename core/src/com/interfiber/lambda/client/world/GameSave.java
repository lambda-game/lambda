package com.interfiber.lambda.client.world;

import com.interfiber.lambda.client.farming.SerializableCrop;
import com.interfiber.lambda.client.items.SerializableItem;
import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.client.weather.WeatherType;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class GameSave implements Serializable {
    public int worldSeed = 0; // world seed
    public int playerX = 0; // player X position
    public int playerY = 0; // player Y position
    public int caveEntranceX = 0; // cave entrance x
    public int caveEntranceY = 0; // cave entrance y
    public int saveFileNum = 0; // save file number
    public Time time; // current time
    public ConcurrentHashMap<Integer, SerializableItem> playerItems = new ConcurrentHashMap<>(); // inventory
    // data
    public List<WorldTile> overworldData; // overworld terrain data
    public List<WorldTile> underworldData; // underworld terrain data
    public ArrayList<SerializableCrop> cropData = new ArrayList<>(); // crop data

    public WorldType currentWorld; // current world

    public int underworldX = 0; // spawn underworld position(X)
    public int underworldY = 0; // spawn underworld position(Y)

    public int overworldY = 0; // spawn overworld position(Y)
    public int overworldX = 0; // spawn overworld position(X)
    public WeatherType weather = WeatherType.CLEAR; // current weather
}
