package com.interfiber.lambda.client.world;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.lighting.LightSource;
import com.interfiber.lambda.client.overlays.FogOverlay;
import com.interfiber.lambda.client.tiles.Tile;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.tiles.TileType;
import org.tinylog.Logger;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Overworld extends World {
    private static List<WorldTile> worldData = new ArrayList<>();

    @Override
    public void generate(int seed) {
        Random rand = new Random(seed);
        Random terrainSeed = new Random(seed); // randomness for things such as rocks, trees, and
        // ores.
        Noise noise = new Noise(rand, Game.mapRoughness, Game.mapWidth, Game.mapHeight);

        Game.worldSeed = seed;
        noise.initialise(); // generate the noise for the world

        // the raw world data
        Array<Float> world = noise.getNoise();
        boolean foundWorldSpawn = false;
        Vector2 worldSpawn = new Vector2(15, 15);


        for (int y = 0; y < Game.mapWidth; y++) {
            for (int x = 0; x < Game.mapWidth; x++) {
                int i = x + y * Game.mapWidth;
                float v = world.get(i);
                WorldTile worldTile = new WorldTile();
                WorldTile treeTile = new WorldTile();
                WorldTile stoneTile = new WorldTile();
                WorldTile grassTile = new WorldTile();
                boolean placedTree = false;
                boolean placedStone = false;
                boolean placedGrass = false;
                worldTile.userPlaced = false;
                // Logger.info(String.valueOf(v));
                if (v < 0.2) {
                    // place water
                    worldTile.type = TileType.WATER;
                    worldTile.x = x * Game.tileSize;
                    worldTile.y = y * Game.tileSize;
                } else if (v < 0.26) {
                    // beaches
                    worldTile.type = TileType.SAND;
                    worldTile.x = x * Game.tileSize;
                    worldTile.y = y * Game.tileSize;
                } else {
                    // place grass
                    worldTile.type = TileType.GRASS;
                    worldTile.x = x * Game.tileSize;
                    worldTile.y = y * Game.tileSize;

                    // randomly place trees
                    int shouldPlaceTree = terrainSeed.nextInt(20);
                    if (shouldPlaceTree == 10) {
                        treeTile.userPlaced = true;
                        treeTile.type = TileType.TREE;
                        treeTile.x = x * Game.tileSize;
                        treeTile.y = y * Game.tileSize;
                        placedTree = true;
                    }

                    // randomly place stone
                    int shouldPlaceStone = terrainSeed.nextInt(20);
                    if (shouldPlaceStone == 5 && !placedTree) {
                        stoneTile.userPlaced = true;
                        stoneTile.type = TileType.ROCK;
                        stoneTile.x = x * Game.tileSize;
                        stoneTile.y = y * Game.tileSize;
                        placedStone = true;

                        if (terrainSeed.nextInt(20) == 3) {
                            worldTile.userPlaced = true;
                            worldTile.type = TileType.CAVE_ENTRANCE;
                            worldTile.x = x * Game.tileSize;
                            worldTile.y = y * Game.tileSize;
                            placedStone = false;
                        }
                    }

                    // randomly place grass
                    int shouldPlaceGrass = terrainSeed.nextInt(20);
                    if (shouldPlaceGrass == 4 && !placedTree && !placedStone && worldTile.type != TileType.CAVE_ENTRANCE) {
                        placedGrass = true;
                        grassTile.type = TileType.LEAFY_GRASS;
                        grassTile.x = x * Game.tileSize;
                        grassTile.y = y * Game.tileSize;
                        grassTile.userPlaced = true;
                    }

                    // find world spawn
                    if (!foundWorldSpawn && x != 0 && y != 0 && !placedStone && !placedTree) {
                        worldSpawn = new Vector2(worldTile.x, worldTile.y);
                        foundWorldSpawn = true;
                    }
                }
                worldData.add(worldTile);
                if (placedTree) {
                    worldData.add(treeTile);
                }
                if (placedStone) {
                    worldData.add(stoneTile);
                }
                if (placedGrass) {
                    worldData.add(grassTile);
                }
            }
        }
        if (!foundWorldSpawn) {
            Logger.warn("Failed to find world spawn, using 15 15 as default");
            JOptionPane.showMessageDialog(null,
                    "Failed to create world spawn point, using 0 0 as default.", "Lambda error",
                    JOptionPane.ERROR_MESSAGE);
        }
        Game.overworldSpawnPoint = worldSpawn;
        // add fog tiles
        tiles += FogOverlay.tiles;
    }

    @Override
    public List<WorldTile> getWorldData() {
        return worldData;
    }

    @Override
    public boolean tileExistsAtPosition(int x, int y) {
        List<WorldTile> worldTiles = this.getWorldData();
        boolean foundTile = false;
        for (WorldTile tile : worldTiles) {
            if (tile.x == x && tile.y == y) {
                foundTile = true;
                break;
            }
        }
        return foundTile;
    }

    @Override
    public WorldTile getTileAtPosition(int x, int y) {
        List<WorldTile> worldTiles = this.getWorldData();
        WorldTile foundTile = null;
        for (WorldTile tile : worldTiles) {
            if (tile.x == x && tile.y == y) {
                foundTile = tile;
                break;
            }
        }
        return foundTile;
    }

    @Override
    public void placeTile(int x, int y, String tileType, boolean userPlaced) {
        List<WorldTile> worldTiles = this.getWorldData();

        WorldTile tile = new WorldTile();
        tile.userPlaced = userPlaced;
        tile.type = TileType.valueOf(tileType.toUpperCase());
        tile.x = x;
        tile.y = y;
        worldTiles.add(tile);
    }

    @Override
    public WorldTile getUserTileAtPosition(int x, int y) {
        List<WorldTile> worldTiles = this.getWorldData();
        WorldTile foundTile = null;
        for (WorldTile tile : worldTiles) {
            if (tile.x == x && tile.y == y && tile.userPlaced) {
                foundTile = tile;
                break;
            }
        }
        return foundTile;
    }

    @Override
    public void removeUserPlacedTile(int x, int y) {
        List<WorldTile> worldTiles = this.getWorldData();
        for (WorldTile tile : worldTiles) {
            if (tile.x == x && tile.y == y && tile.userPlaced) {
                boolean canRemove = TileLoader.getTile(tile.type).destroyTile();
                if (!canRemove)
                    break;
                worldTiles.remove(tile);
                if (TileLoader.getTile(tile.type) instanceof LightSource) {
                    new Thread(() -> Gdx.app.postRunnable(() -> {
                        for (Map.Entry<TileType, Tile> set : TileLoader.getTiles().entrySet()) {
                            if (set.getValue() instanceof LightSource) {
                                ((LightSource) set.getValue()).clearLights();
                            }
                        }
                        Game.lightManager.lightWorld();
                    })).start();
                }
                break;
            }
        }
    }

    @Override
    public void loadWorldData(List<WorldTile> dat) {
        Logger.info("Loading world data...");
        worldData = dat;
        Logger.info("Loaded world data");
    }

    @Override
    public void deleteUserPlacedTile(int x, int y) {
        List<WorldTile> worldTiles = this.getWorldData();
        worldTiles.removeIf(tile -> tile.x == x && tile.y == y && tile.userPlaced);
    }

    @Override
    public void deleteTile(int x, int y) {
        for (WorldTile tile : worldData) {
            if (tile.x == x && tile.y == y) {
                worldData.remove(tile);
                break;
            }
        }
    }
}
