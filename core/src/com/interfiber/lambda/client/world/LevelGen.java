package com.interfiber.lambda.client.world;

import com.interfiber.lambda.client.Game;
import org.tinylog.Logger;

import java.io.IOException;
import java.util.Random;

public class LevelGen {
    public static void main(String[] args) throws IOException {
        Logger.info("World generator version: 1");
        int seed = (int) System.currentTimeMillis();
        Logger.info("World seed: " + seed);
        Random rand = new Random(seed);
        Logger.info("Running noise generation...");
        Logger.info("Roughness: " + Game.mapRoughness);
    }
}
