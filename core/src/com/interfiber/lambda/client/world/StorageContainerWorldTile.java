package com.interfiber.lambda.client.world;

import com.interfiber.lambda.client.items.SerializableItem;

import java.util.concurrent.ConcurrentHashMap;

public class StorageContainerWorldTile extends WorldTile {
    public ConcurrentHashMap<Integer, SerializableItem> contents = new ConcurrentHashMap<>();
}
