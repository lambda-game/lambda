package com.interfiber.lambda.client.world;

public enum WorldType {
    OVERWORLD,
    UNDERWORLD,
    UNKNOWN
}
