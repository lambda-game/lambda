package com.interfiber.lambda.client.tiles.info;

import com.interfiber.lambda.client.items.ItemType;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;

import java.util.HashMap;

public class TileHardness {

    // the int in the hashmap is the time in seconds it takes to break the tile
    private static final HashMap<TileType, Integer> hardnessDB = new HashMap<>();
    private static final int defaultBreakValue = 0;

    public static int getHardnessForTile(TileType type) {
        if (hardnessDB.size() == 0) {
            buildHardnessDb();
        }
        int change = 0;
        if (Player.inventory.getCurrentlySelectedItem() != null) {
            change = getHardnessChangeForItem(
                    Player.inventory.getCurrentlySelectedItem().getType());
        }

        return hardnessDB.getOrDefault(type, defaultBreakValue) - change;
    }

    public static int getHardnessChangeForItem(ItemType item) {
        switch (item) {
            case WOODEN_PICKAXE:
                return 1;
            case STONE_PICKAXE:
                return 2;
            case IRON_PICKAXE:
                return 4;
            default:
                return 0;
        }
    }

    public static void buildHardnessDb() {

        // clear the database
        hardnessDB.clear();

        // rebuild it
        hardnessDB.put(TileType.WOOD_PLANKS, 3);
        hardnessDB.put(TileType.CAVE_WALL_STONE, 9);
        hardnessDB.put(TileType.IRON_ORE_WALL, 11);
        hardnessDB.put(TileType.ROCK, 5);
        hardnessDB.put(TileType.TREE, 8);
        hardnessDB.put(TileType.TORCH, 0);
        hardnessDB.put(TileType.FURNACE, 4);
        hardnessDB.put(TileType.WORKBENCH, 3);
        hardnessDB.put(TileType.CHEST, 5);
    }
}
