package com.interfiber.lambda.client.tiles;

import com.interfiber.lambda.client.player.Inventory;

// StorageContainer
// A storage container is a tile that can contain items.
// Its stored just like a normal tile, but instead of using a WorldTile it uses a StorageContainerWorldTile

public abstract class StorageContainer extends Tile {
    public abstract Inventory getStorageInventory();

    public abstract String getTitle();
}
