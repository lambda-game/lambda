package com.interfiber.lambda.client.tiles;

import box2dLight.PointLight;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.lighting.LightSource;
import com.interfiber.lambda.client.loot.TileLoot;
import com.interfiber.lambda.client.loot.tiles.TorchTileLoot;

public class TorchTile extends LightSource {
    @Override
    public Texture getTextureName() {
        return Assets.manager.get("torch.png", Texture.class);
    }

    @Override
    public void render(SpriteBatch tileBatch, int x, int y) {
        tileBatch.draw(getTextureName(), x, y);
    }

    @Override
    public boolean onDestroy() {
        return true;
    }

    @Override
    public TileLoot getLoot() {
        return new TorchTileLoot();
    }

    @Override
    public void createLight(int x, int y) {
        PointLight light = Game.lightManager.createPointLight(Color.WHITE, 100f, x, y);
        Game.lightManager.addLight(this.generateAndStoreUUID(), light);
        addLightToTracker(light);
    }

    @Override
    public boolean onPlace(int x, int y) {
        if (Game.getCurrentWorld().getTileAtPosition(x, y).type != TileType.WATER) {
            createLight(x, y);
            return true;
        } else {
            return false;
        }
    }
}
