package com.interfiber.lambda.client.tiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.loot.TileLoot;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.tilehandlers.TileHandler;
import com.interfiber.lambda.client.world.collision.WorldMap;

import java.util.UUID;

public abstract class Tile {
    private static String tileColliderId = "none";
    public Texture texture;

    private static String generateColliderID() {
        return UUID.randomUUID().toString();
    }

    public abstract Texture getTextureName();

    public abstract void render(SpriteBatch tileBatch, int x, int y);

    public abstract boolean onDestroy();

    public abstract TileLoot getLoot();

    public void attachCollider(int x, int y, int height, int width) {
        // generate a collider ID
        String colliderID = generateColliderID();
        // add the collider to the world map
        WorldMap.addCollider(colliderID, x, y, height, width);

        // save the collider ID
        tileColliderId = colliderID;
    }

    public boolean destroyTile() {
        TileLoot loot = this.getLoot();
        boolean canDestroy = this.onDestroy();
        if (canDestroy) {
            if (loot != null) {
                loot.giveToPlayer(Player.inventory);
            }
        }
        return canDestroy;
    }

    public String getColliderId() {
        return tileColliderId;
    }

    public TileHandler getTileHandler() {
        return null;
    }

    public boolean onPlace(int x, int y) {
        return true;
    }

}
