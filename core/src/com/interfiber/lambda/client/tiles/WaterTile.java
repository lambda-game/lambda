package com.interfiber.lambda.client.tiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.loot.TileLoot;

public class WaterTile extends Tile {
    @Override
    public Texture getTextureName() {
        return Assets.manager.get("water.png", Texture.class);
    }

    public void render(SpriteBatch renderBatch, int x, int y) {
        renderBatch.draw(getTextureName(), x, y);
    }

    @Override
    public boolean onDestroy() {
        return false;
    }

    @Override
    public TileLoot getLoot() {
        return null;
    }
}
