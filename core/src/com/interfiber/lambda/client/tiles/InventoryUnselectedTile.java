package com.interfiber.lambda.client.tiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.loot.TileLoot;

public class InventoryUnselectedTile extends Tile {
    @Override
    public Texture getTextureName() {
        return Assets.manager.get("inventory_unselected.png", Texture.class);
    }

    @Override
    public void render(SpriteBatch tileBatch, int x, int y) {
        tileBatch.draw(getTextureName(), x, y);
    }

    @Override
    public boolean onDestroy() {
        return false;
    }

    @Override
    public TileLoot getLoot() {
        return null;
    }
}
