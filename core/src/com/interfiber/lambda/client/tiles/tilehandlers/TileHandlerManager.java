package com.interfiber.lambda.client.tiles.tilehandlers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.math.Vector3;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;
import com.interfiber.lambda.client.world.World;
import com.interfiber.lambda.client.world.WorldTile;
import org.tinylog.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class TileHandlerManager {
    private static final HashMap<TileType, TileHandler> tileHandlers = new HashMap<>();

    public static void addHandler(TileType tileType, TileHandler handler) {
        tileHandlers.put(tileType, handler);
    }


    public static void executeAllHandlers() {
        for (Map.Entry<TileType, TileHandler> set : tileHandlers.entrySet()) {
            TileType tileType = set.getKey();
            TileHandler handler = set.getValue();
            // check if we should execute the handler

            // interact key is X
            if (Gdx.input.isKeyJustPressed(Input.Keys.X)) {
                World currentWorld = Game.getCurrentWorld();
                // get the direction we are facing
                String facing = Player.facing;

                // get the new player position
                Vector3 newPlayerPosition;
                if (Objects.equals(facing, "down")) {
                    newPlayerPosition = new Vector3(Player.x, Player.y - Game.tileSize, 0);
                } else if (Objects.equals(facing, "up")) {
                    newPlayerPosition = new Vector3(Player.x, Player.y + Game.tileSize, 0);
                } else if (Objects.equals(facing, "left")) {
                    newPlayerPosition = new Vector3(Player.x - Game.tileSize, Player.y, 0);
                } else if (Objects.equals(facing, "right")) {
                    newPlayerPosition = new Vector3(Player.x + Game.tileSize, Player.y, 0);
                } else {
                    throw new IllegalStateException(
                            "Invalid Player.facing value, cannot determine player facing direction");
                }

                // get the tile position
                int tileNumX = (int) Math.floor(newPlayerPosition.x / Game.tileSize);
                int tileNumY = (int) Math.floor(newPlayerPosition.y / Game.tileSize);

                // get the WorldTile that we are aimed at, null if nothing
                WorldTile playerPlacedTile = currentWorld
                        .getUserTileAtPosition(tileNumX * Game.tileSize, tileNumY * Game.tileSize);

                if (playerPlacedTile == null) {
                    // check for a non-user placed tile
                    WorldTile searchTile = currentWorld.getTileAtPosition(tileNumX * Game.tileSize,
                            tileNumY * Game.tileSize);
                    if (searchTile != null) {
                        playerPlacedTile = searchTile;
                    } else {
                        return;
                    }
                }

                if (tileType == playerPlacedTile.type) {
                    Logger.debug("Player interacted with tile " + playerPlacedTile.type
                            + ", running handler");
                    handler.interact(playerPlacedTile);
                }
            }
        }
    }

    public static TileHandler getHandler(TileType tileType) {
        return tileHandlers.get(tileType);
    }

    public static HashMap<TileType, TileHandler> getTileHandlers() {
        return tileHandlers;
    }

}
