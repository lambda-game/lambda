package com.interfiber.lambda.client.tiles.tilehandlers;

import com.interfiber.lambda.client.world.WorldTile;

public abstract class TileHandler {
    public abstract void interact(WorldTile worldTile); // on tile interaction

    public abstract void create(); // handler creation
}
