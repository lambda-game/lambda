package com.interfiber.lambda.client.tiles.tilehandlers;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.multiplayer.MultiplayerGameState;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.screens.GameOverworldScreen;
import com.interfiber.lambda.client.screens.GameUnderworldScreen;
import com.interfiber.lambda.client.screens.SwitchingWorldScreen;
import com.interfiber.lambda.client.world.WorldTile;
import com.interfiber.lambda.client.world.WorldType;
import com.interfiber.lambda.server.networking.packets.ParsedPacket;
import com.interfiber.lambda.server.networking.packets.serverbound.SwitchWorldPacket;
import org.tinylog.Logger;

public class CaveEntranceTileHandler extends TileHandler {
    @Override
    public void interact(WorldTile worldTile) {


        WorldType targetWorld;

        if (Game.currentWorld == WorldType.OVERWORLD) {
            targetWorld = WorldType.UNDERWORLD;
        } else if (Game.currentWorld == WorldType.UNDERWORLD) {
            targetWorld = WorldType.OVERWORLD;
        } else {
            Logger.warn("Invalid world type");
            return;
        }

        if (Game.multiplayerEnabled) {
            ParsedPacket packet = new ParsedPacket();
            packet.initGuard(MultiplayerGameState.player.authUUID);
            packet.content.put("targetWorld", targetWorld);

            new SwitchWorldPacket(packet).writeData(MultiplayerGameState.serverConnection);
            Game.screenManager.switchScene(new SwitchingWorldScreen());
            return;
        }

        if (targetWorld == WorldType.UNDERWORLD) {
            Logger.info("Switching to underworld scene");
            Game.currentWorld = WorldType.UNDERWORLD;
            Player.caveEnterX = Player.x;
            Player.caveEnterY = Player.y;
            Game.screenManager.switchScene(new GameUnderworldScreen(true));
        } else {
            Logger.info("Switching to overworld scene");
            Game.currentWorld = WorldType.OVERWORLD;
            Player.x = Player.caveEnterX;
            Player.y = Player.caveEnterY;
            Game.screenManager.switchScene(new GameOverworldScreen());
        }

    }

    @Override
    public void create() {
    }
}
