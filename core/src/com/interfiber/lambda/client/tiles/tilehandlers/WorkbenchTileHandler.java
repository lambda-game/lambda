package com.interfiber.lambda.client.tiles.tilehandlers;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.crafting.CraftingMenuMode;
import com.interfiber.lambda.client.crafting.CraftingUtils;
import com.interfiber.lambda.client.world.WorldTile;

public class WorkbenchTileHandler extends TileHandler {

    @Override
    public void interact(WorldTile worldTile) {
        // if we interact with the gui when the crafting menu is already open, close it.
        if (Game.craftingMenuOpen) {
            Game.craftingMenuOpen = false;
            return;
        }
        // show crafting gui
        Game.craftingMenuMode = CraftingMenuMode.WORKBENCH_CRAFTING;
        CraftingUtils.rebuildOverlayRecipeCache();
        Game.craftingMenuOpen = true;
    }

    @Override
    public void create() {

    }
}
