package com.interfiber.lambda.client.tiles.tilehandlers;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.world.WorldTile;

public class StorageContainerTileHandler extends TileHandler {

    @Override
    public void interact(WorldTile worldTile) {
        if (Game.chestOpen) {
            Game.chestOpen = false;
            Game.chestX = -1;
            Game.chestY = -1;
        } else {
            Game.chestOpen = true;
            Game.chestX = worldTile.x;
            Game.chestY = worldTile.y;
        }
    }

    @Override
    public void create() {
    }
}
