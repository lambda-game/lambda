package com.interfiber.lambda.client.tiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.loot.TileLoot;
import com.interfiber.lambda.client.world.collision.WorldMap;

import java.util.Objects;

public class CobblestoneTile extends Tile {
    @Override
    public Texture getTextureName() {
        return Assets.manager.get("cobblestone.png", Texture.class);

    }

    @Override
    public void render(SpriteBatch tileBatch, int x, int y) {
        if (!Objects.equals(getColliderId(), "none")) {
            WorldMap.getCollider(getColliderId()).setPosition(new Vector2(x, y));
        }
        tileBatch.draw(getTextureName(), x, y);
    }

    @Override
    public boolean onDestroy() {
        return false;
    }

    @Override
    public TileLoot getLoot() {
        return null;
    }
}
