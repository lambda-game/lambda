package com.interfiber.lambda.client.tiles;

import java.util.HashMap;

public class TileLoader {
    private static final HashMap<TileType, Tile> tiles = new HashMap<>();

    private static String lastAccessedTile = "";

    public static void addTile(TileType tileType, Tile tile) {
        tiles.put(tileType, tile);
    }

    public static Tile getTile(TileType tileId) {
        Tile tile = tiles.get(tileId);
        if (tile == null) {
            throw new RuntimeException("Failed to get tile with id: " + tileId);
        } else {
            lastAccessedTile = tileId.toString();
            return tile;
        }
    }

    public static HashMap<TileType, Tile> getTiles() {
        return tiles;
    }

    public static String getLastAccessedTile() {
        return lastAccessedTile;
    }
}
