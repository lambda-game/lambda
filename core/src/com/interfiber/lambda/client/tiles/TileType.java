package com.interfiber.lambda.client.tiles;

// tiles in the world, excludes stuff like the player, fog, and inventory selected tiles
public enum TileType {
    WATER,
    GRASS,
    TREE,
    AIR,
    STONE,
    WOOD_PLANKS,
    ITEM,
    WORKBENCH,
    TORCH,
    FURNACE,
    CAVE_ENTRANCE,
    ROCK,
    CAVE_WALL_STONE,
    COBBLESTONE,
    FOG,
    INVENTORYSELECTED,
    INVENTORYUNSELECTED,
    IRON_ORE_WALL,
    CHEST,
    FARMLAND,
    WATERED_FARMLAND,
    WHEAT_SEEDS,
    SAND,
    LEAFY_GRASS
}
