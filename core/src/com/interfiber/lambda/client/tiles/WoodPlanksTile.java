package com.interfiber.lambda.client.tiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.loot.TileLoot;
import com.interfiber.lambda.client.loot.tiles.WoodPlanksTileLoot;

public class WoodPlanksTile extends Tile {

    @Override
    public Texture getTextureName() {
        return Assets.manager.get("wood_planks.png", Texture.class);
    }

    @Override
    public void render(SpriteBatch tileBatch, int x, int y) {
        tileBatch.draw(getTextureName(), x, y);
    }

    @Override
    public boolean onDestroy() {
        return true;
    }

    @Override
    public TileLoot getLoot() {
        return new WoodPlanksTileLoot();
    }
}
