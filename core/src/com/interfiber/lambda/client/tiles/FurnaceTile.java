package com.interfiber.lambda.client.tiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.loot.TileLoot;
import com.interfiber.lambda.client.loot.tiles.FurnaceTileLoot;
import com.interfiber.lambda.client.tiles.tilehandlers.TileHandler;
import com.interfiber.lambda.client.tiles.tilehandlers.TileHandlerManager;

public class FurnaceTile extends Tile {
    @Override
    public Texture getTextureName() {
        return Assets.manager.get("furnace.png", Texture.class);
    }

    @Override
    public void render(SpriteBatch tileBatch, int x, int y) {
        tileBatch.draw(getTextureName(), x, y);
    }

    @Override
    public boolean onDestroy() {
        return true;
    }

    @Override
    public TileLoot getLoot() {
        return new FurnaceTileLoot();
    }

    @Override
    public TileHandler getTileHandler() {
        return TileHandlerManager.getHandler(TileType.FURNACE);
    }
}
