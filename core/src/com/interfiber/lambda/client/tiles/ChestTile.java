package com.interfiber.lambda.client.tiles;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.loot.TileLoot;
import com.interfiber.lambda.client.loot.tiles.ChestTileLoot;
import com.interfiber.lambda.client.player.Inventory;

public class ChestTile extends StorageContainer {

    @Override
    public Inventory getStorageInventory() {
        return null;
    }

    @Override
    public String getTitle() {
        return "Chest";
    }

    @Override
    public Texture getTextureName() {
        return Assets.manager.get("chest.png");
    }

    @Override
    public void render(SpriteBatch tileBatch, int x, int y) {
        tileBatch.draw(this.getTextureName(), x, y);
    }

    @Override
    public boolean onDestroy() {
        return true;
    }

    @Override
    public TileLoot getLoot() {
        return new ChestTileLoot();
    }
}
