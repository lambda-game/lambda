package com.interfiber.lambda.client.lighting;

import com.badlogic.gdx.utils.TimeUtils;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.server.MultiplayerGame;
import org.tinylog.Logger;

public class DayNightCycle {
    private static final Time[] order = {Time.MORNING, Time.NOON, Time.AFTERNOON, Time.NIGHT};
    private static long startTime = TimeUtils.millis();
    private static int index = 0;

    public static void render() {

        if (Game.time == Time.MORNING) {
            Game.lightManager.setLightLevel(.7f, .7f, .7f, .7f);
        } else if (Game.time == Time.NOON) {
            Game.lightManager.setLightLevel(.9f, .9f, .9f, .9f);
        } else if (Game.time == Time.AFTERNOON) {
            Game.lightManager.setLightLevel(1f, 1f, 1f, 1f);
        } else if (Game.time == Time.EVENING) {
            Game.lightManager.setLightLevel(.7f, .7f, .7f, .7f);
        } else if (Game.time == Time.NIGHT) {
            Game.lightManager.setLightLevel(.2f, .2f, .2f, .2f);
        } else {
            Logger.warn("Invalid time: " + Game.time + ", setting time to morning");
            Game.time = Time.MORNING;
        }

        if (!Game.multiplayerEnabled) {
            if (TimeUtils.timeSinceMillis(startTime) > 300000) {
                Logger.info("DayNight cycle updating");
                startTime = TimeUtils.millis();
                if (index + 1 > order.length) {
                    index = 0;
                }
                if (MultiplayerGame.server != null) {
                    MultiplayerGame.time = order[index];
                    MultiplayerGame.sendTimeUpdate();
                } else {
                    Game.time = order[index];
                }
            }
        }

    }
}
