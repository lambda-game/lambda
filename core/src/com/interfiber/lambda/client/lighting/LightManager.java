package com.interfiber.lambda.client.lighting;

import box2dLight.PointLight;
import box2dLight.RayHandler;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.tiles.Tile;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.world.WorldTile;
import org.tinylog.Logger;

import java.util.HashMap;
import java.util.List;

public class LightManager {
    private final HashMap<String, PointLight> lightList = new HashMap<>();
    private RayHandler rayHandler;
    private Camera masterCamera;
    private World world;

    public void init() {
        world = new World(new Vector2(), true);
        RayHandler.useDiffuseLight(true);

        rayHandler = new RayHandler(world);
        rayHandler.setBlur(false);
        rayHandler.setCulling(true);
        rayHandler.setAmbientLight(1f);
    }

    public void setCamera(Camera camera) {
        masterCamera = camera;
    }

    public void addLight(String lightID, PointLight light) {
        lightList.put(lightID, light);
    }

    public void setLightLevel(float r, float g, float b, float a) {
        rayHandler.setAmbientLight(r, g, b, a);
    }

    public PointLight createPointLight(Color color, float dist, int x, int y) {
        if (rayHandler == null) {
            throw new IllegalStateException("rayHandler cannot be null!");
        }
        return new PointLight(rayHandler, 200, color, dist, x, y);
    }

    public void setLightPosition(String lightID, int newX, int newY) {
        PointLight light = lightList.get(lightID);
        if (light == null) {
            Logger.warn("Failed to update light position: lightID is not in list of lights");
        } else {
            light.setPosition(newX, newY);
            lightList.remove(lightID);
            lightList.put(lightID, light);
        }
    }

    public void renderLights() {
        rayHandler.setCombinedMatrix((OrthographicCamera) masterCamera);
        rayHandler.updateAndRender();
    }

    public void dispose() {
        rayHandler.dispose();
        world.dispose();
    }

    public void lightWorld() {
        List<WorldTile> worldTileList = Game.getCurrentWorld().getWorldData();
        for (WorldTile tile : worldTileList) {
            Tile renderTile = TileLoader.getTile(tile.type);
            if (renderTile instanceof LightSource) {
                ((LightSource) renderTile).createLight(tile.x, tile.y);
            }
        }
    }

    public boolean getLight(String lightId) {
        return lightList.containsKey(lightId);
    }
}
