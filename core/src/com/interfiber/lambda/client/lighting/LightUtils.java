package com.interfiber.lambda.client.lighting;

import box2dLight.PointLight;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.Tile;
import com.interfiber.lambda.client.tiles.TileLoader;
import com.interfiber.lambda.client.tiles.TileType;
import org.tinylog.Logger;

import java.util.Map;

public class LightUtils {
    public static void rebuildLighting() {
        new Thread(() -> Gdx.app.postRunnable(() -> {
            for (Map.Entry<TileType, Tile> set : TileLoader.getTiles().entrySet()) {
                if (set.getValue() instanceof LightSource) {
                    ((LightSource) set.getValue()).clearLights();
                }
            }
            Game.lightManager.lightWorld();
        })).start();
    }

    public static void initPlayerLight() {
        boolean lightExists = Game.lightManager.getLight("player");
        if (!lightExists) {
            PointLight playerLight = Game.lightManager.createPointLight(Color.WHITE, 100f, Player.x, Player.y);
            Game.lightManager.addLight("player", playerLight);
        }
    }

    public static void updatePlayerLight() {
        boolean lightExists = Game.lightManager.getLight("player");
        if (!lightExists) {
            Logger.error("Player light does not exist, cannot update light position!");
        } else {
            Game.lightManager.setLightPosition("player", Player.x, Player.y);
        }
    }
}
