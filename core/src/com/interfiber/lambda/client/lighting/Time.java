package com.interfiber.lambda.client.lighting;

public enum Time {
    MORNING,
    NOON,
    AFTERNOON,
    EVENING,
    NIGHT
}
