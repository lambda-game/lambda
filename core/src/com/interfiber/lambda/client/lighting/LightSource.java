package com.interfiber.lambda.client.lighting;

import box2dLight.PointLight;
import com.interfiber.lambda.client.tiles.Tile;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public abstract class LightSource extends Tile {
    private final List<PointLight> tileLights = new ArrayList<>();

    public String generateAndStoreUUID() {
        return UUID.randomUUID().toString();
    }

    public abstract void createLight(int x, int y);

    public void addLightToTracker(PointLight light) {
        this.tileLights.add(light);
    }

    public void clearLights() {
        for (PointLight light : tileLights) {
            light.remove();
        }
        tileLights.clear();
    }

}
