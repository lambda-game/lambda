package com.interfiber.lambda.client;

import com.badlogic.gdx.math.Vector2;
import com.interfiber.lambda.client.alerts.AlertSystem;
import com.interfiber.lambda.client.crafting.CraftingMenuMode;
import com.interfiber.lambda.client.lighting.LightManager;
import com.interfiber.lambda.client.lighting.Time;
import com.interfiber.lambda.client.multiplayer.ClientChatMessage;
import com.interfiber.lambda.client.options.GameOptions;
import com.interfiber.lambda.client.screens.ScreenManager;
import com.interfiber.lambda.client.ticks.TickManager;
import com.interfiber.lambda.client.weather.Weather;
import com.interfiber.lambda.client.world.Overworld;
import com.interfiber.lambda.client.world.Underworld;
import com.interfiber.lambda.client.world.World;
import com.interfiber.lambda.client.world.WorldType;
import org.tinylog.Logger;

import java.util.ArrayList;


// the Game class is a global class to store game state info in
public class Game {
    public static int mapHeight = 1000; // height of the world in tiles
    public static int mapWidth = 1000; // width of the world in tiles
    public static float mapRoughness = 1.84f; // world generator setting for roughness
    public static int worldSeed = 0; // the current world seed
    public static int tileSize = 32; // tile size of a tile, currently 32x32 public static Vector2
    public static Vector2 overworldSpawnPoint = new Vector2(0, 0); // overworld spawn point
    public static Vector2 underworldSpawnPoint = new Vector2(0, 0); // underworld spawn point
    public static Overworld overworld; // overworld instance for the game
    public static Underworld underworld; // underworld(caves) instance for the game
    public static WorldType currentWorld = WorldType.UNKNOWN; // current world
    public static int inventoryBottomBarSlots = 9; // inventory hot bar slots
    public static int maxStackSize = 64; // max stack size of items
    public static boolean hiddenCursor = false; // if the cursor is hidden
    public static String worldCreatorName = ""; // current world name in the creator
    public static String serverIp = ""; // server ip
    public static int currentSaveFile = -1; // current save file
    public static boolean pauseMenuOpen = false; // if the pause menu is open
    public static boolean multiplayerEnabled = false; // if connected to a multiplayer server
    public static boolean craftingMenuOpen = false; // if the crafting menu is open
    public static GameOptions options; // current game options
    public static CraftingMenuMode craftingMenuMode = CraftingMenuMode.PORTABLE_CRAFTING; // crafting
    // menu
    // mode
    public static boolean developerModeEnabled = false; // if developer mode is enabled
    public static boolean debugOverlayEnabled = false; // if the debug overlay is enabled
    public static boolean debugColliderEnabled = true; // disables colliders
    public static Time time; // current time
    public static LightManager lightManager; // the global light manager
    public static ScreenManager screenManager;
    public static AlertSystem alertSystem; // game alert system
    public static TickManager tickManager; // tick manager
    public static float tileBreakProgress = 0; // tile break progress
    public static boolean chestOpen = false; // if we have a chest open
    public static int chestX = -1; // x coords of the open chest
    public static int chestY = -1; // y coords of the open chest
    public static Weather weather; // weather in-game, if null its clear weather
    public static int gameTicks = 0; // how many game ticks have passed in-game
    public static String serverConnectStatus = "Connecting..."; // server connect status show in-game
    public static boolean chatOpen = false; // is the chat open?
    public static String chatMessage = ""; // chat message
    public static ArrayList<ClientChatMessage> chatHistory = new ArrayList<>();
    public static String authToken = null; // multiplayer auth token

    public static World getCurrentWorld() {
        if (Game.currentWorld == WorldType.OVERWORLD) {
            return Game.overworld;
        } else if (Game.currentWorld == WorldType.UNDERWORLD) {
            return Game.underworld;
        } else {
            Logger.error("Failed to get current world!");
            throw new IllegalStateException(
                    "Invalid WorldType, failed to get current world instance!");
        }
    }
}
