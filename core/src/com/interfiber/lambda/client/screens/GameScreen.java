package com.interfiber.lambda.client.screens;

import com.badlogic.gdx.ScreenAdapter;

public abstract class GameScreen extends ScreenAdapter {
    public abstract boolean renderOverlays(); // should we render overlays in this screen?
}
