package com.interfiber.lambda.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.world.WorldSaver;
import com.interfiber.lambda.client.world.WorldType;
import org.tinylog.Logger;

import java.util.ArrayList;
import java.util.List;

public class LoadWorldScreen extends ScreenAdapter {
    String worldStoragePath;
    GlyphLayout titleLayout;
    GlyphLayout worldLayout;
    List<Integer> worlds = new ArrayList<>();
    String title = "Select world, press ENTER when done";
    private BitmapFont menuFont;
    private SpriteBatch menuSpriteBatch;
    private int worldSelectionMax = 0;
    private int worldSelected = 0;
    private int tick = 0;

    @Override
    public void show() {
        FreeTypeFontGenerator fontGenerator =
                new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter menuFontGenerated =
                new FreeTypeFontGenerator.FreeTypeFontParameter();
        menuFontGenerated.size = 65;
        menuFont = fontGenerator.generateFont(menuFontGenerated);
        WorldSaver.createWorldStorage();
        worldStoragePath = WorldSaver.getStorageDir();
        Logger.info("Loading worlds");
        worlds = WorldSaver.getWorlds();
        worldSelectionMax = worlds.size();
        if (worldSelectionMax == 0) {
            Logger.warn("No worlds to load, resetting scene to main menu");
            Game.alertSystem.createToast("No worlds to load, please create one!");
            Game.screenManager.switchScene(new MainMenuScreen());
        }
        Game.hiddenCursor = true;
        fontGenerator.dispose();
        menuSpriteBatch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            if (worldSelected + 1 >= worldSelectionMax) {
                worldSelected = 0;
            } else {
                worldSelected += 1;
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            if (worldSelected - 1 < 0) {
                worldSelected = worldSelectionMax - 1;
            } else {
                worldSelected -= 1;
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) && tick == 0) {
            title = "Loading world, please wait...";
            Logger.info("Loading world: " + worldSelected);
            tick = 1;
        }
        if (tick != 0) {
            tick += 1;
        }
        if (tick == 5) {
            WorldSaver.loadWorld(worldSelected);
            Logger.info("Switching scenes...");
            if (Game.currentWorld == WorldType.OVERWORLD) {
                Game.screenManager.switchScene(new GameOverworldScreen());
            } else if (Game.currentWorld == WorldType.UNDERWORLD) {
                Game.screenManager.switchScene(new GameUnderworldScreen(false));
            } else {
                Logger.error(
                        "Failed to switch to World scene: Invalid Game.currentWorld type, save file corrupted or game bug!!!");
                throw new IllegalStateException("Invalid Game.currentWorld type");
            }
        }
        titleLayout = new GlyphLayout(menuFont, title);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        menuSpriteBatch.begin();
        menuSpriteBatch.draw(Assets.manager.get("river_thunder_bg.png", Texture.class), 0, 0);
        menuFont.draw(menuSpriteBatch, titleLayout,
                (Gdx.graphics.getWidth() - titleLayout.width) / 2, Gdx.graphics.getHeight() - 40);
        for (int x = 0; x < worlds.size(); x++) {
            if (x == worldSelected) {
                worldLayout = new GlyphLayout(menuFont, ">  World " + x);
            } else {
                worldLayout = new GlyphLayout(menuFont, "World " + x);
            }
            if (x == 0) {
                menuFont.draw(menuSpriteBatch, worldLayout,
                        (Gdx.graphics.getWidth() - worldLayout.width) / 2,
                        Gdx.graphics.getHeight() - 100 - (menuFont.getLineHeight() - x));
            } else {
                menuFont.draw(menuSpriteBatch, worldLayout,
                        (Gdx.graphics.getWidth() - worldLayout.width) / 2, Gdx.graphics.getHeight()
                                - (100 * x + 100) - (menuFont.getLineHeight() - x));
            }
        }
        menuSpriteBatch.end();
    }

    @Override
    public void dispose() {
        menuSpriteBatch.dispose();
        menuFont.dispose();
    }

    @Override
    public void resize(int width, int height) {
    }
}
