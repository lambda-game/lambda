package com.interfiber.lambda.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import org.tinylog.Logger;

public class MainMenuScreen extends GameScreen {
    String loadString = "    1. Load";
    String createString = "    2. Create";
    String optionsString = "    3. Options";
    String serverString = "    4. Multiplayer";
    String exitString = "    5. Quit";
    int currentSelected = 0;
    int selectedMax = 4;
    GlyphLayout loadLayout;
    GlyphLayout newLayout;
    GlyphLayout exitLayout;
    GlyphLayout serverLayout;
    GlyphLayout optionsLayout;
    private SpriteBatch menuSpriteBatch;
    private BitmapFont menuFont;
    private BitmapFont smallMenuFont;
    private GlyphLayout layout;

    @Override
    public void show() {
        menuSpriteBatch = new SpriteBatch();
        FreeTypeFontGenerator fontGenerator =
                new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter menuFontGenerated =
                new FreeTypeFontGenerator.FreeTypeFontParameter();
        menuFontGenerated.size = 65;
        menuFont = fontGenerator.generateFont(menuFontGenerated);
        menuFontGenerated.size = 34;
        smallMenuFont = fontGenerator.generateFont(menuFontGenerated);
        String title = "Lambda: Alpha";
        layout = new GlyphLayout(menuFont, title);
        Game.hiddenCursor = true;
    }

    @Override
    public void render(float delta) {
        loadLayout = new GlyphLayout(menuFont, loadString);
        newLayout = new GlyphLayout(menuFont, createString);
        exitLayout = new GlyphLayout(menuFont, exitString);
        optionsLayout = new GlyphLayout(menuFont, optionsString);
        serverLayout = new GlyphLayout(menuFont, serverString);
        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            if (currentSelected - 1 < 0) {
                currentSelected = selectedMax;
            } else {
                currentSelected -= 1;
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            if (currentSelected + 1 > selectedMax) {
                currentSelected = 0;
            } else {
                currentSelected += 1;
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            Logger.info("Selected menu with ID: " + currentSelected);
            if (currentSelected == 4) {
                Logger.info("Exiting...");
                System.exit(0);
            } else if (currentSelected == 1) {
                Logger.info("Switching scenes to: Scene creator");
                Game.screenManager.switchScene(new NewWorldScreen());
            } else if (currentSelected == 0) {
                Logger.info("Switching scenes");
                Game.screenManager.switchScene(new LoadWorldScreen());
            } else if (currentSelected == 2) {
                Logger.info("Switching scenes to: Options");
                Game.screenManager.switchScene(new GameConfigScreen());
            } else if (currentSelected == 3) {
                if (Game.authToken == null) {
                    Game.alertSystem.createToast("Please sign in via the launcher to play multiplayer");
                } else {
                    Logger.info("Switching scenes to: server connect");
                    Game.screenManager.switchScene(new ConnectToServerScreen());
                }
            }
        }

        if (currentSelected == 0) {
            loadLayout = new GlyphLayout(menuFont, ">    1. Load");
        } else if (currentSelected == 1) {
            newLayout = new GlyphLayout(menuFont, ">    2. Create");
        } else if (currentSelected == 2) {
            optionsLayout = new GlyphLayout(menuFont, ">    3. Options");
        } else if (currentSelected == 3) {
            serverLayout = new GlyphLayout(menuFont, ">    4. Multiplayer");
        } else if (currentSelected == 4) {
            exitLayout = new GlyphLayout(menuFont, ">    5. Quit");
        }
        menuSpriteBatch.begin();
        menuSpriteBatch.draw(Assets.manager.get("beach_thunder_bg.png", Texture.class), 0, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        menuFont.draw(menuSpriteBatch, layout, (Gdx.graphics.getWidth() - layout.width) / 2,
                Gdx.graphics.getHeight() / 2);
        menuFont.draw(menuSpriteBatch, loadLayout, (Gdx.graphics.getWidth() - layout.width) / 2,
                Gdx.graphics.getHeight() / 2 - (smallMenuFont.getLineHeight() * 2));
        menuFont.draw(menuSpriteBatch, newLayout, (Gdx.graphics.getWidth() - layout.width) / 2,
                Gdx.graphics.getHeight() / 2 - (smallMenuFont.getLineHeight() * 4));
        menuFont.draw(menuSpriteBatch, optionsLayout, (Gdx.graphics.getWidth() - layout.width) / 2,
                Gdx.graphics.getHeight() / 2 - (smallMenuFont.getLineHeight() * 6));
        menuFont.draw(menuSpriteBatch, serverLayout, (Gdx.graphics.getWidth() - layout.width) / 2,
                Gdx.graphics.getHeight() / 2 - (smallMenuFont.getLineHeight() * 8));
        menuFont.draw(menuSpriteBatch, exitLayout, (Gdx.graphics.getWidth() - layout.width) / 2,
                Gdx.graphics.getHeight() / 2 - (smallMenuFont.getLineHeight() * 10));
        menuSpriteBatch.end();
    }

    @Override
    public void dispose() {
        menuSpriteBatch.dispose();
        menuFont.dispose();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public boolean renderOverlays() {
        return false;
    }
}
