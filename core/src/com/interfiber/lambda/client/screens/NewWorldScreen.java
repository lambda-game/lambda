package com.interfiber.lambda.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.controllers.WorldNameController;
import com.interfiber.lambda.client.world.WorldSaver;
import org.tinylog.Logger;

import java.io.IOException;

public class NewWorldScreen extends ScreenAdapter {
    String worldStoragePath;
    GlyphLayout titleLayout;
    GlyphLayout newWorldLayout;
    GlyphLayout worldNameLayout;
    String title = "New world creator, press ENTER when done";
    private BitmapFont menuFont;
    private SpriteBatch menuSpriteBatch;
    private int tick = 0;

    @Override
    public void show() {
        FreeTypeFontGenerator fontGenerator =
                new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter menuFontGenerated =
                new FreeTypeFontGenerator.FreeTypeFontParameter();
        menuFontGenerated.size = 65;
        menuFont = fontGenerator.generateFont(menuFontGenerated);
        String enterTextTitle = "Enter world name:";
        newWorldLayout = new GlyphLayout(menuFont, enterTextTitle);
        WorldSaver.createWorldStorage();
        worldStoragePath = WorldSaver.getStorageDir();
        Game.hiddenCursor = true;
        fontGenerator.dispose();
        menuSpriteBatch = new SpriteBatch();
        WorldNameController worldNameListener = new WorldNameController();
        Gdx.input.setInputProcessor(worldNameListener);
    }

    @Override
    public void render(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) && tick == 0) {
            tick = 1;
            title = "Generating world, please wait...";
        }
        if (tick != 0) {
            tick += 1;
        }
        if (tick > 5) {
            tick = 0;
            // create world
            Logger.info("Creating world");
            try {
                WorldSaver.createWorld(WorldSaver.getNewWorldId());
                Game.screenManager.switchScene(new GameOverworldScreen());
            } catch (IOException e) {
                Logger.error(e);
                throw new RuntimeException("Failed to create world");
            }
        }
        worldNameLayout = new GlyphLayout(menuFont, Game.worldCreatorName);
        titleLayout = new GlyphLayout(menuFont, title);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        menuSpriteBatch.begin();
        menuSpriteBatch.draw(Assets.manager.get("river_thunder_bg.png", Texture.class), 0, 0);
        menuFont.draw(menuSpriteBatch, titleLayout,
                (Gdx.graphics.getWidth() - titleLayout.width) / 2, Gdx.graphics.getHeight() - 40);
        menuFont.draw(menuSpriteBatch, newWorldLayout,
                (Gdx.graphics.getWidth() - newWorldLayout.width) / 2,
                Gdx.graphics.getHeight() - (40 * 4));
        menuFont.draw(menuSpriteBatch, worldNameLayout,
                (Gdx.graphics.getWidth() - newWorldLayout.width) / 2,
                Gdx.graphics.getHeight() - (40 * 8));
        menuSpriteBatch.end();
    }

    @Override
    public void dispose() {
        menuSpriteBatch.dispose();
        menuFont.dispose();
    }

    @Override
    public void hide() {
        Gdx.input.setInputProcessor(null);
    }

    @Override
    public void resize(int width, int height) {
    }
}
