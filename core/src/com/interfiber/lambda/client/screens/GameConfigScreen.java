package com.interfiber.lambda.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.options.GameOption;
import com.interfiber.lambda.client.options.OptionManager;
import com.interfiber.lambda.client.options.OptionStorage;
import com.interfiber.lambda.client.options.OptionType;
import org.tinylog.Logger;

import java.util.HashMap;
import java.util.Map;

public class GameConfigScreen extends GameScreen {

    private static HashMap<String, GameOption> gameOptions;
    private Stage stage;
    private Table table;
    private SpriteBatch batch;
    private BitmapFont font;
    private Label.LabelStyle labelStyle;
    private int selectedOption = 1;
    private int optionCount;
    private String selectedOptionKey = null;
    private OptionType selectedOptionType = null;

    @Override
    public void show() {
        // load all the game options
        gameOptions = OptionManager.getOptions();
        batch = new SpriteBatch();
        optionCount = gameOptions.size();

        // create gui elements
        stage = new Stage();
        table = new Table();
        table.setFillParent(true);
        table.setDebug(false);
        stage.addActor(table);

        // render fonts

        FreeTypeFontGenerator fontGenerator =
                new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter fontGenerated =
                new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontGenerated.size = 45;
        fontGenerated.genMipMaps = true;
        font = fontGenerator.generateFont(fontGenerated);

        // skin
        labelStyle = new Label.LabelStyle();
        labelStyle.font = font;

        // game state
        Game.hiddenCursor = false;
        Logger.info("Created GameConfigScene");
    }

    @Override
    public void render(float delta) {

        // input

        if (Gdx.input.isKeyJustPressed(Input.Keys.DOWN)) {
            if (selectedOption + 1 > optionCount) {
                selectedOption = 1;
            } else {
                selectedOption += 1;
            }
        }

        if (Gdx.input.isKeyJustPressed(Input.Keys.UP)) {
            if (selectedOption - 1 <= 0) {
                selectedOption = optionCount;
            } else {
                selectedOption -= 1;
            }
        }

        // render

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        batch.begin();
        batch.draw(Assets.manager.get("river_thunder_bg.png", Texture.class), 0, 0);
        batch.end();
        table.clear();
        table.add(new Label("Lambda game config, all changes require a restart", labelStyle));
        table.row();
        table.add(new Label("Press ENTER to change the option value, SPACE to exit menu",
                labelStyle));
        table.row();
        int x = 1;
        for (Map.Entry<String, GameOption> set : gameOptions.entrySet()) {
            OptionType optionType = set.getValue().getOptionType();
            // String desc = set.getValue().getDisplayDesc();
            String name = set.getValue().getDisplayName();
            String key = set.getValue().getStorageKey();
            String value = set.getValue().getDefaultValue();
            if (optionType == OptionType.BOOLEAN_VALUE) {
                if (Game.options.booleanOptionIsSet(key)) {
                    value = String.valueOf(Game.options.getBooleanOption(key));
                }
                if (selectedOption == x) {
                    selectedOptionKey = key;
                    selectedOptionType = optionType;
                    table.add(new Label("> " + name + ": " + value + " <", labelStyle));
                } else {
                    table.add(new Label(name + ": " + value, labelStyle));
                }
                table.row();
            }
            x++;
        }
        stage.act(Gdx.graphics.getDeltaTime());

        stage.draw();

        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER)) {
            if (selectedOptionType == OptionType.BOOLEAN_VALUE) {
                boolean isSet = Game.options.booleanOptionIsSet(selectedOptionKey);
                boolean newValue;
                if (!isSet) {
                    newValue = !Boolean.parseBoolean(
                            OptionManager.getOptionWithKey(selectedOptionKey).getDefaultValue());
                } else {
                    newValue = !Game.options.getBooleanOption(selectedOptionKey);
                }
                Game.options.setBooleanOption(selectedOptionKey, newValue);
            }
            OptionStorage.writeOptionsFile();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            Game.screenManager.switchScene(new MainMenuScreen());
        }
    }

    @Override
    public void dispose() {
        stage.dispose();
        font.dispose();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public boolean renderOverlays() {
        return false;
    }
}
