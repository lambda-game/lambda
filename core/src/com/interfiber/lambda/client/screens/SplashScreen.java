package com.interfiber.lambda.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.misc.SplashInfo;

import java.util.Random;

public class SplashScreen extends GameScreen {

    private GlyphLayout titleLayout;
    private GlyphLayout studioLayout;
    private GlyphLayout splashLayout;
    private GlyphLayout loadingLayout;
    private BitmapFont menuFont;
    private SpriteBatch menuBatch;
    private int tick = 0;

    @Override
    public void show() {

        menuBatch = new SpriteBatch();

        // load fonts
        FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter menuFontGenerated = new FreeTypeFontGenerator.FreeTypeFontParameter();
        menuFontGenerated.size = 65;
        menuFont = fontGenerator.generateFont(menuFontGenerated);
        menuFontGenerated.size = 34;

        menuFont.getData().markupEnabled = true;

        titleLayout = new GlyphLayout(menuFont, SplashInfo.gameName);
        studioLayout = new GlyphLayout(menuFont, "Created by: " + SplashInfo.studioName);
        splashLayout = new GlyphLayout(menuFont, "[YELLOW] > " + SplashInfo.splashStrings[new Random().nextInt(SplashInfo.splashStrings.length)] + " <");
        loadingLayout = new GlyphLayout(menuFont, "[RED] Please wait, loading game");
        Game.hiddenCursor = true;
    }

    @Override
    public void render(float delta) {

        if (Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            tick = 200;
        }

        if (tick == 200) {
            Game.screenManager.switchScene(new MainMenuScreen());
        }
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT); // clear screen
        menuBatch.begin();
        menuFont.draw(menuBatch, titleLayout, (Gdx.graphics.getWidth() - titleLayout.width) / 2, Gdx.graphics.getHeight() / 2);
        menuFont.draw(menuBatch, studioLayout, (Gdx.graphics.getWidth() - studioLayout.width) / 2, Gdx.graphics.getHeight() / 2 - (menuFont.getLineHeight() * 2));
        menuFont.draw(menuBatch, splashLayout, (Gdx.graphics.getWidth() - splashLayout.width) / 2, Gdx.graphics.getHeight() / 2 - (menuFont.getLineHeight() * 3));
        menuFont.draw(menuBatch, loadingLayout, (Gdx.graphics.getWidth() - loadingLayout.width) / 2, menuFont.getLineHeight());
        menuBatch.end();
        tick += 1;
    }

    @Override
    public void dispose() {
        // TODO
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public boolean renderOverlays() {
        return false;
    }
}
