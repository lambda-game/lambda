package com.interfiber.lambda.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.audio.AudioManager;
import com.interfiber.lambda.client.entitys.EntityManager;
import com.interfiber.lambda.client.lighting.LightUtils;
import com.interfiber.lambda.client.misc.Cursor;
import com.interfiber.lambda.client.multiplayer.MultiplayerRendering;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.tilehandlers.TileHandlerManager;
import org.tinylog.Logger;

public class GameUnderworldScreen extends GameScreen {
    Viewport viewport;
    SpriteBatch worldSpriteBatch;
    SpriteBatch entitySpriteBatch;
    SpriteBatch mouseSpriteBatch;
    Music theme;

    boolean usedTravel;

    public GameUnderworldScreen(boolean t) {
        this.usedTravel = t;
    }

    @Override
    public void hide() {
        theme.stop();
    }

    @Override
    public void show() {
        Logger.info("Creating GameUnderWorldScene");

        worldSpriteBatch = new SpriteBatch();
        entitySpriteBatch = new SpriteBatch();
        mouseSpriteBatch = new SpriteBatch();

        // render prep for entity's

        Logger.info("Running render prep for entity's");
        EntityManager.runRenderPrep();

        // check if the player already has a camera
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();

        if (Player.camera != null) {
            Logger.info("Not recreating player camera, player camera already exists");
        } else {
            Player.camera = new OrthographicCamera(w / 2, h / 2);
        }

        if (this.usedTravel) {
            Player.x = (int) Game.underworldSpawnPoint.x;
            Player.y = (int) Game.underworldSpawnPoint.y;
        }

        // set camera position
        Player.camera.position.set(Player.x, Player.y, 0);
        Player.camera.update();


        // set light manager master camera
        Game.lightManager.setCamera(Player.camera);
        LightUtils.rebuildLighting();

        // create viewport
        viewport = new ExtendViewport(Player.camera.viewportWidth, Player.camera.viewportHeight,
                Player.camera);

        Game.lightManager.lightWorld();
        Game.hiddenCursor = false; // no hidden cursor!

        // player light
        LightUtils.initPlayerLight();

        if (Game.multiplayerEnabled) {
            MultiplayerRendering.prepRendering();
        }

        // light level
        Game.lightManager.setLightLevel(0.2f, 0.2f, 0.2f, 0.2f);


        // start playing underground theme
        theme = AudioManager.getMusic("caves_theme_ambient");
        theme.setLooping(true);
        theme.play();

        Logger.info("Underworld Scene creation completed");
    }

    @Override
    public void render(float delta) {
        ScreenUtils.clear(Color.BLACK);
        Player.camera.position.set(Player.x, Player.y, 0);
        Player.camera.update();
        LightUtils.updatePlayerLight();

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        // region render world
        worldSpriteBatch.setProjectionMatrix(Player.camera.combined);
        worldSpriteBatch.begin();
        Game.underworld.render(worldSpriteBatch);
        worldSpriteBatch.end();

        // endregion

        // region render entity's
        entitySpriteBatch.setProjectionMatrix(viewport.getCamera().combined);
        entitySpriteBatch.begin();
        EntityManager.executeControllers();
        // player
        EntityManager.getEntity("player").render(entitySpriteBatch, 0, 0); // render a movable
        if (Game.multiplayerEnabled) {
            MultiplayerRendering.renderPlayers(entitySpriteBatch);
        }
        entitySpriteBatch.end();
        // endregion

        // region render item next to mouse
        mouseSpriteBatch.setProjectionMatrix(viewport.getCamera().combined);
        mouseSpriteBatch.begin();
        Cursor.render(mouseSpriteBatch);
        mouseSpriteBatch.end();
        // endregion

        // region render lighting
        Game.lightManager.renderLights();
        // endregion

        // region execute tile handlers
        TileHandlerManager.executeAllHandlers();
        // endregion
    }

    @Override
    public void dispose() {
        entitySpriteBatch.dispose();
        worldSpriteBatch.dispose();
        mouseSpriteBatch.dispose();
        theme.dispose();
        if (Game.multiplayerEnabled) {
            MultiplayerRendering.dispose();
        }
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public boolean renderOverlays() {
        return true;
    }
}
