package com.interfiber.lambda.client.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;

public class ScreenManager {
    private final Game game;

    public ScreenManager(Game g) {
        this.game = g;
    }

    public void switchScene(Screen screen) {
        game.setScreen(screen);
    }
}
