package com.interfiber.lambda.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.controllers.ServerIpController;
import com.interfiber.lambda.client.multiplayer.MultiplayerThread;
import org.tinylog.Logger;

public class ConnectToServerScreen extends ScreenAdapter {
    GlyphLayout titleLayout;
    GlyphLayout ipLayout;
    String title = "Enter a server IP, press ENTER to connect";
    int tick = 0;
    private BitmapFont menuFont;
    private SpriteBatch menuSpriteBatch;

    @Override
    public void show() {
        FreeTypeFontGenerator fontGenerator =
                new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter menuFontGenerated =
                new FreeTypeFontGenerator.FreeTypeFontParameter();
        menuFontGenerated.size = 65;
        menuFont = fontGenerator.generateFont(menuFontGenerated);
        Game.hiddenCursor = true;
        fontGenerator.dispose();
        menuSpriteBatch = new SpriteBatch();
    }

    @Override
    public void render(float delta) {
        if (tick != 0) {
            tick += 1;
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) && tick == 0) {
            title = "Connecting to server";
            tick = 1;
        }
        if (tick >= 1) {
            title = Game.serverConnectStatus;
        }

        if (tick == 2) {
            Logger.info("Connecting to server");
            Thread thread = new Thread(new MultiplayerThread());
            thread.setName("Multiplayer-Client");
            thread.start();
        }


        titleLayout = new GlyphLayout(menuFont, title);
        ipLayout = new GlyphLayout(menuFont, Game.serverIp);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        menuSpriteBatch.begin();
        menuSpriteBatch.draw(Assets.manager.get("river_thunder_bg.png", Texture.class), 0, 0);
        menuFont.draw(menuSpriteBatch, titleLayout,
                (Gdx.graphics.getWidth() - titleLayout.width) / 2, Gdx.graphics.getHeight() - 40);
        menuFont.draw(menuSpriteBatch, ipLayout, (Gdx.graphics.getWidth() - titleLayout.width) / 2, Gdx.graphics.getHeight() - 150);
        menuSpriteBatch.end();
        Gdx.input.setInputProcessor(new ServerIpController());
    }

    @Override
    public void dispose() {
        menuSpriteBatch.dispose();
        menuFont.dispose();
    }

    @Override
    public void resize(int width, int height) {
    }
}
