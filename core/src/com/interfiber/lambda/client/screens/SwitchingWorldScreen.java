package com.interfiber.lambda.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.interfiber.lambda.client.Assets;
import com.interfiber.lambda.client.Game;

public class SwitchingWorldScreen extends GameScreen {

    private GlyphLayout titleLayout;

    private BitmapFont menuFont;
    private SpriteBatch menuBatch;

    @Override
    public boolean renderOverlays() {
        return false;
    }

    @Override
    public void show() {
        menuBatch = new SpriteBatch();

        FreeTypeFontGenerator fontGenerator = new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter menuFontGenerated = new FreeTypeFontGenerator.FreeTypeFontParameter();
        menuFontGenerated.size = 65;
        menuFont = fontGenerator.generateFont(menuFontGenerated);
        menuFont.getData().markupEnabled = true;

        titleLayout = new GlyphLayout(menuFont, "[YELLOW] Loading world...");
        Game.hiddenCursor = true;
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT); // clear screen
        menuBatch.begin();
        menuBatch.draw(Assets.manager.get("beach_thunder_bg.png", Texture.class), 0, 0);
        menuFont.draw(menuBatch, titleLayout, (Gdx.graphics.getWidth() - titleLayout.width) / 2, Gdx.graphics.getHeight() / 2);
        menuBatch.end();
    }

    @Override
    public void dispose() {

    }
}
