package com.interfiber.lambda.client.misc;

import org.tinylog.Logger;

public class AutoGCService {
    public static void runGC() {
        Logger.debug("Running automatic garbage collection");
        Runtime.getRuntime().gc();
    }
}
