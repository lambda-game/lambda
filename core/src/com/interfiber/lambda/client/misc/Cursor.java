package com.interfiber.lambda.client.misc;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector3;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.player.Player;
import com.interfiber.lambda.client.tiles.TileType;

public class Cursor {
    private static Texture itemTexture;
    private static BitmapFont font;
    private static Pixmap pixmap;

    public static void render(SpriteBatch batch) {
        if (Player.inventoryTile != TileType.AIR
                && Player.inventory.getCurrentlySelectedItem() != null) {
            Vector3 tmpLocation = new Vector3(Gdx.input.getX() + 50, Gdx.input.getY(), 0);
            Player.camera.unproject(tmpLocation);
            itemTexture = Player.inventory.getCurrentlySelectedItem().getTextureName();
            batch.draw(itemTexture, tmpLocation.x, tmpLocation.y, itemTexture.getWidth() / 2,
                    itemTexture.getHeight() / 2);
        }
        // render break progress next to cursor if we are breaking a tile
        if (Player.isBreakingTile) {
            Vector3 tmpLocation = new Vector3(Gdx.input.getX() + 50, Gdx.input.getY(), 0);
            Player.camera.unproject(tmpLocation);
            batch.setColor(Color.WHITE);
            String msg = (int) Game.tileBreakProgress + "%";
            font.draw(batch, msg, tmpLocation.x + msg.length(), tmpLocation.y);
        }
    }

    public static void create() {
        FreeTypeFontGenerator fontGenerator =
                new FreeTypeFontGenerator(Gdx.files.internal("LanaPixel.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter fontGenerated =
                new FreeTypeFontGenerator.FreeTypeFontParameter();
        fontGenerated.size = 12;
        fontGenerated.genMipMaps = true;
        font = fontGenerator.generateFont(fontGenerated);
    }

    public static void setCustomCursor() {
        pixmap = new Pixmap(Gdx.files.internal("cursor.png"));
        Gdx.graphics.setCursor(Gdx.graphics.newCursor(pixmap, 0, 0));
    }

    public static void dispose() {
        if (itemTexture != null) {
            itemTexture.dispose();
        }

        if (pixmap != null) {
            pixmap.dispose();
        }
        if (font != null) {
            font.dispose();
        }
    }
}
