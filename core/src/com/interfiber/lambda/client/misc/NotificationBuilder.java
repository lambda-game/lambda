package com.interfiber.lambda.client.misc;

import com.badlogic.gdx.graphics.Color;

public class NotificationBuilder {
    public static Notification buildNotification(final String title, final Color titleColor, final String content) {
        return new Notification() {

            @Override
            public String getTitle() {
                return title;
            }

            @Override
            public Color getTitleColor() {
                return titleColor;
            }

            @Override
            public String getContent() {
                return content;
            }
        };

    }
}
