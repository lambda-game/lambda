package com.interfiber.lambda.client.misc;

import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.items.Item;
import com.interfiber.lambda.client.world.StorageContainerWorldTile;

public class StorageContainerUtils {
    public static void addItemToChest(int x, int y, int slot, Item item) {
        StorageContainerWorldTile tile = (StorageContainerWorldTile) Game.getCurrentWorld().getUserTileAtPosition(x, y);
        Game.getCurrentWorld().getWorldData().remove(tile);
        tile.contents.put(slot, item.getSerializableItem());
        Game.getCurrentWorld().getWorldData().add(tile);
    }
}
