package com.interfiber.lambda.client.misc;

import com.badlogic.gdx.graphics.Color;

public abstract class Notification {
    public abstract String getTitle();

    public abstract Color getTitleColor();

    public abstract String getContent();
}
