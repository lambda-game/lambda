package com.interfiber.lambda;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.interfiber.lambda.client.Game;
import com.interfiber.lambda.client.LambdaGame;
import com.interfiber.lambda.client.bootstrap.GameBootstrap;
import com.interfiber.lambda.client.world.LevelGen;
import com.interfiber.lambda.client.world.SaveFileEditor;
import com.interfiber.lambda.server.MultiplayerGame;
import com.interfiber.lambda.server.bootstrap.ServerBootstrap;
import org.tinylog.Logger;

import javax.swing.*;
import java.io.IOException;


// Please note that on macOS your application needs to be started with the -XstartOnFirstThread JVM
// argument
public class DesktopLauncher {
    public static void main(String[] arg) throws IOException, ClassNotFoundException {
        Logger.info("Lambda v1.0.0 is loading");
        if (arg.length >= 1) {
            if (arg[0].equals("--world-generator")) {
                Logger.info("Running test world generator!");
                LevelGen.main(new String[0]);
                return;
            }
            if (arg[0].equals("--about")) {
                JOptionPane.showMessageDialog(null,
                        "Lambda v1.0.0 ALPHA, created by Interfiber(https://github.com/Interfiber)");
                return;
            }
            if (arg[0].equals("--developer-mode-enable")) {
                Logger.warn("Enabling developer mode for current run!!");
                Game.developerModeEnabled = true;
            }
            if (arg[0].equals("--launch-multiplayer-server")) {
                ServerBootstrap.main(arg);
                return;
            }

            if (arg[0].contains("login-token")) {
                String[] args = arg[0].split("=");
                if (args.length == 1) {
                    Logger.error("Login token param parsing error: syntax is like: --login-token=<TOKEN>");
                    System.exit(-1);
                }
                Logger.info("Setting auth token");
                Game.authToken = args[1];
            }
            if (arg[0].equals("--open-save-file")) {
                Logger.info("Opening save file viewer");
                SaveFileEditor.openSave(arg[1]);
                return;
            }
        }
        Logger.info("Developer mode status: " + Game.developerModeEnabled);
        Logger.info("Running game bootstrap");
        GameBootstrap.bootstrapGame();
        Logger.info("Running game startup");
        Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
        config.setTitle("Lambda");
        config.setWindowIcon(Files.FileType.Internal, "windowicon.png");
        config.setFullscreenMode(Lwjgl3ApplicationConfiguration.getDisplayMode());
        new Lwjgl3Application(new LambdaGame(), config);
        System.exit(0);
    }
}
