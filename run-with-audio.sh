#!/usr/bin/env bash

# This is only needed if you are running NixOS
# Make sure that steam-run is installed first

gradle desktop:dist
steam-run java -jar $PWD/desktop/build/libs/desktop-1.0.jar $@
